﻿using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Runtime.InteropServices;


namespace BandObjectLib
{
    [RunInstaller(true)]
    public partial class BandObjectLibInstaller : System.Configuration.Install.Installer
    {
        public BandObjectLibInstaller()
        {
            InitializeComponent();
        }

        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);

            RegistrationServices regsrv = new RegistrationServices();
            if (!regsrv.RegisterAssembly(this.GetType().Assembly,
                AssemblyRegistrationFlags.SetCodeBase))
            {
                throw new InstallException("Failed to Register for COM");
            }
        }

        public override void Uninstall(IDictionary savedState)
        {
            base.Uninstall(savedState);

            RegistrationServices regsrv = new RegistrationServices();
            if (!regsrv.UnregisterAssembly(this.GetType().Assembly))
            {
                throw new InstallException("Failed to Unregister for COM");
            }
        }
    }
}
