﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Win32;
using SHDocVw;
using mshtml;

namespace BandObjectLib
{
    /*  // The delegate of the handler method.
      public delegate void HtmlEvent(MSHTML.IHTMLEventObj e);

      [ComVisible(true)]
      public class HTMLEventHandler
      {

          private MSHTML.HTMLDocument htmlDocument;

          public event HtmlEvent eventHandler;

          public HTMLEventHandler(MSHTML.HTMLDocument htmlDocument)
          {
              this.htmlDocument = htmlDocument;
          }

          [DispId(0)]
          public void FireEvent()
          {
              this.eventHandler(this.htmlDocument.parentWindow.@event);
          }
      }
      [ComVisible(true)]
      public class HTMLDocumentEventHelper
      {

          private HTMLDocument document;

          public HTMLDocumentEventHelper(HTMLDocument document)
          {
              this.document = document;
          }

          public event HtmlEvent oncontextmenu
          {
              add
              {
                  DispHTMLDocument dispDoc = this.document as DispHTMLDocument;

                  object existingHandler = dispDoc.oncontextmenu;
                  HTMLEventHandler handler = existingHandler is HTMLEventHandler ?
                      existingHandler as HTMLEventHandler :
                      new HTMLEventHandler(this.document);

                  // Set the handler to the oncontextmenu event.
                  dispDoc.oncontextmenu = handler;

                  handler.eventHandler += value;
              }
              remove
              {
                  DispHTMLDocument dispDoc = this.document as DispHTMLDocument;
                  object existingHandler = dispDoc.oncontextmenu;

                  HTMLEventHandler handler = existingHandler is HTMLEventHandler ?
                      existingHandler as HTMLEventHandler : null;

                  if (handler != null)
                      handler.eventHandler -= value;
              }
          }
      } */

    
    /// <summary>
    /// Implements generic Band Object functionality. 
    /// </summary>
    /// <example>
    /// [Guid("YOURGUID-GOES-HERE-YOUR-GUIDGOESHERE")]
    /// [BandObject("Hello World Bar", BandObjectStyle.Horizontal | BandObjectStyle.ExplorerToolbar , HelpText = "Shows bar that says hello.")]
    /// public class HelloWorldBar : BandObject
    /// { /*...*/ }
    /// </example>
    /// 
    
    public class BandObject : UserControl, IObjectWithSite, IDeskBand, IDockingWindow, IOleWindow, IInputObject
    {
        /// <summary>
        /// Reference to the host explorer.
        /// </summary>
        public WebBrowserClass Explorer;
        protected IInputObjectSite BandObjectSite;
        /// <summary>
        /// This event is fired after reference to hosting explorer is retreived and stored in Explorer property.
        /// </summary>
        public event EventHandler ExplorerAttached;

        public BandObject()
        {
            InitializeComponent();            
        }

        private void InitializeComponent()
        {
            // 
            // ExplorerBar
            // 
            this.Name = "BandObject";
        }


        /// <summary>
        /// Title of band object. Displayed at the left or on top of the band object.
        /// </summary>
        [Browsable(true)]
        [DefaultValue("")]
        public String Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }String _title;


        /// <summary>
        /// Minimum size of the band object. Default value of -1 sets no minimum constraint.
        /// </summary>
        [Browsable(true)]
        [DefaultValue(typeof(Size), "-1,-1")]
        public Size MinSize
        {
            get
            {
                return _minSize;
            }
            set
            {
                _minSize = value;
            }
        }Size _minSize = new Size(-1, -1);

        /// <summary>
        /// Maximum size of the band object. Default value of -1 sets no maximum constraint.
        /// </summary>
        [Browsable(true)]
        [DefaultValue(typeof(Size), "-1,-1")]
        public Size MaxSize
        {
            get
            {
                return _maxSize;
            }
            set
            {
                _maxSize = value;
            }
        }Size _maxSize = new Size(-1, -1);

        /// <summary>
        /// Says that band object's size must be multiple of this size. Defauilt value of -1 does not set this constraint.
        /// </summary>
        [Browsable(true)]
        [DefaultValue(typeof(Size), "-1,-1")]
        public Size IntegralSize
        {
            get
            {
                return _integralSize;
            }
            set
            {
                _integralSize = value;
            }
        }Size _integralSize = new Size(-1, -1);


        public virtual void GetBandInfo(
            UInt32 dwBandID,
            UInt32 dwViewMode,
            ref DESKBANDINFO dbi)
        {
            dbi.wszTitle = this.Title;

            dbi.ptActual.X = this.Size.Width;
            dbi.ptActual.Y = this.Size.Height;

            dbi.ptMaxSize.X = this.MaxSize.Width;
            dbi.ptMaxSize.Y = this.MaxSize.Height;

            dbi.ptMinSize.X = this.MinSize.Width;
            dbi.ptMinSize.Y = this.MinSize.Height;

            dbi.ptIntegral.X = this.IntegralSize.Width;
            dbi.ptIntegral.Y = this.IntegralSize.Height;

            dbi.dwModeFlags = DBIM.TITLE | DBIM.VARIABLEHEIGHT | DBIM.NORMAL;
            
        }

        /// <summary>
        /// Called by explorer when band object needs to be showed or hidden.
        /// </summary>
        /// <param name="fShow"></param>
        public virtual void ShowDW(bool fShow)
        {
            if (fShow)
            {
                
                Show();

            }
            else
                Hide();
        }

        /// <summary>
        /// Called by explorer when window is about to close.
        /// </summary>
        public virtual void CloseDW(UInt32 dwReserved)
        {
            Dispose(true);
        }

        /// <summary>
        /// Not used.
        /// </summary>
        public virtual void ResizeBorderDW(IntPtr prcBorder, Object punkToolbarSite, bool fReserved) { }

        public virtual void GetWindow(out System.IntPtr phwnd)
        {
            phwnd = Handle;
        }

        public virtual void ContextSensitiveHelp(bool fEnterMode) { }

        public virtual void SetSite(Object pUnkSite)
        {          
              try
              {
                  if (BandObjectSite != null)
                      Marshal.ReleaseComObject(BandObjectSite);
                  
                  if (Explorer != null)
                  {
                      Marshal.ReleaseComObject(Explorer);
                      Explorer = null;
                  }
                  
                  BandObjectSite = pUnkSite as IInputObjectSite;                                 

                  if (BandObjectSite != null)
                  {                      
                      //pUnkSite is a pointer to object that implements IOleWindowSite or something  similar
                      //we need to get access to the top level object - explorer itself
                      //to allows this explorer objects also implement IServiceProvider interface
                      //(don't mix it with System.IServiceProvider!)
                      //we get this interface and ask it to find WebBrowserApp
                      _IServiceProvider sp = BandObjectSite as _IServiceProvider;
                      Guid guid = ExplorerGUIDs.IID_IWebBrowserApp;
                      Guid riid = ExplorerGUIDs.IID_IUnknown;

                      try
                      {
                          object w;
                          sp.QueryService(
                              ref guid,
                              ref riid,
                              out w);

                          //once we have interface to the COM object we can create RCW from it
                          Explorer = (WebBrowserClass)Marshal.CreateWrapperOfType(
                              w as IWebBrowser,
                              typeof(WebBrowserClass)
                              );

                          OnExplorerAttached(EventArgs.Empty);


                      }
                      catch (COMException comex)
                      {                          
                          //we anticipate this exception in case our object instantiated 
                          //as a Desk Band. There is no web browser service available.
                      }
                  }
              }
              catch (COMException comex)
              {                 
                  //we anticipate this exception in case our object instantiated 
                  //as a Desk Band. There is no web browser service available.
              }
              catch(Exception ex)
              {                  
              }     
        }

       void ieInstance_DocumentComplete(object pDisp, ref object URL)
        {
            string url = URL as string;
            if (string.IsNullOrEmpty(url)
                || url.Equals("about:blank", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            InternetExplorer explorer = pDisp as InternetExplorer;

            // Set the handler of the document in InternetExplorer.
            if (explorer != null)
            {
                // SetHandler(explorer);
                //OnExplorerAttached(EventArgs.Empty);
                
            }
        }


        //void SetHandler(InternetExplorer explorer)
        //{
        //    try
        //    {

        //        // Register the oncontextmenu event of the  document in InternetExplorer.
        //        HTMLDocumentEventHelper helper =
        //            new HTMLDocumentEventHelper(explorer.Document as HTMLDocument);
        //        helper.oncontextmenu += new HtmlEvent(oncontextmenuHandler);
        //    }
        //    catch { }
        //}

        ///// <summary>
        ///// Handle the oncontextmenu event.
        ///// </summary>
        ///// <param name="e"></param>
        //void oncontextmenuHandler(IHTMLEventObj e)
        //{

        //    // To cancel the default behavior, set the returnValue property of the event
        //    // object to false.
        //    e.returnValue = false;

        //}  

        public virtual void GetSite(ref Guid riid, out Object ppvSite)
        {
            ppvSite = BandObjectSite;
        }

        /// <summary>
        /// Called explorer when focus has to be chenged.
        /// </summary>
        public virtual void UIActivateIO(Int32 fActivate, ref MSG Msg)
        {
            if (fActivate != 0)
            {
                Control ctrl = GetNextControl(this, true);//first
                if (ModifierKeys == Keys.Shift)
                    ctrl = GetNextControl(ctrl, false);//last

                if (ctrl != null) ctrl.Select();
                this.Focus();
                BandObjectSite.OnFocusChangeIS(this as IInputObject, 1);
            }
        }

        public virtual Int32 HasFocusIO()
        {
            return this.ContainsFocus ? 0 : 1; //S_OK : S_FALSE;
        }

        /// <summary>
        /// Called by explorer to process keyboard events. Undersatands Tab and F6.
        /// </summary>
        /// <param name="msg"></param>
        /// <returns>S_OK if message was processed, S_FALSE otherwise.</returns>
        public virtual Int32 TranslateAcceleratorIO(ref MSG msg)
        {
            if (msg.message == 0x100)//WM_KEYDOWN
            {
                if (msg.wParam == (uint)Keys.Tab || msg.wParam == (uint)Keys.F6)//keys used by explorer to navigate from control to control
                {
                    if (SelectNextControl(
                            ActiveControl,
                            ModifierKeys == Keys.Shift ? false : true,
                            true,
                            true,
                            false)
                        )
                        return 0;//S_OK
                }
                
            }
            return 1;//S_FALSE
        }

        /// <summary>
        /// Override this method to handle ExplorerAttached event.
        /// </summary>
        /// <param name="ea"></param>
        protected virtual void OnExplorerAttached(EventArgs ea)
        {
            if (ExplorerAttached != null)
                ExplorerAttached(this, ea);
        }

        /// <summary>
        /// Notifies explorer of focus change.
        /// </summary>
        protected override void OnGotFocus(System.EventArgs e)
        {
            base.OnGotFocus(e);
            BandObjectSite.OnFocusChangeIS(this as IInputObject, 1);
        }
        /// <summary>
        /// Notifies explorer of focus change.
        /// </summary>
        protected override void OnLostFocus(System.EventArgs e)
        {
            base.OnLostFocus(e);
            if (ActiveControl == null)
                BandObjectSite.OnFocusChangeIS(this as IInputObject, 0);
        }

    }
}
