﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows;

namespace Deque.WorldSpace.AddIns
{
    public class LogEvent
    {
        private static string logSourceName = "WorldSpace Attest";
        /// <summary>
        /// 
        /// </summary>
        public static void CreateLogSource()
        {
            try
            {
                // Create Event Log Source (must be completed with Admin rights during install
                if (!EventLog.SourceExists(logSourceName))
                {
                    EventLog.CreateEventSource(logSourceName, "Application");
                }
            }
            catch(Exception ex) {
               // MessageBox.Show(ex.Message);
            };
        }

        public static void LogErrorMessage(string msg, string sourceMethod)
        {
            try
            {
                if (!EventLog.SourceExists(logSourceName))
                {
                    CreateLogSource();
                }
                EventLog.WriteEntry(logSourceName, msg + " , source method: "  + WSConstants.WS_ADDIN_NAME + " " + sourceMethod, EventLogEntryType.Information);
            }
            catch(Exception ex) {
               // MessageBox.Show("Error received: " + msg + " , source method: " + sourceMethod + " , " + ex.Message, WSConstants.WS_ADDIN_NAME);
            }
        }

        public static void LogWarningMessage(string msg, string sourceMethod)
        {
            try
            {
                if (!EventLog.SourceExists(logSourceName))
                {
                    CreateLogSource();
                }
                EventLog.WriteEntry(logSourceName, msg + " , source method: " + sourceMethod, EventLogEntryType.Warning);
            }
            catch { };
        }

        public static void LogInformationMessage(string msg, string sourceMethod)
        {
            try
            {
                if (!EventLog.SourceExists(logSourceName))
                {
                    CreateLogSource();
                }
                EventLog.WriteEntry(logSourceName, msg + " , source method: " + sourceMethod, EventLogEntryType.Information);
            }
            catch { };
        }
    }
}
