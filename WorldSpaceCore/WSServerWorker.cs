﻿using mshtml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Windows.Threading;
using System.Xml;

namespace Deque.WorldSpace.AddIns
{
    public sealed class WSServerWorker : INotifyPropertyChanged
    {

        #region Variables

        static readonly WSServerWorker _instance = new WSServerWorker();
        private string status = "Not Logged In";
        private string projectName = "No Project";
        private HttpWebRequest loginRequest;
        private CookieContainer sessionCookies;
        private WorldSpaceProject selectedProject;
        private WSProjectDomain[] projectDomains;
        private WorldSpaceProject[] projects = null;
        public string _serverUrl = String.Empty;
        public string selectedLabel = String.Empty;
        public string selectedUserName = String.Empty;
        public string _SSOToken = String.Empty;
        public WSActiveRule activeRule;
        public bool IsSSOSignIn = false;
        private string _userName = String.Empty;
        private string _password = String.Empty;
        public  string projectRuleId = string.Empty;
        private bool _analysisRunning = false;
        private bool _userLoggedIn = false;
        private string _analysisStatus = WSConstants.WS_NO_STATUS;
        private bool storeIssues = false;
        private bool _automaticallyScan = false;
        private bool _highlightIssues = false;
        private string issuesLabel = String.Empty;
        public bool IsProjectChanged = false;
        public AccessibilityIssue[] violations { get; set; }
        public AccessibilityIssue[] incomplete { get; set; }
        private ObservableCollectionEx<AccessibilityIssue> accessibilityIssues = new ObservableCollectionEx<AccessibilityIssue>();
        private ObservableCollectionEx<WorldSpaceAttestProject> attestProjects = new ObservableCollectionEx<WorldSpaceAttestProject>();
        private Hashtable worldSpaceViolationsShortMessages = new Hashtable();
        private Hashtable worldSpaceHelpFileAssociations = new Hashtable();
        private List<ViolationLookup> violationLookUpData = new List<ViolationLookup>();
        private WSRules rules = new WSRules();
        private WSResources projectDetails = null;
        private ObservableCollection<string> filterIssueSeverity;       
        private ObservableCollection<string> filterIssuePriority;
        //private List<string> filterIssueSeverity = new List<string>() { "level.manual", "level.potential", "level.violation", "level.recommendation" };
        //private List<string> filterIssuePriority = new List<string>() {"minor", "moderate", "serious", "critical" };
        private HTMLDocument htmlDoc = new HTMLDocument();
        private string browserUrl = String.Empty;
        DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private IIssueProcessor issueProcessor = null;
        private string serverVersion = String.Empty;
        private string pluginVersion = String.Empty;
        public static string CONFIG_SETTINGS_FOLDER_URL = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "WorldspaceAttestIE");

        #endregion

        #region Properties

        public ArrayList ProjectUsers
        {
            get { return projectDetails.usernames; }
        }

        public ObservableCollection<string> FilterIssueSeverity
        {
            get { return filterIssueSeverity; }
        }

        public ObservableCollection<string> FilterIssuePriority
        {
            get { return filterIssuePriority; }
        }

        public string UserName
        {
            get { return _userName; }
        }

        public int violationCount
        {
            get; set;
        }

        public int needsReviewCount
        {
            get; set;
        }


        

        public WSResources ProjectDetails
        {
            get
            {
                if (projectDetails == null) LoadProjectDetails();
                return projectDetails;
            }
            set
            {
                projectDetails = value;
                OnPropertyChanged("ProjectDetails");
            }
        }

        public ObservableCollectionEx<WorldSpaceAttestProject> Attestprojects
        {
            get
            {
                return attestProjects;
            }
        }

        public ObservableCollectionEx<AccessibilityIssue> AccessibilityIssues
        {
            get
            {
                return accessibilityIssues;
            }
        }

        public string IssuesLabel
        {
            get {
                if (String.IsNullOrEmpty(issuesLabel)) issuesLabel = DateTime.Now.ToString("D");
                return issuesLabel; 
            }
            set 
            { 
                issuesLabel = value;
                OnPropertyChanged("IssuesLabel");
            }
        }

        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                OnPropertyChanged("Status");
            }
        }

        public string AnalysisStatus
        {
            get { return _analysisStatus; }
            set
            {
                _analysisStatus = value;
                OnPropertyChanged("AnalysisStatus");
            }
        }

        public string ProjectName
        {
            get { return projectName; }
            set
            {
                projectName = value;
                OnPropertyChanged("ProjectName");
            }
        }

        public WorldSpaceProject SelectedProject
        {
            get
            {
                return selectedProject;
            }
            set
            {
                selectedProject = value;
                OnPropertyChanged("SelectedProject");
            }
        }

        public WSProjectDomain[] ProjectDomains
        {
            get { return projectDomains; }
            set { projectDomains = value; }
        }

        public WorldSpaceProject[] Projects
        {
            get { return projects; }
            set
            {
                projects = value;
                OnPropertyChanged("Projects");
            }
        }


        public WSDownloadRulesResult downloadRulesResult;

        public bool AnalysisRunning
        {
            get { return _analysisRunning; }
            set 
            { 
                _analysisRunning = value;
                OnPropertyChanged("AnalysisRunning");
            }
        }

        public bool UserLoggedIn
        {
            get { return _userLoggedIn; }
            set
            {
                _userLoggedIn = value;
                
            }
        }

        public bool StoreIssues
        {
            get { return storeIssues; }
            set
            {
                storeIssues = value;
                OnPropertyChanged("StoreIssues");
            }
        }

        public bool AutomaticallyScan
        {
            get { return _automaticallyScan; }
            set
            {
                _automaticallyScan = value;
                OnPropertyChanged("AutomaticallyScan");
            }
        }

        public bool HighlightIssues
        {
            get { return _highlightIssues; }
            set { _highlightIssues = value; }
        }

        public string CurrentBrowserURLDomain
        {
            get
            {
                Uri browserURL = new Uri(BrowserURL);
                return browserURL.Host;
            }
        }

        public string CompleteURL
        {
            get
            {
                Uri browserURL = new Uri(BrowserURL);
                return browserURL.GetLeftPart(UriPartial.Authority);
                    
            }
        }

        public Hashtable WorldSpaceHelpFileAssociations 
        { 
            get
            {
                if (worldSpaceHelpFileAssociations.Count < 1) LoadWorldSpaceHelpFileAssociations();
                return worldSpaceHelpFileAssociations;
            }
            set
            {
                worldSpaceHelpFileAssociations = value; 
            }
        }

        public Hashtable WorldSpaceViolationsShortMessages
        {
            get
            {
                if (worldSpaceViolationsShortMessages.Count < 1) LoadWorldSpaceViolationsShortMessages();
                return worldSpaceViolationsShortMessages;
            }
            set
            {
                worldSpaceViolationsShortMessages = value;
            }
        }

        public List<ViolationLookup> ViolationLookUpData
        {
            get
            {
                if (violationLookUpData.Count < 1) LoadViolationLookupData();
                return violationLookUpData;
            }
            set
            {
                violationLookUpData = value;
            }
        }

        public WSRules Rules
        {
            get
            {
                if (!rules.success) LoadServerRules();
                return rules;
            }
        }

        public HTMLDocument HTMLDoc
        {
            get
            {
                return htmlDoc;
            }
            set
            {
                htmlDoc = value;
                OnPropertyChanged("HTMLDoc");
            }
        }
        
        public string BrowserURL
        {
            get { return browserUrl; }
            set
            {
                browserUrl = value;
                OnPropertyChanged("BrowserURL");
            }
        }

        public string ServerVersion
        {
            get 
            {
                if (Status != "Logged In")
                    return String.Empty;
                else
                    return serverVersion;
            }
            set
            {
                serverVersion = value;
                OnPropertyChanged("ServerVersion");
            }
        }

        public string PluginVersion
        {
            get
            {
                return typeof(WSServerWorker).Assembly.GetName().Version.ToString();
            }
        }
        #endregion

        #region Ctors

        public static WSServerWorker Instance
        {
            get { return _instance; }
        }

        private WSServerWorker()
        {
            // Setup FilterIssueSeverity and Priority
            filterIssueSeverity = new ObservableCollection<string>();
            filterIssueSeverity.Add("level.manual");
            filterIssueSeverity.Add("level.potential");
            filterIssueSeverity.Add("level.violation");
            filterIssueSeverity.Add("level.recommendation");
        
            filterIssuePriority = new ObservableCollection<string>();
            filterIssuePriority.Add("minor");
            filterIssuePriority.Add("moderate");
            filterIssuePriority.Add("serious");
            filterIssuePriority.Add("critical");
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Private Methods

        private void LoadProjectDetails()
        {
            if (SelectedProject != null && SelectedProject.id > -1)
            {
                try
                {
                    string urlString = RESTRequestActions.WS_REST_PROJECT_RESOURCES_ACTION + "" + SelectedProject.id.ToString();
                    string result = GetRESTResponse(urlString, "GET", null);

                    if (!String.IsNullOrEmpty(result))
                    {
                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        jsonSerializer.MaxJsonLength = Int32.MaxValue;
                        WSResources wsResult = jsonSerializer.Deserialize<WSResources>(result);
                        if (wsResult.success)
                        {
                            ProjectDetails = wsResult; ;
                        }
                    }
                }
                catch(Exception ex)
                {
                    LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Common utility to send the request to REST API
        /// </summary>
        /// <param name="actionURLString"></param>
        /// <param name="parameters"></param>
        private string GetRESTResponse(string actionURLString, string reqMethod, Hashtable parameters)
        {
            string result = null;
            Uri worldSpaceUri;

            //send a request to REST API and verify the validity of login information            
            try
            {
                
                if (loginRequest == null)
                {
                    Login();
                }

                Uri.TryCreate(_serverUrl + actionURLString, UriKind.Absolute, out worldSpaceUri);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                HttpWebRequest wsreq = WebRequest.Create(worldSpaceUri) as HttpWebRequest;
                wsreq.CookieContainer = sessionCookies;
                wsreq.KeepAlive = true;
                //V6 - Adding token for authorization
                wsreq.Headers.Add("Authorization", "bearer " + _SSOToken); // "Bearer " + token;
                wsreq.Method = reqMethod;
                wsreq.Timeout = wsreq.ServicePoint.MaxIdleTime = wsreq.ReadWriteTimeout = 4 * 60 * 1000; // 4 minutes
                GetParametrizedRequest(ref wsreq, parameters);
                try
                {
                    wsreq.ServicePoint.MaxIdleTime = 240 * 1000;
                    // Pick up the response:
                    using (HttpWebResponse resp = wsreq.GetResponse() as HttpWebResponse)
                    {
                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        result = reader.ReadToEnd();
                        
                    }
                    wsreq = null;
                    
                }
                catch (Exception inEx)
                {
                    wsreq = null;
                    LogEvent.LogErrorMessage(inEx.Message, MethodInfo.GetCurrentMethod().Name);
                    throw inEx;
                }
                return result;
                
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <param name="newSize"></param>
        /// <returns></returns>
        private byte[] ReDim(byte[] olddata, byte[] newdata)
        {
            byte[] abytNew = null;
            try
            {
                if (olddata == null)
                    return newdata;

                abytNew = new byte[olddata.Length + newdata.Length];
                Buffer.BlockCopy(olddata, 0, abytNew, 0, olddata.Length);
                Buffer.BlockCopy(newdata, 0, abytNew, olddata.Length, newdata.Length);

            }
            catch { }

            return abytNew;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wsRequest"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private HttpWebRequest GetParametrizedRequest(ref HttpWebRequest wsRequest, Hashtable parameters)
        {
            try
            {
                if (parameters != null)
                {
                    //POST                
                    wsRequest.ContentType = "application/x-www-form-urlencoded"; 
                    StringBuilder paramz = new StringBuilder();
                    int keyCount = 0;

                    byte[] pdata = null;

                    foreach (string key in parameters.Keys)
                    {
                        byte[] formData = null;
                        if (!parameters[key].GetType().Equals(typeof(object[])))
                        {
                            string str = key + "=" + HttpUtility.UrlEncode(parameters[key].ToString());
                            formData = UTF8Encoding.UTF8.GetBytes(str);
                        }
                        else
                        {
                            string str = key + "=";
                            formData = UTF8Encoding.UTF8.GetBytes(str);
                            pdata = ReDim(pdata, formData);

                            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                            MemoryStream ms = new MemoryStream();
                            bf.Serialize(ms, parameters[key]);
                            formData = ms.ToArray();
                        }
                        pdata = ReDim(pdata, formData);

                        if (keyCount < parameters.Count)
                        {
                            string str = "&";
                            formData = UTF8Encoding.UTF8.GetBytes(str);
                            pdata = ReDim(pdata, formData);

                            keyCount++;
                        }
                    }

                    wsRequest.ContentLength = pdata.Length;
                    using (Stream post = wsRequest.GetRequestStream())
                    {
                        post.Write(pdata, 0, pdata.Length);
                    }


                }
                else
                {
                    //GET
                    wsRequest.Method = "GET";
                }

                
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
            return wsRequest;
        }
                
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public bool SSOSignout(string logoutURL)
        {
            bool blnSignout = false;
            try
            {
                HttpWebRequest logoutRequest = WebRequest.Create(logoutURL) as HttpWebRequest;
                logoutRequest.ContentType = "application/x-www-form-urlencoded";
                logoutRequest.KeepAlive = true;
                logoutRequest.Headers.Add("Authorization", "bearer " + _SSOToken);
                logoutRequest.Method = "GET";
                using (HttpWebResponse resp = logoutRequest.GetResponse() as HttpWebResponse)
                {
                    blnSignout = true;
                    StreamReader reader = new StreamReader(resp.GetResponseStream());
                    string result = reader.ReadToEnd();
                }
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
            return blnSignout;
        }

        public bool SSOLogin()
        {
            bool loginResults = false;

            try
            {
                string result = String.Empty;
                Hashtable loginParameters = new Hashtable();
                if (IsURLValid(_serverUrl))
                {
                    loginParameters["ClientId"] = "attest";
                    loginParameters["Realm"] = "worldspace-comply";

                    Uri worldSpaceUri;
                    Uri.TryCreate(_serverUrl, UriKind.Absolute, out worldSpaceUri);
                    loginRequest = WebRequest.Create(worldSpaceUri) as HttpWebRequest;
                    loginRequest.ContentType = "application/x-www-form-urlencoded";
                    loginRequest.KeepAlive = true;
                    loginRequest.Headers.Add("Authorization", "bearer " + _SSOToken);
                    loginRequest.Method = "GET";
                    GetParametrizedRequest(ref loginRequest, loginParameters);
                    //System.Diagnostics.Process.Start("IEXPLORE",loginRequest.RequestUri.ToString());
                    //// Pick up the response:    
                    try
                    {
                        loginResults = true; //Temporary
                        using (HttpWebResponse resp = loginRequest.GetResponse() as HttpWebResponse)
                        {
                            StreamReader reader = new StreamReader(resp.GetResponseStream());
                            result = reader.ReadToEnd();
                        }
                    }
                    catch(Exception ex)
                    {
                        Status = "Not Logged In";
                        issueProcessor = IssueProcessorFactory.getInstance("2.0.0");
                        LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                        return loginResults;
                    }
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    jsonSerializer.MaxJsonLength = Int32.MaxValue;
                    WSLogin wsLogin = jsonSerializer.Deserialize<WSLogin>(result);
                    if (wsLogin == null || !wsLogin.authenticated)
                    {
                        Status = "Not Logged In";
                        return loginResults;
                    }
                    else
                    {
                        Status = "Logged In";
                        loginResults = true;
                        ServerVersion = wsLogin.version;
                        issueProcessor = IssueProcessorFactory.getInstance(wsLogin.version);
                    }
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                return loginResults;

            }
            return loginResults;
        }

        private bool Login()
        {
            bool loginResults = false;
            if(_SSOToken != "" || _SSOToken != string.Empty)
            {
                loginResults = true;
                issueProcessor = IssueProcessorFactory.getInstance("2.0.0");
            }
            else
            {
                loginResults = false;
            }
            return loginResults;           
        }


        /// <summary>
        /// Validates the WorldSpace Comply URL
        /// </summary>
        public bool ValidateComplyLoginURL(string wsURL)
        {
            try
            {
                Uri uriResult;
                bool result = Uri.TryCreate(wsURL, UriKind.Absolute, out uriResult)
                    && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                if (!result)
                {
                    return false;
                }
                try
                {
                    HttpWebRequest wsreq = WebRequest.Create(wsURL) as HttpWebRequest;
                    wsreq.Method = "GET";
                    wsreq.KeepAlive = false;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    // Pick up the response:
                    using (HttpWebResponse resp = wsreq.GetResponse() as HttpWebResponse)
                    {
                        if (resp.StatusCode != HttpStatusCode.Unauthorized)
                        {
                            return false;
                        }
                    }
                    wsreq = null;
                }
                catch (Exception innerEx)
                {
                    return false;
                }
                
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Validates the WorldSpace Comply URL
        /// </summary>
        public bool ValidateWorldSpaceURL(ref string wsURL, out string signInErrorStatus)
        {
            try
            {
                if (wsURL == "")
                {
                    signInErrorStatus = "Please provide a server URL";
                    return false;
                }
                wsURL = wsURL.ToLower();
                if (!wsURL.EndsWith("/"))
                {
                    wsURL += "/";
                }
                if (!wsURL.Contains("/worldspace"))
                {
                    signInErrorStatus = "Please provide a valid server URL";
                    return false;
                }
                Uri uriResult;
                bool result = Uri.TryCreate(wsURL, UriKind.Absolute, out uriResult)
                    && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                if (!result)
                {                    
                    signInErrorStatus = "Please provide a valid server URL";
                    return false;
                }
                _serverUrl = wsURL;
                //try
                //{
                //    HttpWebRequest wsreq = WebRequest.Create(wsURL) as HttpWebRequest;
                //    wsreq.Method = "GET";
                //    wsreq.KeepAlive = false;
                //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                //    // Pick up the response:
                //    using (HttpWebResponse resp = wsreq.GetResponse() as HttpWebResponse)
                //    {
                //        if (resp.StatusCode != HttpStatusCode.Unauthorized)
                //        {
                //            signInErrorStatus = "Please provide a valid server URL";
                //            return false;
                //        }
                //    }
                //    wsreq = null;
                //}
                //catch (Exception innerEx)
                //{
                //    signInErrorStatus = "Please provide a valid server URL";
                //    return false;
                //}
                signInErrorStatus = "";
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetPageHtml(HTMLDocument htmlDoc)
        {
            try
            {
                IHTMLElement rootNode = htmlDoc.documentElement;
                IHTMLElementCollection docNodes = (IHTMLElementCollection)rootNode.children;
                int childCount = docNodes != null ? docNodes.length : 0;
                StringBuilder pageSource = new StringBuilder();
                string htmlDeclaration = rootNode.outerHTML.Substring(0, rootNode.outerHTML.IndexOf('>') + 1);
                pageSource.Append(htmlDeclaration);

                SetScriptsANDRelativePaths(docNodes);

                foreach (IHTMLElement child in docNodes)
                {
                    pageSource.Append(child.outerHTML);
                }
                
                try
                {
                    Regex cssUrls = new Regex("@import url\\(([\"\"']?)(?<url>[^\"\"')]+)\\)", RegexOptions.IgnoreCase);
                    foreach (Match item in cssUrls.Matches(pageSource.ToString()))
                    {
                        if (item.ToString().ToLower().Contains("@import"))
                        {
                            string relativeUrl = item.Groups[2].Value.ToString();
                            if (relativeUrl.StartsWith("/"))
                            {
                                string host = CompleteURL;
                                string absoluteUrl = host + relativeUrl;
                                pageSource = pageSource.Replace(relativeUrl, absoluteUrl);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                    throw ex;
                }

                pageSource.Append("</html>");
                return pageSource.ToString();
            }
            catch(Exception ex) 
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                return String.Empty;
            }
        }

        private void SetScriptsANDRelativePaths(IHTMLElementCollection nodes)
        {
            try
            {
                foreach (IHTMLElement hChild in nodes)
                {
                    string tagname = hChild.tagName;
                    if (tagname.Equals("head", StringComparison.CurrentCultureIgnoreCase))
                    {
                        // Find any image tags and remove
                        foreach (IHTMLElement innerChild in (IHTMLElementCollection)hChild.children)
                        {
                            if (innerChild.tagName.Equals("img", StringComparison.CurrentCultureIgnoreCase) ||
                                innerChild.tagName.Equals("div", StringComparison.CurrentCultureIgnoreCase))
                            {
                                innerChild.outerHTML = String.Empty;
                            }
                        }
                    }

                    if (tagname.Equals("script", StringComparison.CurrentCultureIgnoreCase))
                    {
                        try
                        {
                            hChild.outerHTML = String.Empty;
                            object srcAtt = hChild.getAttribute("src", 4);
                            if (srcAtt != null && !srcAtt.ToString().Trim().Equals(String.Empty))
                            {
                                hChild.removeAttribute("src", 0);
                            }
                            object typeAtt = hChild.getAttribute("type", 0);
                            if (typeAtt != null && !typeAtt.ToString().Trim().Equals(String.Empty))
                            {
                                hChild.removeAttribute("type", 0);
                            }
                            object charAtt = hChild.getAttribute("charset", 0);
                            if (charAtt != null && !charAtt.ToString().Trim().Equals(String.Empty))
                            {
                                hChild.removeAttribute("charset", 0);
                            }
                            object deferAtt = hChild.getAttribute("defer", 0);
                            if (deferAtt != null && !deferAtt.ToString().Trim().Equals(String.Empty))
                            {
                                hChild.removeAttribute("defer", 0);
                            }
                            object asyncAtt = hChild.getAttribute("async", 0);
                            if (asyncAtt != null && !asyncAtt.ToString().Trim().Equals(String.Empty))
                            {
                                hChild.removeAttribute("async", 0);
                            }
                            object xmlAtt = hChild.getAttribute("xml:space", 0);
                            if (xmlAtt != null && !xmlAtt.ToString().Trim().Equals(String.Empty))
                            {
                                hChild.removeAttribute("xml:space", 0);
                            }
                        }
                        catch(Exception ex){
                            LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                            throw ex;
                        }
                    }
                    else
                    {
                        object hrefAtt = hChild.getAttribute("href", 4);
                        object srcAtt = hChild.getAttribute("src", 4);

                        if (hrefAtt != null && !hrefAtt.ToString().Trim().Equals(String.Empty))
                        {
                            hChild.setAttribute("href", hrefAtt.ToString(), 0);
                        }

                        if (srcAtt != null && !srcAtt.ToString().Trim().Equals(String.Empty))
                        {
                            hChild.setAttribute("src", srcAtt.ToString(), 0);
                        }

                        if (hChild.children != null)
                        {
                            SetScriptsANDRelativePaths((IHTMLElementCollection)hChild.children);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadWorldSpaceHelpFileAssociations()
        {
            try
            {
                worldSpaceHelpFileAssociations = new Hashtable();

                string allHelpFiles = Properties.Resources.help;
                string[] helpFiles = Regex.Split(allHelpFiles, "\\[(.*?)\\]");

                for (int ind = 1; ind < helpFiles.Length; ind++)
                {
                    if (ind >= 3 && ind % 2 != 0)
                    {
                        string wsHelp = helpFiles[ind];
                        string[] helpData = wsHelp.Split(new char[] { ',' });
                        helpData[0] = helpData[0].Replace("\"", "").Trim();
                        helpData[1] = helpData[1].Replace("\"", "").Trim();

                        if (!worldSpaceHelpFileAssociations.ContainsKey(helpData[0]))
                        {
                            worldSpaceHelpFileAssociations.Add(helpData[0], helpData[1]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        private void LoadViolationLookupData()
        {
            try
            {
                violationLookUpData = new List<ViolationLookup>();
                string allViolations = Properties.Resources.violation_lookup_table;
                string[] violations = Regex.Split(allViolations, "\\[(.*?)\\]");

                for (int ind = 1; ind < violations.Length; ind++)
                {
                    if (ind >= 3 && ind % 2 != 0)
                    {
                        string wsViolation = violations[ind];
                        string[] violationData = wsViolation.Split(new char[] { ',' });
                        violationData[0] = violationData[0].Replace("\"", "");
                        violationData[1] = violationData[1].Replace("\"", "");
                        violationData[2] = violationData[2].Replace("\"", "");
                        violationData[3] = violationData[3].Replace("\"", "");

                        violationLookUpData.Add(new ViolationLookup(violationData[0], violationData[1], violationData[2], violationData[3]));
                    }
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadWorldSpaceViolationsShortMessages()
        {
            try
            {
                worldSpaceViolationsShortMessages = new Hashtable();

                string allErrorDescriptions = Properties.Resources.WSErrorDescriptions;

                // Get rid of JavaScript array wrapper and declaration statement
                allErrorDescriptions = allErrorDescriptions.Substring(0, allErrorDescriptions.Length - 1);
                allErrorDescriptions = allErrorDescriptions.Substring(allErrorDescriptions.IndexOf('=') + 3);

                // Separate everthing by square brackets
                var pattern = @"\[(.*?)\]";
                var matches = Regex.Matches(allErrorDescriptions, pattern);

                foreach (Match m in matches)
                {
                    // Get everything before the comma and assign to key, everything after becomes the value
                    string entry = m.Value;
                    string keyEntry = entry.Substring(2, entry.IndexOf(',') - 2);
                    string valueEntry = entry.Substring(entry.IndexOf(',') + 1);
                    
                    // Remove leading and trailing apostrophes and clean it up
                    keyEntry = keyEntry.Trim();
                    keyEntry = keyEntry.TrimStart('\'').TrimEnd('\'');
                    valueEntry = valueEntry.Trim();
                    valueEntry = valueEntry.Substring(0, valueEntry.Length - 2).TrimStart('\'').TrimEnd('\'');

                    if (!worldSpaceViolationsShortMessages.ContainsKey(keyEntry))
                    worldSpaceViolationsShortMessages.Add(keyEntry, valueEntry);

                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private void LoadServerRules()
        {
            try
            {
                if (SelectedProject != null)
                {
                    HttpWebRequest request = WebRequest.Create(new Uri(_serverUrl + RESTRequestActions.WS_REST_SERVER_RULES_PUBLIC_PROJECT)) as HttpWebRequest;
                    if (!String.IsNullOrEmpty(SelectedProject.organizationName))
                    {
                        request = WebRequest.Create(new Uri(_serverUrl + RESTRequestActions.WS_REST_SERVER_RULES_ORG_PROJECT + SelectedProject.organizationId)) as HttpWebRequest;
                    }
                    
                    request.ContentType = "application/json; charset=utf-8";
                    request.CookieContainer = sessionCookies;
                    request.KeepAlive = true;
                    request.Method = "GET";

                    using (HttpWebResponse resp = request.GetResponse() as HttpWebResponse)
                    {
                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        string result = reader.ReadToEnd();

                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        jsonSerializer.MaxJsonLength = Int32.MaxValue;

                        rules = jsonSerializer.Deserialize<WSRules>(result);
                    }
                }
                    
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private AccessibilityResults Analyze(string url, HTMLDocument document)
        {
            AccessibilityResults results = new AccessibilityResults();
            results.Success = false;
            results.Message = String.Empty;

            try
            {
                AnalysisStatus = WSConstants.WS_ANALYZE_STATUS;
                AnalysisRunning = true;

                if (SelectedProject == null)
                {
                    throw new Exception("Please select a project to analyze.");
                }

                Hashtable parameters = new Hashtable();
                parameters["id"] = SelectedProject.id.ToString();
                parameters["page"] = url;
                parameters["fetchUrl"] = "off"; // fetchUrl ? "on" : "off"; - reserving it for future use
                parameters["store"] = storeIssues ? "on" : "off";
                parameters["analyze"] = "on";
                if (storeIssues)
                    parameters["label"] = issuesLabel;

                {
                    parameters["content"] = GetPageHtml(document);
                    if (parameters["content"].ToString().Trim().Equals(String.Empty))
                    {
                        throw new Exception("Unable to load the page source, please check later.");
                    }
                }

                string analysisResults = GetRESTResponse(RESTRequestActions.WS_REST_ANALYZE_CONTENT_ACTION, "POST", parameters);

                if (String.IsNullOrEmpty(analysisResults))
                {
                    results.Message = "Request was not successful, Please check the error log for any issues.";
                    return results;
                }
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                jsonSerializer.MaxJsonLength = Int32.MaxValue;
                AnalysisResult wsResult = jsonSerializer.Deserialize<AnalysisResult>(analysisResults);

                if (wsResult.success)
                {
                    results.Issues = issueProcessor.PostImportProcess(wsResult.issues, accessibilityIssues, SelectedProject,
                        WorldSpaceViolationsShortMessages, Rules, ViolationLookUpData, IssuesLabel);
                }
                else
                {
                    results.Message = wsResult.message;
                }
                results.Success = wsResult.success;
            }
            catch (Exception ex)
            {
                //parentForm.statusImg.Visible = false;
                //parentForm.statusText.Text = String.Empty;
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                results.Message = ex.Message;
                results.Success = false;
            }
            finally
            {
                AnalysisStatus = String.Empty;
                AnalysisRunning = false;
            }

            return results;
        }

        #endregion

        #region Public Methods

        public void LoadProjects()
        {
            try
            {
                string result = String.Empty;

                if (Login())
                {
                    result = GetRESTResponse(RESTRequestActions.WS_REST_PROJECTS_LIST_ACTION, "GET", null);
                    if (!String.IsNullOrEmpty(result))
                    {
                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        jsonSerializer.MaxJsonLength = Int32.MaxValue;
                        WSProjects WorldSpaceProjectsList = jsonSerializer.Deserialize<WSProjects>(result);

                        Projects = WorldSpaceProjectsList.projects.OrderBy(i => i.organizationName)
                        .ThenBy(i => i.name).ToArray();
                    }
                }

                
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                Projects = null;
                throw ex;
            }
        }

        public bool isSSOSign()
        {
            try
            {
                IsSSOSignIn = false;
                string result = String.Empty;

                if (Login())
                {
                    result = GetRESTResponse(RESTRequestActions.WS_REST_PROJECTS_LIST_ACTION, "GET", null);
                    if (!String.IsNullOrEmpty(result))
                    {
                        IsSSOSignIn = true;
                    }
                }
            }
            catch(Exception ex)
            {
                IsSSOSignIn = false;

            }
            return IsSSOSignIn;
        }

        public void SetActiveProject(long projectID)
        {
            try
            {
                if (Projects == null) LoadProjects();

                WorldSpaceProject project = Projects.FirstOrDefault(i => i.id == projectID);
                if (project != null)
                {
                    SelectedProject = project;
                    ProjectName = SelectedProject.name;
                    LoadProjectDetails();
                }

            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        public bool SSOLogin(string serverURL)
        {
            Status = "Not Logged In";
            _serverUrl = serverURL;
            

            SelectedProject = null;
            ProjectName = "No Project";

            if (SSOLogin())
            {
                LoadProjects();
                return true;
            }
            
            return false;
        }

        public bool Login(string serverURL, string userName, string password)
        {
            Status = "Not Logged In";
            _serverUrl = serverURL;
            _userName = userName;
            _password = password;

            SelectedProject = null;
            ProjectName = "No Project";
            
            if (Login())
            {
                LoadProjects();
                return true;
            }

            return false;
        }

        public bool IsURLValid(string url)
        {
            Uri uri;
            if (!Uri.TryCreate(url + RESTRequestActions.WS_REST_LOGIN_REQUEST_ACTION, UriKind.Absolute, out uri))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void normalizeIssues()
        {
            try
            {
                IList<AccessibilityIssue> objViolations = new List<AccessibilityIssue>();
                IList<AccessibilityIssue> objReviews = new List<AccessibilityIssue>();
                AccessibilityIssue violationIssue = null, reviewIssue= null;
                List<AccessibilityNode> listViolationNodes;;
                List<AccessibilityNode> listReviewNodes = new List<AccessibilityNode>();
                foreach (AccessibilityIssue objIssue in accessibilityIssues)
                {
                    listViolationNodes = new List<AccessibilityNode>();
                    listReviewNodes = new List<AccessibilityNode>();
                    violationIssue = new AccessibilityIssue();
                    reviewIssue = new AccessibilityIssue();
                    foreach (PropertyInfo property in objIssue.GetType().GetProperties())
                    {
                        property.SetValue(violationIssue, property.GetValue(objIssue, null), null);
                        property.SetValue(reviewIssue, property.GetValue(objIssue, null), null);
                    }

                    violationIssue.nodes = null;
                    reviewIssue.nodes = null;
                         
                    foreach (AccessibilityNode objNode in objIssue.nodes)
                    {
                        
                        //List<AccessibilityIssue> issues = tempIssue.nodes.ToList();
                        
                        //objListnodes = tempIssue.nodes.ToList();
                        //objListnodes.RemoveAll(i => !object.ReferenceEquals(i, objNode));
                        //tempIssue.nodes = null;
                        //violationIssue = reviewIssue = tempIssue;
                        ////tempIssue.nodes = objListnodes.ToArray();

                        if (objNode.issueType == IssueTypes.Violations.ToString())
                        {
                            listViolationNodes.Add(objNode);
                        }
                        else if (objNode.issueType == IssueTypes.Review.ToString())
                        {
                            listReviewNodes.Add(objNode);
                        }
                    }
                    violationIssue.nodes = listViolationNodes.ToArray();
                    reviewIssue.nodes = listReviewNodes.ToArray();
                    objViolations.Add(violationIssue);
                    objReviews.Add(reviewIssue);
                }
                violations = objViolations.ToArray();
                incomplete = objReviews.ToArray();

            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        public void Analyze(object sender, DoWorkEventArgs e)
        {
            Dictionary<string, object> parameters = e.Argument as Dictionary<string, object>;
            string message = String.Empty;

            string url = (string)parameters["url"];
            HTMLDocument document = (HTMLDocument)parameters["htmldocument"];

            AccessibilityResults results = Analyze(url, document);

            if (!results.Success)
            {
                Login();
                results = Analyze(url, document);
            }

            e.Result = results;
        }    
        public void DownloadIssues(object sender, DoWorkEventArgs e)
        {
            AccessibilityResults results = new AccessibilityResults();
            try
            {
                results.Success = false;
                results.Message = String.Empty;
                DownloadEventArgs args = e.Argument as DownloadEventArgs;
                
                if (SelectedProject == null || SelectedProject.id != args.selectedProject.id)
                {   
                    //set the new project details everywhere
                    SetActiveProject(args.selectedProject.id);
                }
                Hashtable parameters = new Hashtable();
                parameters.Add("projectID", args.selectedProject.id);
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                jsonSerializer.MaxJsonLength = Int32.MaxValue;
                string jsonString = jsonSerializer.Serialize(parameters);
                string actionURLString = RESTRequestActions.WS_REST_DOWNLOAD_DATA_ACTION + "/" + parameters["projectID"].ToString() + "?assignment="+ selectedUserName + "&url="+ browserUrl + "&label=" + selectedLabel; //+ ":" + parameters["label"].ToString();
                string restResponse = String.Empty;
                if (Login())
                {
                    restResponse = GetRESTResponse(actionURLString, "POST", parameters);
                }
                if (!String.IsNullOrEmpty(restResponse))
                {
                    AttestDownloadResult downloadResult = jsonSerializer.Deserialize<AttestDownloadResult>(restResponse);
                    if (downloadResult.success)
                    {
                        
                        WSServerWorker serverWorker = WSServerWorker.Instance;
                        serverWorker.AccessibilityIssues.Clear();
                        if(downloadResult.violations.Length == 0)
                        {
                            results.Success = false;
                            results.Message = "No issues found.";
                            e.Result = results;
                            return;
                        }
                        else
                        {
                            int violationCount = 0, reviewCount = 0;
                            foreach (AccessibilityIssue vltn in downloadResult.violations)
                            {
                                results.Success = downloadResult.success;
                                vltn.issueType = IssueTypes.Violations.ToString();
                                string temp = "";
                                temp = vltn.help;
                                vltn.help = vltn.description;
                                vltn.description = temp;

                                foreach (AccessibilityNode objNode in vltn.nodes)
                                {
                                    violationCount = violationCount + 1;
                                    string failureSummary = "Fix any of the following:\n";
                                    objNode.issueType = IssueTypes.Violations.ToString();
                                    foreach (AccessibilityFailureMessage failMsg in objNode.any)
                                    {
                                        failureSummary = failureSummary + failMsg.message + "\n";
                                    }
                                    objNode.failureSummary = failureSummary;
                                }
                                serverWorker.AccessibilityIssues.Add(vltn);
                            }

                            foreach (AccessibilityIssue vltn in downloadResult.incomplete)
                            {
                                vltn.issueType = IssueTypes.Review.ToString();
                                string temp = "";
                                temp = vltn.help;
                                vltn.help = vltn.description;
                                vltn.description = temp;
                                foreach (AccessibilityNode objNode in vltn.nodes)
                                {
                                    reviewCount = reviewCount + 1;
                                    string failureSummary = "Fix any of the following:\n";
                                    objNode.issueType = IssueTypes.Review.ToString();
                                    foreach (AccessibilityFailureMessage failMsg in objNode.any)
                                    {
                                        failureSummary = failureSummary + failMsg.message + "\n";
                                    }
                                    objNode.failureSummary = failureSummary;
                                }
                                serverWorker.AccessibilityIssues.Add(vltn);
                            }
                            serverWorker.violationCount = violationCount;
                            serverWorker.needsReviewCount = reviewCount;
                        }
                    }

                    
                }
                e.Result = results;
                
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                results.Success = false;
                results.Message = "Unable to find matching results.";
                e.Result = results;
                
            }
        }

        public string GetHelpText(AccessibilityIssue currentIssue, out bool fallbackToHardcoded)
        {
            if (Login() && issueProcessor.GetHelpTextViaRestCall())
            {
                string restResponse = String.Empty;
                fallbackToHardcoded = false;

                if (currentIssue.violations != null & currentIssue.violations.Length > 0 && currentIssue.project != null)
                {
                    string actionURLString = RESTRequestActions.WS_REST_HELP_TEXT + "?code=" + currentIssue.violations[0].code + "&projectid=" + currentIssue.project.ID;
                    restResponse = GetRESTResponse(actionURLString, "GET", null);
                }
                return restResponse.Replace("/FireEyes/helpfiles/images/", _serverUrl + "helpfiles/images/");
            }
            fallbackToHardcoded = true;
            return String.Empty;
        }

        /// <summary>
        /// Upload accessibility issues to WorldSpace Server
        /// </summary>
        public WSUploadResult UploadIssues()
        {
            WSUploadResult uploadResult = new WSUploadResult();
            try
            {
                AnalysisRunning = true;
                AnalysisStatus = WSConstants.WS_UPLOAD_STATUS;

                IEnumerable<AccessibilityIssue> obsCollection = (IEnumerable<AccessibilityIssue>)accessibilityIssues;
                var list = new List<AccessibilityIssue>(obsCollection);

                AttestUploadParams uploadParams = new AttestUploadParams();
                uploadParams.timestamp = DateTime.Now.ToLongDateString();
                uploadParams.url = browserUrl;
                normalizeIssues();
                uploadParams.violations = violations;
                uploadParams.incomplete = incomplete;


                Hashtable parameters = new Hashtable();
                parameters["projectId"] = SelectedProject.id;
                parameters["label"] = IssuesLabel;
                parameters["axeResult"] = uploadParams;
                
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                jsonSerializer.MaxJsonLength = Int32.MaxValue;
                string jsonString = jsonSerializer.Serialize(parameters);
                byte[] bytes = Encoding.UTF8.GetBytes(jsonString);
                
                string restResponse = String.Empty;
                if (Login())
                {
                    HttpWebRequest request = WebRequest.Create(new Uri(_serverUrl + RESTRequestActions.WS_REST_UPLOAD_DATA_ACTION)) as HttpWebRequest;
                    request.ContentType = "application/json; charset=utf-8";
                    request.CookieContainer = sessionCookies;
                    request.KeepAlive = true;
                    request.Headers.Add("Authorization", "Bearer " + _SSOToken);
                    request.Method = "POST";
                    request.ContentLength = bytes.Length;
                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(bytes, 0, bytes.Count());
                    }

                    using (HttpWebResponse resp = request.GetResponse() as HttpWebResponse)
                    {
                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        string result = reader.ReadToEnd();
                        jsonSerializer = new JavaScriptSerializer();
                        jsonSerializer.MaxJsonLength = Int32.MaxValue;
                        uploadResult = jsonSerializer.Deserialize<WSUploadResult>(result);
                        return uploadResult;
                    }

                }

                }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message,  MethodInfo.GetCurrentMethod().Name);
                uploadResult.success = false;
                throw ex;
            }

            return uploadResult;
        }

        public WSDownloadResourceResult DownloadLabels(string projId)
        {
            WSDownloadResourceResult results = new WSDownloadResourceResult();
            results.success = false;
            try
            {
                Hashtable parameters = new Hashtable();
                parameters.Add("projectID", projId);

                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                jsonSerializer.MaxJsonLength = Int32.MaxValue;
                string jsonString = jsonSerializer.Serialize(parameters);
                string actionURLString = RESTRequestActions.WS_REST_DOWNLOAD_RESOURCES_ACTION + parameters["projectID"].ToString();
                string restResponse = String.Empty;
                if (Login())
                {
                    Uri baseUrl = new Uri(_serverUrl);
                    HttpWebRequest request = WebRequest.Create(new Uri(_serverUrl + actionURLString)) as HttpWebRequest;
                    request.ContentType = "application/json; charset=utf-8";
                    request.CookieContainer = sessionCookies;
                    request.KeepAlive = true;
                    request.Headers.Add("Authorization", "Bearer " + _SSOToken);
                    request.Method = "GET";
                    using (HttpWebResponse resp = request.GetResponse() as HttpWebResponse)
                    {
                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        string result = reader.ReadToEnd();
                        jsonSerializer = new JavaScriptSerializer();
                        jsonSerializer.MaxJsonLength = Int32.MaxValue;
                        results = jsonSerializer.Deserialize<WSDownloadResourceResult>(result);
                        
                    }
                }

                return results;

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
            return results;
        }

        public WorldSpaceProject getProject(string projId)
        {
            WorldSpaceProject results = new WorldSpaceProject();
            try
            {
                Hashtable parameters = new Hashtable();
                parameters.Add("projectID", projId);

                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                jsonSerializer.MaxJsonLength = Int32.MaxValue;
                string jsonString = jsonSerializer.Serialize(parameters);
                string actionURLString = RESTRequestActions.WS_REST_PROJECT_INFO_ACTION + parameters["projectID"].ToString();
                string restResponse = String.Empty;
                if (Login())
                {
                    Uri baseUrl = new Uri(_serverUrl);
                    HttpWebRequest request = WebRequest.Create(new Uri(_serverUrl + actionURLString)) as HttpWebRequest;
                    request.ContentType = "application/json; charset=utf-8";
                    request.CookieContainer = sessionCookies;
                    request.KeepAlive = true;
                    request.Headers.Add("Authorization", "Bearer " + _SSOToken);
                    request.Method = "GET";
                    using (HttpWebResponse resp = request.GetResponse() as HttpWebResponse)
                    {
                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        string result = reader.ReadToEnd();
                        jsonSerializer = new JavaScriptSerializer();
                        jsonSerializer.MaxJsonLength = Int32.MaxValue;
                        results = jsonSerializer.Deserialize<WorldSpaceProject>(result);

                    }
                }

                return results;

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
            return results;
        }

        public WSDownloadRulesResult DownloadCustomRules(string orgId)
        {
            WSDownloadRulesResult results = new WSDownloadRulesResult();
            results.success = false;
            try
            {
                Hashtable parameters = new Hashtable();
                parameters.Add("projectID", orgId);

                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                jsonSerializer.MaxJsonLength = Int32.MaxValue;
                string jsonString = jsonSerializer.Serialize(parameters);
                string actionURLString = RESTRequestActions.WS_REST_DOWNLOAD_RULES_ACTION + "?organizationId=" + parameters["projectID"].ToString() + "&size=1024";
                string restResponse = String.Empty;
                if (Login())
                {
                    Uri baseUrl = new Uri(_serverUrl);
                    HttpWebRequest request = WebRequest.Create(new Uri(baseUrl.GetLeftPart(System.UriPartial.Authority) + actionURLString)) as HttpWebRequest;
                    request.ContentType = "application/json; charset=utf-8";
                    request.CookieContainer = sessionCookies;
                    request.KeepAlive = true;
                    request.Accept = "application/vnd.com.deque.ecp.ruleSetDefinition.v1+json";
                    request.Headers.Add("x-deque-realm-header", "worldspace-comply");
                    request.Headers.Add("Authorization", "Bearer " + _SSOToken);
                    request.Method = "GET";
                    using (HttpWebResponse resp = request.GetResponse() as HttpWebResponse)
                    {
                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        string result = reader.ReadToEnd();
                        jsonSerializer = new JavaScriptSerializer();
                        jsonSerializer.MaxJsonLength = Int32.MaxValue;
                        results = jsonSerializer.Deserialize<WSDownloadRulesResult>(result);
                        results.success = true;
                        downloadRulesResult = results;
                    }
                }
                
                return results;

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                
            }
            return results;
        }

        public AccessibilityIssue CreateNewAccessibilityIssue(string severity)
        {
            AccessibilityIssue accessibilityIssue = new AccessibilityIssue();

            accessibilityIssue.stickyResult = false;
            accessibilityIssue.result = false;
            
            switch (severity)
            {
                case WSIssueSeverity.WS_VIOLATION_ISSUE:
                    accessibilityIssue.severityCode = "level.violation";
                    accessibilityIssue.staticIssue = true;
                    accessibilityIssue.manual = false;
                    break;
                case WSIssueSeverity.WS_POTENTIAL_VIOLATION_ISSUE:
                    accessibilityIssue.severityCode = "level.potential";
                    accessibilityIssue.staticIssue = true;
                    accessibilityIssue.manual = false;
                    break;
                default:
                    accessibilityIssue.severityCode = "level.manual";
                    accessibilityIssue.staticIssue = false;
                    accessibilityIssue.manual = true;
                    break;
            }
            
            accessibilityIssue.msgArgs = "";
            accessibilityIssue.xpathArray = new XPathArrayElement[] { };
            accessibilityIssue.frameXpathArray = new XPathArrayElement[] { };
            accessibilityIssue.frameXpath = "";
            
            accessibilityIssue.serverProjectID = accessibilityIssue.projectID = SelectedProject.id;
            accessibilityIssue.projectMode = "prod";
            
            accessibilityIssue.domainId = SelectedProject.domains[0].id;
            accessibilityIssue.stemmedOwnerurl = accessibilityIssue.stemmedTaburl = accessibilityIssue.stemmedUrl = accessibilityIssue.taburl = accessibilityIssue.url = BrowserURL;
            accessibilityIssue.label = IssuesLabel;
            
            accessibilityIssue.colorContrast = false;
            accessibilityIssue.relatedNodes = new RelatedNode[] { };
            accessibilityIssue.readingOrder = false;

            accessibilityIssue.hasServerId = false;
            accessibilityIssue.practice = "A";
            accessibilityIssue.script = String.Empty;             

            accessibilityIssue.scriptStep = String.Empty;
            accessibilityIssue.sandboxOwner = null;
            accessibilityIssue.page = null;
            
            accessibilityIssue.reportType = "issue";
            accessibilityIssue.issueStage = "Analysis";
            accessibilityIssue.priority = "minor";
            accessibilityIssue.status = "open";
            accessibilityIssue.assignedTo = String.Empty;
            accessibilityIssue.comment = String.Empty;
            accessibilityIssue.date = 0;
                    
            return accessibilityIssue;
        }

        public void CreateNewReadingOrderIssue(string html, string tag, string xpath, long issueDate)
        {
            if (issueProcessor == null) { Login(); }

            AccessibilityIssue orderIssue = CreateNewAccessibilityIssue(WSIssueSeverity.WS_VIOLATION_ISSUE);
            orderIssue.id = orderIssue.category = orderIssue.ruleId = orderIssue.messageCode = "010302-NOCSS";
            if (WorldSpaceViolationsShortMessages.Contains(orderIssue.messageCode))
            {
                orderIssue.description = WorldSpaceViolationsShortMessages[orderIssue.messageCode].ToString();
            }

            ILookup<string, ViolationLookup> lookup = ViolationLookUpData.ToLookup(p => p.ID, p => p);
            if (lookup.Contains(orderIssue.messageCode))
            {
                IGrouping<string, ViolationLookup> vlookup = (IGrouping<string, ViolationLookup>)lookup[orderIssue.messageCode];
                ViolationLookup vl = vlookup.ElementAt<ViolationLookup>(0);
                orderIssue.standard = vl.Standard;
                orderIssue.level = vl.Level;
                orderIssue.category = vl.Category;

            }
            else
            {
                orderIssue.standard = String.Empty;
                orderIssue.level = String.Empty;
                orderIssue.category = String.Empty;
            }

            AccessibilityIssues.Add(issueProcessor.CreateNewReadingOrderIssue(orderIssue, html, tag, xpath, issueDate));
        }

        #endregion
    }

    public class ObservableCollectionEx<T> : ObservableCollection<T>
    {
        // Override the event so this class can access it
        public override event System.Collections.Specialized.NotifyCollectionChangedEventHandler CollectionChanged;

        public AccessibilityIssue[] Contains(Func<object, object> p)
        {
            throw new NotImplementedException();
        }

        public T1[] Contains<T1>(Func<object, object> p)
        {
            throw new NotImplementedException();
        }

        protected override void OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            // Be nice - use BlockReentrancy like MSDN said
            using (BlockReentrancy())
            {
                System.Collections.Specialized.NotifyCollectionChangedEventHandler eventHandler = CollectionChanged;
                if (eventHandler == null)
                    return;

                Delegate[] delegates = eventHandler.GetInvocationList();
                // Walk thru invocation list
                foreach (System.Collections.Specialized.NotifyCollectionChangedEventHandler handler in delegates)
                {
                    DispatcherObject dispatcherObject = handler.Target as DispatcherObject;
                    // If the subscriber is a DispatcherObject and different thread
                    if (dispatcherObject != null && dispatcherObject.CheckAccess() == false)
                    {
                        // Invoke handler in the target dispatcher's thread
                        dispatcherObject.Dispatcher.Invoke(DispatcherPriority.DataBind, handler, this, e);
                    }
                    else // Execute handler as is
                        handler(this, e);
                }
            }
        }
    }
}
