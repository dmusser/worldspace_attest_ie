﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using System.IO;
using System.Reflection;

namespace Deque.WorldSpace.AddIns
{
 
    public class ConfigurationSettings
    {
        static string filename = WSServerWorker.CONFIG_SETTINGS_FOLDER_URL + "\\settings.xml";
        static  XmlDocument settings = null;
        public static XmlDocument loadSettingsFile()
        {
            XmlTextWriter xtw = null;
            try
            {
                if (!(Directory.Exists(WSServerWorker.CONFIG_SETTINGS_FOLDER_URL)))
                {
                    Directory.CreateDirectory(WSServerWorker.CONFIG_SETTINGS_FOLDER_URL);
                }

                if (!File.Exists(filename))
                {
                    File.Create(filename);
                }
                settings = new XmlDocument();
                using (StreamReader rdr = new StreamReader(filename))
                {
                    settings.Load(rdr);
                    rdr.Close();
                }

                XmlElement mainNode = (XmlElement)FindNode(settings.ChildNodes, "WorldSpaceSettings");
                if (mainNode == null)
                {
                    xtw = new XmlTextWriter(filename, System.Text.Encoding.UTF8);
                    xtw.WriteStartDocument();
                    xtw.WriteStartElement("WorldSpaceSettings");
                    xtw.WriteEndElement();
                    xtw.Close();
                }
                
                using (StreamReader rdr = new StreamReader(filename))
                {
                    settings.Load(rdr);
                    rdr.Close();
                }
                return settings;
            }
            catch(Exception ex)
            {
                
                if (filename != null && !filename.Trim().Equals(String.Empty) && File.Exists(filename))
                {
                    try
                    {
                        if (xtw != null)
                            xtw.Close();
                        
                    }
                    catch { }
                }
                return settings;
            }
        }

        public static void saveConfigFile()
        {
            try
            {
                settings.Save(filename);
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        
        public static XmlNode FindNode(XmlNodeList list, string nodeName)
        {
            if (list.Count > 0)
            {
                foreach (XmlNode node in list)
                {
                    if (node.Name.Equals(nodeName)) return node;
                    if (node.HasChildNodes)
                    {
                       XmlNode returnNode = FindNode(node.ChildNodes, nodeName);
                       if (returnNode != null) return returnNode;
                    }
                }
            }
            return null;
        }

        public static XmlNode addChildNode(XmlNode node,string nodename,string nodetext)
        {
            try
            {
                XmlDocument settings = new XmlDocument();
                XmlNode nodeFound = FindNode(node.ChildNodes, nodename);
                if(nodeFound == null)
                {
                    XmlNode newChild = settings.CreateElement(nodename);
                    newChild.InnerText = nodetext;
                    XmlNode importNode = node.OwnerDocument.ImportNode(newChild, true);
                    node.AppendChild(importNode);
                    return newChild;
                }
                else
                {
                    nodeFound.InnerText = nodetext;
                }
                return nodeFound;
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }

        }


        public static XmlNodeList getRuleNodes()
        {
            XmlNodeList rulesList = null;
            try
            {
                loadSettingsFile();
                if (settings != null && settings.ChildNodes.Count > 0)
                { 
                    XmlNode parentNode = ConfigurationSettings.FindNode(settings.ChildNodes, "CustomRules");
                    if (parentNode != null) rulesList = parentNode.ChildNodes;
                }
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
            return rulesList;
        }

        public static XmlNode addComplyRuleChildNode(XmlNode node, string nodename, string nodetext,string ruleId, bool isActive)
        {
            try
            {
                XmlDocument settings = new XmlDocument();
                
                if (nodename.Contains(" "))
                {
                    nodename = nodename.Replace(" ", "___");
                }
                XmlNode nodeFound = FindNode(node.ChildNodes, nodename);
                if (nodeFound == null)
                {
                    XmlNode newChild = settings.CreateElement(nodename);
                    newChild.InnerText = nodetext;
                    ((XmlElement)newChild).SetAttribute("Active", isActive.ToString());
                    ((XmlElement)newChild).SetAttribute("Id", ruleId.ToString());
                    XmlNode importNode = node.OwnerDocument.ImportNode(newChild, true);
                    node.AppendChild(importNode);
                    return newChild;
                }
                else
                {
                    nodeFound.InnerText = nodetext;
                    ((XmlElement)nodeFound).SetAttribute("Active", isActive.ToString());
                }
                return nodeFound;
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }

        }

        public static XmlNode addRuleChildNode(XmlNode node, string nodename, string nodetext)
        {
            try
            {
                XmlDocument settings = new XmlDocument();
                foreach(XmlNode nodeObj in node.ChildNodes)
                {
                    if(nodeObj != null && (nodeObj.GetType() == typeof(XmlElement) || nodeObj.GetType() == typeof(XmlNode)))
                        ((XmlElement)nodeObj).SetAttribute("Active", "false");
                }
                if (nodename.Contains(" "))
                {
                    nodename = nodename.Replace(" ", "___");
                }
                XmlNode nodeFound = FindNode(node.ChildNodes, nodename);
                if (nodeFound == null)
                {
                    
                    XmlNode newChild = settings.CreateElement(nodename);
                    newChild.InnerText = nodetext;
                    ((XmlElement)newChild).SetAttribute("Active", "true");
                    XmlNode importNode = node.OwnerDocument.ImportNode(newChild, true);
                    node.AppendChild(importNode);
                    return newChild;
                }
                else
                {
                    nodeFound.InnerText = nodetext;
                    ((XmlElement)nodeFound).SetAttribute("Active", "true");
                }
                return nodeFound;
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }

        }

        public static XmlNode addNode(string nodeName, string nodeText)
        {
            try {
                
                if (settings.HasChildNodes)
                {
                    XmlElement mainNode = (XmlElement) FindNode(settings.ChildNodes, "WorldSpaceSettings");
                    if (mainNode != null)
                    {
                        XmlNode nodeFound = FindNode(mainNode.ChildNodes, nodeName);
                        if (nodeFound != null)
                        {
                            nodeFound.InnerText = nodeText;
                        }
                        else
                        {
                            nodeFound = settings.CreateElement(nodeName);
                            nodeFound.InnerText = nodeText;
                            mainNode.AppendChild(nodeFound);
                        }

                        return nodeFound;
                    }
                    else
                        return mainNode;
                }
                else
                {
                    XmlNode selProjectNode = settings.CreateElement(nodeName);
                    selProjectNode.InnerText = nodeText;
                    settings.AppendChild(selProjectNode);
                    return selProjectNode;
                }
                
            } catch(Exception ex) {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
            
        }

        public static string getNodeText(string nodeName)
        {
            try
            {
                string nodeText = "";
                if (settings.HasChildNodes)
                {
                    XmlNode nodeFound = FindNode(settings.ChildNodes, nodeName);
                    if (nodeFound != null)
                    {
                        nodeText = nodeFound.InnerText;
                    }
                    
                }
                return nodeText;

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }

        }
    }

}
