function getAllChildrenInReadingOrder( node) {
    var child = node.firstChild;
    var retVal = new Array();
    while( child) {
        retVal.push( child); //getJSObject(child));
        var childElements = getAllChildrenInReadingOrder(child);  //getJSObject(child));
        retVal = retVal.concat( childElements);
        child = child.nextSibling;
    }
    return retVal;
}

function getOffsetParentLeft( element) {
    var parent = element.offsetParent;
    if ( parent != null) {
        var pLeft = parent.offsetLeft;
        pLeft += getOffsetParentLeft( parent);
        return pLeft;
    }
    return 0;
}

function getOffsetParentTop( element) {
    var parent = element.offsetParent;
    if ( parent != null) {
        var pTop = parent.offsetTop;
        pTop += getOffsetParentTop( parent);
        return pTop;
    }
    return 0;
}

function hasTextDirectDescendant( node) {
    var child = node.firstChild;
    while( child) {
        if ( child.nodeName == "#text" && child.wholeText.trim().length > 0) return true;
        child = child.nextSibling;
    }
    return false;
}

function isCurrentElementChildOfPreviousElement( curr, prev) {
    var parent = curr.parentNode;
    while ( parent) {
        if ( parent == prev) {
            return true;
        }
        if ( parent.nodeName.toLowerCase() == "body" || parent == parent.parentNode) {
            break;
        }
        parent = parent.parentNode;
    }
    return false;
}

function getElementCoordinates(elt) {
    var coords = {
        "top": 0,
        "right": 0,
        "bottom": 0,
        "left": 0,
        "width": 0,
        "height": 0
    };

    var frameCoords = coords;
     
    if (elt) {
        var win = elt.ownerDocument.defaultView.top;
        var xOffset = win.scrollX;
        var yOffset = win.scrollY;
        if (win && win.frames.length > 0) {
            for (var i = 0; i < win.frames.length; i++) {
                try {
                    if (win.frames[i].document == elt.ownerDocument) {
                        frameCoords = win.frames[i].frameElement.getBoundingClientRect();
                        break;
                    }
                }
                catch (err) { }
            }
        }

        var rect = elt.getBoundingClientRect();        

        coords = {
            "top": rect.top + frameCoords.top + yOffset,
            "right": rect.right + frameCoords.right + xOffset,
            "bottom": rect.bottom + frameCoords.bottom + yOffset,
            "left": rect.left + frameCoords.left + xOffset,
            "width": rect.right - rect.left,
            "height": rect.bottom - rect.top
        };
    }
    return coords;
};

function CheckReadingOrder(win) {
    var vltns = new Array();
     
    //Get all the objects in the DOM to check
    if ( typeof(win.nodeName) == 'undefined') {
        var doc = win.document;
    } else {
        var doc = win ;
    } 

    var prevTop = 0;
    var prevLeft = 0;
    var prevHeight = 0;
    var prevWidth = 0;
    var prevIndex = 0;
    
    objects = getAllChildrenInReadingOrder(doc.body); // getJSObject(doc.body));
    //dump( objects);
   
    for (var i = 0; i < objects.length; i++) {      
        if (objects[i].nodeName == '#text' || objects[i].tagName == "OPTION") {
            continue;
        }
        try {
            //dumpObject( objects[i]);
            try {
                if ( !isNotOffScreenOrHidden( objects[i]) || objects[i].offsetParent == null) {
                    //dump( "NOT VISIBLE\n");
                    continue;
                }
                if ( !(doesContainInvisibleAltText( objects[i]) || hasTextDirectDescendant(objects[i]))) {
                    //dump( "doesContainInvisibleAltText( objects[i]):"+doesContainInvisibleAltText( objects[i])+", hasTextDirectDescendant(objects[i]:"+hasTextDirectDescendant(objects[i])+"\n");
                    continue;
                }
            } catch( testerr) {
                //dump( testerr+"\n");
            }
           
/*          var elCoords = getElementCoordinates( objects[i]);
          var currentLeft = elCoords.left;
          var currentTop = elCoords.top;
          var currentWidth = elCoords.width;
          var currentHeight = elCoords.height;

          alert(currentLeft + " , " + currentTop + ", " + currentWidth + ", " + currentHeight);*/

            var currentLeft = objects[i].offsetLeft + getOffsetParentLeft(objects[i]);
            var currentTop = objects[i].offsetTop + getOffsetParentTop(objects[i]);
            var currentWidth = objects[i].offsetWidth;
            var currentHeight = objects[i].offsetHeight;
           
            if ( currentTop >=0 && currentLeft >=0 && currentWidth > 1 && currentHeight > 1) {
                // If the element is not positioned off screen
                var verticalPrecedence;
                var horizontalPrecedence;
                var GRACE_DISTANCE = 5; // distance we allow an element to be behind before we start complaining - allows for many small alignment errors that do not affect accessibility
                verticalPrecedence = (currentTop < (prevTop-GRACE_DISTANCE)) && ((currentTop+currentHeight) < (prevTop+prevHeight));
                horizontalPrecedence = (currentLeft < (prevLeft-GRACE_DISTANCE)) && ((currentLeft+currentWidth) < (prevLeft+prevWidth));
                var overlappingVertically = (currentTop >= prevTop && currentTop <= (prevTop+prevHeight-GRACE_DISTANCE)) ||
                                            ((currentTop+currentHeight) >= prevTop && (currentTop+currentHeight) <= (prevTop+prevHeight-GRACE_DISTANCE));
                var overlappingHorizontally = (currentLeft >= prevLeft && currentLeft <= (prevLeft+prevWidth-GRACE_DISTANCE)) ||
                                            ((currentLeft+currentWidth) >= prevLeft && (currentLeft+currentWidth) <= (prevLeft+prevWidth-GRACE_DISTANCE));
                var overlappingVerticallyHorizontalPrecedence = (overlappingVertically && horizontalPrecedence);
                var overlappingHorizontallyVerticalPrecedence = (verticalPrecedence && overlappingHorizontally);
                var bothUpAndBack = (currentTop < (prevTop - GRACE_DISTANCE) && currentLeft < (prevLeft - GRACE_DISTANCE));

                
                if ( overlappingHorizontallyVerticalPrecedence || overlappingVerticallyHorizontalPrecedence || bothUpAndBack) {
                    if ( !isCurrentElementChildOfPreviousElement( objects[i], objects[prevIndex])) {                         
                        // We have a violation
                        var vltn = new Violation(objects[i], '', '');
                        vltn.node = objects[i];
                        vltn.relatedNodes = [objects[prevIndex]];
                        vltn.xPath = getElementXPath(objects[i]);                       
                        vltns.push( vltn);
                        //debugOut( "Reading Order violation\n");
                        //debugOut( objects[i].nodeName+"\n");
                        //debugOut( "verticalPrecedence:"+verticalPrecedence+", horizontalPrecedence:"+horizontalPrecedence+", overlappingVertically:"+overlappingVertically+", overlappingVerticallyHorizontalPrecedence:"+overlappingVerticallyHorizontalPrecedence+"\n");
                        //debugOut( "prevTop:"+prevTop+", prevLeft:"+prevLeft+", prevHeight:"+prevHeight+", prevWidth:"+prevWidth+"\n");
                        //debugOut( "currentTop:"+currentTop+", currentLeft:"+currentLeft+", currentHeight:"+currentHeight+", currentWidth:"+currentWidth+"\n");
                    }
                }
                prevIndex = i;
                prevTop = currentTop;
                prevLeft = currentLeft;
                prevHeight = currentHeight;
                prevWidth = currentWidth;
            } else {
                //dump( "Off screen\n");
            }
        } catch( err) {
            //dump( err+"\n");
        }
    }
    
    return vltns;
}

function Violation(vltn, addtnlInfo, xpath) {
    this.vltn = vltn;
    this.addtnlInfo = addtnlInfo;
    this.xPath = xpath;
    this.getNode = function () { return this.vltn; }
    this.getAdditionalInfo = function () { return this.addtnlInfo; }
    this.getXPath = function () { return this.xPath; }
}

getElementXPath = function (element) {
    if (element && element.id)
        return '//*[@id="' + element.id + '"]';
    else
        return getElementTreeXPath(element);
}; 

getElementTreeXPath = function (element) {
    var paths = [];

    for (; element && element.nodeType == 1; element = element.parentNode) {
        var index = 0;
        for (var sibling = element.previousSibling; sibling; sibling = sibling.previousSibling) {
            if (sibling.nodeName == element.nodeName)
                ++index;
        }

        var tagName = element.nodeName.toLowerCase();
        var pathIndex = (index > 0 ? "[" + (index + 1) + "]" : "");
        paths.splice(0, 0, tagName + pathIndex);
    }

    return paths.length ? "//" + paths.join("/") : null;
};


var vltns;

function CheckForReadingOrder(win) {
    vltns = CheckReadingOrder(win);
    for (var i = 0; i < vltns.length; i++) {  
        if (vltns[i].relatedNodes.length > 0) {
            vltns[i].addtnlInfo = getElementXPath(vltns[i].relatedNodes[0]);
        }
    }
    return vltns;
}

function GetViolation(ind) {
    return vltns[ind];
}