﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Deque.WorldSpace.AddIns
{
    public sealed class FilterItems
    {

    }
    
    public sealed class WSRules
    {
        public bool success { get; set; }
        public List<WSServerRule> rules { get; set; }
        public List<WSMessage> messages { get; set; }
    }

    public sealed class WSActiveRule
    {
        public string ruleName;
        public string ruleJSON;
        public string ruleType;
    }

    public sealed class WSMessage
    {
        public string issueId;
        public string defaultDescription;
        public string standardLabel;
        public string customMessage;
    }

    public sealed class WSServerRule
    {
        // public string class { get; set; }
        public long id { get; set; }
        //public string defaultDescription { get; set; }
        //public string descriptionLookup { get; set; }
        //public bool hasOptionValue { get; set; }
        //public bool isCustom { get; set; }
        //public bool isScanLevel { get; set; }
        //public string javaClass { get; set; }
        //public string javaMethod { get; set; }
        //public string javaMethodStatic { get; set; } //bool or string ?
        //public string javaScript { get; set; }
        //public WSOptionStructure optionStructure { get; set; }
        //public string optionValueDefault { get; set; }
        //public WSOrganization organization { get; set; }
        //public string practice { get; set; }
        //public string ruleDescription { get; set; }
        //public string ruleHelp { get; set; }
        public string ruleId { get; set; }
        //public string serviceUrl { get; set; }
        //public bool usesOptionStructure { get; set; }
        //public string xpaths { get; set; }
    }

    public sealed class WSOrganization
    {
        public string id { get; set; }
    }

    public sealed class WSScript
    {
        public WSScriptStep[] events { get; set; }
        public string scriptName { get; set; }
        public double scriptScalingFactor { get; set; }
        public long serverProjectID { get; set; }
    }

    public sealed class WSScriptStep
    {
        public long date { get; set; }
        public long timedelta { get; set; }
        public string type { get; set; }
        public string xpath { get; set; }
        public string frameXpath { get; set; }
        public string url { get; set; }
        public string ownerurl { get; set; }
        public string taburl { get; set; }
        public string eventInfo { get; set; }
        public string projectMode { get; set; }
        public string guid { get; set; }
    }

    public sealed class WSLogin
    {
        public bool authenticated { get; set; }
        public string version { get; set; }
    }

    public sealed class WSProjects
    {
        public bool success { get; set; }
        public WorldSpaceProject[] projects { get; set; }
    }

    public sealed class WSResources
    {
        public bool success { get; set; }
        public ArrayList labels { get; set; }
        public ArrayList usernames { get; set; }
        public WSScript[] scripts { get; set; }
        public WorldSpaceProject project { get; set; }
        public string organizationName { get; set; }
        public long organizationId { get; set; }
        public bool useOrganizationStandard { get; set; }
        public bool enforceProjectOptions { get; set; }
    }

    public sealed class WSProjectDomain
    {
        public long id { get; set; }
        public string production { get; set; }
        public string qa { get; set; }
    }

    public sealed class WorldSpaceProject : IEquatable<WorldSpaceProject>
    {
        public long id { get; set; }
        public string name { get; set; }
        public WSProjectDomain[] domains { get; set; }
        public string organizationName { get; set; }

        public string displayName {
            get {
                return organizationName + "-" + name;
            }
        }
        public string organizationId { get; set; }
        public string useOrganizationStandard { get; set; }
        public string enforceProjectOptions { get; set; }
        public string eventOptions { get; set; }
        public string orgProjName
        {
            get
            {
                if (!string.IsNullOrEmpty(organizationId))
                {
                    return organizationName + " - " + name;
                }
                else
                {
                    return name;
                }
            }
        }

        public string ruleSetDefinitionId
        {
            get; set;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj as WorldSpaceProject);
        }

        public bool Equals(WorldSpaceProject other)
        {
            if (other.id == this.id)
            {
                return true;
            }
            return false;
        }

        public static bool operator ==(WorldSpaceProject left, WorldSpaceProject right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(WorldSpaceProject left, WorldSpaceProject right)
        {
            return !Equals(left, right);
        }
    }

    public sealed class WSProjectInfo
    {
        public bool success { get; set; }
        public WSStandardOption[] standardOptions { get; set; }
    }

    public sealed class WSStandardOption
    {
        public string id { get; set; }
        public bool enabled { get; set; }
        public string practice { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        public bool radio { get; set; }
        public string optionid { get; set; }
        public string visibleStr { get; set; }
        public WSStandardOption[] children { get; set; }
        public bool usesOptionStructure { get; set; }
        public WSOptionStructure[] optionStructure { get; set; }
    }

    public sealed class WSOptionStructure
    {
        public string id { get; set; }
        public bool enabled { get; set; }
        public string elementName { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        public string optionid { get; set; }
        public string label { get; set; }
        public string uiElement { get; set; }
        public string javaRule { get; set; }
    }

    public sealed class ProjectEventOptions
    {
        public bool enabled { get; set; }
        public string name { get; set; }
        public SingleEventOption[] children { get; set; }
    }

    public sealed class SingleEventOption
    {
        public bool enabled { get; set; }
        public string name { get; set; }
        public SingleEventOption[] children { get; set; }
        public string optionid { get; set; }
    }

    public sealed class AnalysisResult
    {
        public bool success { get; set; }
        public long projectid { get; set; }
        public string id { get; set; }
        public int issuetotalcount { get; set; }
        public int issueuniquecount { get; set; }
        public int issueviolationcount { get; set; }
        public int issuemanualcount { get; set; }
        public AccessibilityIssue[] issues { get; set; }
        public string message { get; set; }
    }

    public sealed class WSUploadResult
    {
        public bool success { get; set; }
    }

    public sealed class WSDownloadResult
    {
        public bool success { get; set; }
        public AccessibilityIssue[] issues { get; set; }
    }

    public sealed class WSDownloadResourceResult
    {
        public bool success { get; set; }
        public string[] labels { get; set; }
        public string[] usernames { get; set; }
        public string[] scripts { get; set; }
        
    }

    public sealed class WSDownloadRulesResult
    {
        public bool success { get; set; }
        public bool first { get; set; }
        public bool last { get; set; }
        public int numberOfElements { get; set; }
        public int size { get; set; }
        public int totalElements { get; set; }
        public int totalPages { get; set; }

        public WSDownloadRule[] content { get; set; }
    }

    public sealed class WSDownloadRule
    {
        public string axeConfiguration { get; set; }

        public string name { get; set; }

        public string axeVersion { get; set; }

        public bool enableExperimentalTag { get; set; }

        public string id { get; set; }

        public int organizationId { get; set; }

        public int ruleSetVersion { get; set; }

        public string type { get; set; }

        public string[] standards { get; set; }

        public string[] tags { get; set; }



    }

    public sealed class AttestDownloadResult
    {
        public bool success { get; set; }
        public AccessibilityIssue[] violations { get; set; }

        public AccessibilityIssue[] incomplete { get; set; }

        public AccessibilityIssue[] passes { get; set; }
    }

    public sealed class AttestUploadParams
    {
        public string timestamp { get; set; }

        public string url { get; set; }

        public AccessibilityIssue[] violations { get; set; }

        public AccessibilityIssue[] incomplete { get; set; }

        public AccessibilityIssue[] passes { get; set; }
    }

    public sealed class AccessibilityRule
    {
        public string id { get; set; }
        public bool hasOptionValue { get; set; }
        public bool isCustom { get; set; }
        public bool isScanLevel { get; set; }
        public string javaScript { get; set; }
        public string optionStructure { get; set; }
        public string optionValueDefault { get; set; }
        public string organization { get; set; }
        public string practice { get; set; }
        public string ruleDescription { get; set; }
        public string ruleHelp { get; set; }
        public string ruleId { get; set; }
        public string serviceUrl { get; set; }
        public bool usesOptionStructure { get; set; }
        //add violations again?!?
        public string xpaths { get; set; }
        public bool isActive { get; set; }

    }

    public sealed class AccessibilityStandard
    {
        public string id { get; set; }
        public string standard { get; set; }
        public string category { get; set; }
        public string level { get; set; }
        public string shortDescription { get; set; }
        public string longDescription { get; set; }
    }

    public sealed class AccessibilityStandardOption
    {
        public string id { get; set; }
        public string optioncode { get; set; }
        public AccessibilityStandard standardNode { get; set; }
    }
    
    public sealed class AccessibilityViolation
    {
        public string id { get; set; }
        public string practice { get; set; }
        public string grouping { get; set; }
        public string code { get; set; }
        public string fullDescription { get; set; }
        public string help { get; set; }
        public string defaultSeverity { get; set; }
        public string defaultPriority { get; set; }
        public AccessibilityRule[] rules { get; set; }
        public AccessibilityStandardOption[] standardOptions { get; set; }
    }

    public  class AccessibilityNode
    {
        public AccessibilityFailureMessage[] any { get; set; }

        public string html { get; set; }

        public string issueType { get; set; }

        public string impact { get; set; }

        public string[] target { get; set; }

        //public string[] all { get; set; }

        public string[] xpath { get; set; }

        //public string[] none { get; set; }

        public string message { get; set; }

        public string failureSummary { get; set; }

    }


    public sealed class AccessibilityFailureMessage
    {
        public string id { get; set; }

        public string message { get; set; }

        public string impact { get; set; }

        //public string[] data { get; set; }

        public AccessibilityRelatedNode[] relatedNodes { get; set; }
        
    }

    public sealed class AccessibilityRelatedNode
    {
        public string html { get; set; }

        public string[] target { get; set; }
    }


    public enum IssueTypes
    {
        Violations,
        Review,
        Rejected
    }

    public enum IssueImpacts
    {
        Serious,
        Critical,
        Moderate,
        Minor
    }

    public enum RuleTypes
    {
        Tag,
        Rule
    }

    


    public sealed class WorldSpaceAttestProject : INotifyPropertyChanged
    {
        public long id { get; set; }
        public string name { get; set; }

        public string organizationName { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public partial class AccessibleLabelsCount
    {
        public string LabelID { get; set; }
        public string Name { get; set; }
        public int count { get; set; }
    }

    public sealed class AccessibilityIssue : INotifyPropertyChanged
    {
        //schema from FireEyes
        public string id { get; set; }
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged("description");
            }
        } private string _description;
        public string url { get; set; }

        
        public string category { get; set; }

        public string help { get; set; }

        public string helpUrl { get; set; }

        public string issueType
        {
            get; set;
        }

        public string impact { get; set; }

        public AccessibilityNode[] nodes { get; set; }

        public bool stickyResult { get; set; }
        public string ruleId
        {
            get { return _ruleId; }
            set { _ruleId = value; }
        } private string _ruleId = "manual.issue";
        public string severityCode { get; set; }
        public string messageCode
        {
            get { return _messageCode; }
            set { _messageCode = value; }
        } private string _messageCode = "manual.issue.just.added";
        public object msgArgs { get; set; } //string[]
        public string xpath
        {
            get { return _xpath; }
            set
            {
                _xpath = value;
                OnPropertyChanged("xpath");
            }
        } private string _xpath = String.Empty;
        public string frameXpath { get; set; }
        public long date { get; set; }
        public string ownerurl { get; set; }
        public string taburl { get; set; }
        public bool readingOrder { get; set; }
        public bool colorContrast { get; set; }
        public string html
        {
            get { return _html; }
            set
            {
                _html = value;
                OnPropertyChanged("html");
            }
        } private string _html;
        public string status { get; set; }

        public string[] tags
        {
            get; set;
        }
        public string tag
        {
            get { return _tag; }
            set
            {
                _tag = value;
                OnPropertyChanged("tag");
            }
        } private string _tag;
        public string comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
                OnPropertyChanged("comment");
            }
        } private string _comment = String.Empty;
        public string projectMode { get; set; }
        public bool manual { get; set; }
        public bool staticIssue { get; set; }
        public string sandboxOwner { get; set; }
        public string assignedTo { get; set; }
        public string script { get; set; }
        public string scriptStep { get; set; }
        public RelatedNode[] relatedNodes { get; set; }
        public Project project { get; set; }
        public Page page { get; set; }
        public Domain domain { get; set; }
        public int weight { get; set; }
        public string standard { get; set; }
        public string level { get; set; }
        public string label { get; set; }
        public string priority { get; set; }
        public AccessibilityViolation[] violations { get; set; }

        //for update
        public bool result { get; set; }
        public XPathArrayElement[] xpathArray { get; set; }
        public XPathArrayElement[] frameXpathArray { get; set; }
        public string reportType { get; set; }
        public long uniqueId { get; set; }
        public long projectID { get; set; }
        public string eventGuid { get; set; }
        public bool reportDisplayed { get; set; }
        public bool currentDisplayed { get; set; }
        public long domainId { get; set; }
        public string stemmedUrl { get; set; }
        public string stemmedOwnerurl { get; set; }
        public string stemmedTaburl { get; set; }
        public long serverProjectID { get; set; }
        public bool hasServerId { get; set; }
        public string issueStage { get; set; }
        public string practice { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }


        public override string ToString()
        {
            return description;
        }
    }

    public sealed class XPathArrayElement
    {
        public string id { get; set; }
        public string str { get; set; }
        public int count { get; set; }
    }

    public sealed class RelatedNode
    {
        #region variables

        public string id { get; set; }
        public string html { get; set; }
        public string xpath { get; set; }
        public string frameXpath { get; set; }
        public AccessibilityIssue issue { get; set; }

        #endregion
    }

    public sealed class Page
    {
        private string id;
        private string url;

        /// <summary>
        /// 
        /// </summary>
        public string ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string URL
        {
            get
            {
                return url;
            }
            set
            {
                url = value;
            }
        }
    }

    public sealed class Project
    {
        private string id;
        private string name;
        private string orgName;

        /// <summary>
        /// 
        /// </summary>
        public string ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string organizationName
        {
            get
            {
                return orgName;
            }
            set
            {
                orgName = value;
            }
        }
    }

    public sealed class Domain
    {
        private string id;
        private string name;

        /// <summary>
        /// 
        /// </summary>
        public string ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
    }

    public class WSHelp
    {
        #region variables

        string fileName;
        string message;
        string content;

        #endregion

        #region properties

        /// <summary>
        /// 
        /// </summary>
        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                content = value;
            }
        }

        #endregion

        #region constructor

        /// <summary>
        /// /
        /// </summary>
        public WSHelp()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="msg"></param>
        /// <param name="text"></param>
        public WSHelp(string file, string msg, string text)
        {
            fileName = file;
            message = msg;
            content = text;
        }

        #endregion
    }

    public class ViolationLookup
    {
        #region Variables

        private string id;
        private string category;
        private string level;
        private string standard;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vID"></param>
        /// <param name="vCategory"></param>
        /// <param name="vLevel"></param>
        /// <param name="vStandard"></param>
        public ViolationLookup(string vID, string vCategory, string vLevel, string vStandard)
        {
            id = vID;
            category = vCategory;
            level = vLevel;
            standard = vStandard;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Category
        {
            get
            {
                return category;
            }
            set
            {
                category = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Level
        {
            get
            {
                return level;
            }
            set
            {
                level = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Standard
        {
            get
            {
                return standard;
            }
            set
            {
                standard = value;
            }
        }

        #endregion
    }

    public class ProjectListItem
    {
        #region Variables

        private long id;
        private string name = String.Empty;
        private string organizationId;
        private string organizationName = String.Empty;
        private WSProjectDomain[] domains;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="sname"></param>
        public ProjectListItem(long sid, string sname)
        {
            id = sid;
            name = sname;
        }

        /// <summary>
        /// 
        /// </summary>
        public ProjectListItem()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public long ID
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string OrganizationID
        {
            get
            {
                return this.organizationId;
            }
            set
            {
                this.organizationId = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string OrganizationName
        {
            get
            {
                return this.organizationName;
            }
            set
            {
                this.organizationName = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public WSProjectDomain[] Domains
        {
            get
            {
                return domains;
            }
            set
            {
                domains = value;
            }
        }

        #endregion
    }

    public class IssueFilter
    {
        #region Variables

        private string filterType;
        private string filterVal;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fType"></param>
        /// <param name="fVal"></param>
        public IssueFilter(string fType, string fVal)
        {
            filterType = fType;
            filterVal = fVal;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string FilterType
        {
            get
            {
                return filterType;
            }
            set
            {
                filterType = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string FilterValue
        {
            get
            {
                return filterVal;
            }
            set
            {
                filterVal = value;
            }
        }

        #endregion
    }

    public class IssueTag
    {
        #region Variables

        private string tagType;
        private string tagName;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fType"></param>
        /// <param name="fVal"></param>
        public IssueTag(string tType, string tName)
        {
            tagType = tType;
            tagName = tName;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string TagType
        {
            get
            {
                return tagType;
            }
            set
            {
                tagType = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string TagName
        {
            get
            {
                return tagName;
            }
            set
            {
                tagName = value;
            }
        }

        #endregion
    }

    public class DownloadEventArgs : EventArgs
    {
        public WorldSpaceProject selectedProject { get; set; }
        public string selectedType { get; set; }
        public string selectedMode { get; set; }
        public string selectedSeverity { get; set; }
        public string selectedPriority { get; set; }
        public string selectedLabel { get; set; }
        public string selectedStatus { get; set; }
        public string selectedAssignedTo { get; set; }
        public string selectedURLPhrase { get; set; }
    }

    public class AccessibilityResults
    {
        public int violationCount;
        public int needsReviewCount;
        public bool Success { get; set; }
        public string Message { get; set; }
        public AccessibilityIssue[] Issues { get; set; }


    }
}
