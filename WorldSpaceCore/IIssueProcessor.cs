﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deque.WorldSpace.AddIns
{
    interface IIssueProcessor
    {
        AccessibilityIssue[] PostImportProcess(AccessibilityIssue[] issues,
            ObservableCollectionEx<AccessibilityIssue> accessibilityIssues,
            WorldSpaceProject selectedProject,
            Hashtable worldSpaceViolationsShortMessages,
            WSRules rules,
            List<ViolationLookup> violationLookUpData,
            string issuesLabel);

        
        ArrayList PreExportProcess(ArrayList issues, WorldSpaceProject selectedProject);

        bool GetHelpTextViaRestCall();

        AccessibilityIssue CreateNewReadingOrderIssue(AccessibilityIssue orderIssue, string html, string tag, string xpath, long issueDate);
    }
}
