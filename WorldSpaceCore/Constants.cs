﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deque.WorldSpace.AddIns
{
    public enum LoggingLevels
    {
        FATAL,
        ERROR,
        WARN,
        INFO,
        DEBUG
    };

    public class WSImages
    {
        public const string WS_EXCLAMATION_RED = "/WorldSpaceAttest;component/Images/exclamation-red.png";
        public const string WS_HAND_PROPERTY = "/WorldSpaceAttest;component/Images/hand-property.png";
        public const string WS_SYSTEM_MONITOR = "/WorldSpaceAttest;component/Images/system-monitor.png";
        public const string WS_COLOR = "/WorldSpaceAttest;component/Images/color.png";
        public const string WS_ORDER = "/WorldSpaceAttest;component/Images/book.png";
        public const string WS_QUESTION = "/WorldSpaceAttest;component/Images/question.png";
        public const string WS_TOGGLE_EXPAND = "../Images/toggle-expand.png";
        public const string WS_TOGGLE = "../Images/toggle.png";
        public const string WS_STATUS_ONLINE = "../Images/status.png";
        public const string WS_STATUS_OFFLINE = "../Images/status-offline.png";
    }

    public class WSIssueSeverity
    {
        public const string WS_MANUAL_ISSUE = "manual";
        public const string WS_VIOLATION_ISSUE = "critical";
        public const string WS_POTENTIAL_VIOLATION_ISSUE = "minor";
    }

    public class WSIssuePriority
    {
        public const string WS_CRITICAL_ISSUE = "critical";
        public const string WS_SERIOUS_ISSUE = "serious";
        public const string WS_MODERATE_ISSUE = "moderate";
        public const string WS_MINOR_ISSUE = "minor";
    }

    public class WSIssueTags
    {
        public const string WS_HTML_TAG = "html";
        public const string WS_SCRIPT_TAG = "script";
        public const string WS_INPUT_TAG = "input";
        public const string WS_DIV_TAG = "div";
        public const string WS_ANCHOR_TAG = "a";
        public const string WS_TABLE_TAG = "table";
        public const string WS_IMG_TAG = "img";
    }

    public class WSIssueTagTypes
    {
        public const string WS_GENERAL_TAG = "General";
        public const string WS_DOCUMENT_TAG = "Document-level";
        public const string WS_FORMS_TAG = "Forms";
        public const string WS_TABLES_TAG = "Tables";
        public const string WS_HEADERS_TAG = "Headers";
        public const string WS_HTML5_TAG = "Html5";
        public const string WS_LIST_TAG = "List";
        public const string WS_INLINE_TAG = "Inline elements";
        public const string WS_BLOCK_TAG = "Block elements";
        public const string WS_EMBEDDED_TAG = "Embedded elements";
    }

    public class WSConstants
    {
        public const string IQID_SC243RDO_Q1 = "The tab/focus order of the document may not be meaningful,Is the tab/focus order of the document meaningful?";
        public const string IQID_SC132RDO_Q1 = "The reading order of the document may not match the positioning of the elements on the page,Does the reading order of the document match the positioning of the elements on the page?";
        public const string IQID_020201_A = "Poor visibility between text and background colors";
        public const int WS_REQUEST_TIMEOUT = 60 * 1000 * 10;
        public const int WS_LOGINTIMER_REQUEST_TIMEOUT = 60 * 1000 * 9;
        public const string WS_VIOLATION_TYPE = "level.violation";
        public const string WS_POTENTIAL_VIOLATION_TYPE = "level.manual";
        public const string WS_MANUAL_ISSUE_PANEL_NAME = "newManualIssuePanel";
        public const int WS_TEXT_FONT_SIZE = 7;
        public const string WS_ADD_SINGLE_EVENT_PANEL_NAME = "ADD_SINGLE_EVENT_PANEL";
        public const string WS_ADDIN_NAME = "WorldSpace Attest";
        public const int WS_DGVACCESSIBILITY_TOP = 120;
        public const string WS_ANALYZE_STATUS = "Analyzing...";
        public const string WS_ANALYZE_STATUS_AUTOMATION = "Analyzing";
        public const string WS_PREPARING_PREPARING_REQUEST_AUTOMATION = "Preparing request";
        public const string WS_PREPARING_REQUEST_STATUS = "Preparing request...";
        public const string WS_NO_STATUS = "";
        public const string WS_COLOR_CONTRAST_STATUS = "Performing color contrast analysis...";
        public const string WS_COLOR_CONTRAST_STATUS_AUTOMATION = "Performing color contrast analysis";
        public const string WS_ORDER_STATUS = "Performing reading order analysis...";
        public const string WS_ORDER_STATUS_AUTOMATION = "Performing reading order analysis";     
        public const string WS_TRI_STATE_CHECKED_TOOLTIP = "tri state checked";
        public const string WS_TRI_STATE_NOT_CHECKED_TOOLTIP = "tri state not checked";
        public const string WS_FILTER_SEVERITY = "severity";
        public const string WS_FILTER_PRIORITY = "priority";
        public const string WS_FILTER_TAGS = "tags";
        public const string WS_POTENTIAL_VIOLATION_TOOLTIP = "Potential Violation";
        public const string WS_VIOLATION_TOOLTIP = "Violation";
        public const string WS_COLOR_TOOLTIP = "from color contrast analysis";
        public const string WS_ORDER_TOOLTIP = "from reading order analysis";
        public const string WS_STATIC_ANALYSIS_TOOLTIP = "from static analysis";
        public const string WS_MANUAL_TOOLTIP = "issue was manually entered";
        public const string WS_MANUAL_VIOLATION_TOOLTIP = "Manual verification";
        public const string WS_UPLOAD_STATUS = "Uploading issues...";
        public const string WS_EVENTVIEWER_MESSAGE = "please check the event viewer log for exact errors";
        public const string WS_WCAGA_DESC = "wcag 2.0 level a";
        public const string WS_WCAGA_JSON = "wcag2a";
        public const string WS_SECTION508_DESC = "Section 508";
        public const string WS_SECTION508_JSON = "section508";
        public const string WS_WCAGAA_DESC = "WCAG 2.0 AA";
        public const string WS_Blank_URL = "about:blank";
        public const string WS_SSO_Realm = "worldspace-comply";
        public const string WS_SSO_ClientId = "attest";
        

        public class WSErrorMessages
        {
            public const string WS_Connection_Issue = "There was a problem having connection.Please reconnect again.";
        }

    }

    public class RESTRequestActions
    {
        public const string WS_SSO_LOGIN_URL = "https://sso.dequelabs.com/auth/realms/worldspace-comply/protocol/openid-connect/auth?response_type=token&scope=&nonce=&client_id=attest&redirect_uri=https%3A%2F%2Fdev.dequelabs.com/*";
        public const string WS_SSO_LOGOUT_URL = "https://sso.dequelabs.com/auth/realms/worldspace-comply/protocol/openid-connect/logout";
        public const string WS_REST_LOGIN_REQUEST_ACTION = "account//login";
        public const string WS_REST_PROJECTS_LIST_ACTION = "projects";
        public const string WS_REST_ANALYZE_CONTENT_ACTION = "pages";
        public const string WS_REST_PROJECT_ACCESSIBILITY_OPTIONS_ACTION = "project/show/";
        public const string WS_REST_ORGPROJECT_ACCESSIBILITY_OPTIONS_ACTION = "organizationProject/policies/";
        public const string WS_REST_UPLOAD_DATA_ACTION = "issues/fromAxe/save";
        public const string WS_REST_UPLOAD_SCRIPTS_ACTION = "issues";
        public const string WS_REST_PROJECT_RESOURCES_ACTION = "projects/resources/";
        public const string WS_REST_PROJECT_INFO_ACTION = "projects/";
        public const string WS_REST_DOWNLOAD_DATA_ACTION = "issues/forAxe";
        public const string WS_REST_DOWNLOAD_RESOURCES_ACTION = "projects/resources/";
        public const string WS_REST_DOWNLOAD_RULES_ACTION = "/custom-rules/ruleSetDefinitions";
        public const string WS_REST_SERVER_RULES_PUBLIC_PROJECT = "rules";
        public const string WS_REST_SERVER_RULES_ORG_PROJECT = "rules/org/";
        public const string WS_REST_HELP_TEXT = "issue/help";

    }

}
