﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deque.WorldSpace.AddIns
{
    class IssueProcessorHc : IIssueProcessor
    {
        public AccessibilityIssue[] PostImportProcess(AccessibilityIssue[] issues,
            ObservableCollectionEx<AccessibilityIssue> accessibilityIssues,
            WorldSpaceProject selectedProject,
            Hashtable worldSpaceViolationsShortMessages,
            WSRules rules,
            List<ViolationLookup> violationLookUpData,
            string issuesLabel)
        {
            List<AccessibilityIssue> results = new List<AccessibilityIssue>();
            foreach (AccessibilityIssue issue in issues)
            {
                if (accessibilityIssues.FirstOrDefault(i => i.id == issue.id) != null)
                {
                    continue;
                }
                switch (issue.weight)
                {
                    case 0: issue.priority = WSIssuePriority.WS_MINOR_ISSUE; break;
                    case 1: issue.priority = WSIssuePriority.WS_MODERATE_ISSUE; break;
                    case 2: issue.priority = WSIssuePriority.WS_SERIOUS_ISSUE; break;
                    case 3: issue.priority = WSIssuePriority.WS_CRITICAL_ISSUE; break;
                }
                if (issue.description == null && worldSpaceViolationsShortMessages.Contains(issue.messageCode))
                {
                    issue.description = worldSpaceViolationsShortMessages[issue.messageCode].ToString();
                }

                if (rules.messages != null && rules.messages.Count > 0)
                {
                    ILookup<string, WSMessage> custMsgs = rules.messages.ToLookup(p => p.issueId, p => p);
                    if (custMsgs != null && custMsgs.Contains(issue.messageCode))
                    {
                        IGrouping<string, WSMessage> custMsgGrp = (IGrouping<string, WSMessage>)custMsgs[issue.messageCode];
                        WSMessage custMsg = custMsgGrp.ElementAt<WSMessage>(0);
                        issue.description = custMsg.customMessage;
                    }
                }

                
                ILookup<string, ViolationLookup> lookup = violationLookUpData.ToLookup(p => p.ID, p => p);

                if (lookup.Contains(issue.messageCode))
                {
                    IGrouping<string, ViolationLookup> vlookup = (IGrouping<string, ViolationLookup>)lookup[issue.messageCode];
                    ViolationLookup vl = vlookup.ElementAt<ViolationLookup>(0);
                    issue.standard = vl.Standard;
                    issue.level = vl.Level;
                    issue.category = vl.Category;
                }
                else
                {
                    issue.standard = String.Empty;
                    issue.level = String.Empty;
                    issue.category = String.Empty;
                }

                
                if (issue.url.Contains("{0}"))
                {
                    string currentUrl = issue.url;
                    long domainID;
                    long.TryParse(issue.domain.ID, out domainID);

                    // Get Domain
                    WSProjectDomain domain = selectedProject.domains.FirstOrDefault(i => i.id == domainID);
                    if (issue.projectMode == "prod")
                    {
                        issue.url = issue.stemmedUrl = issue.stemmedOwnerurl = issue.stemmedTaburl = String.Format(currentUrl, domain.production);
                    }
                    else
                    {
                        issue.url = issue.stemmedUrl = issue.stemmedOwnerurl = issue.stemmedTaburl = String.Format(currentUrl, domain.qa);
                    }

                }
                else
                {
                    issue.stemmedUrl = issue.stemmedOwnerurl = issue.stemmedTaburl = issue.url;
                }
                issue.label = issuesLabel;
                issue.xpathArray = new XPathArrayElement[] { };
                issue.frameXpathArray = new XPathArrayElement[] { };
                issue.serverProjectID = issue.projectID = selectedProject.id;
                issue.reportType = "issue";
                issue.domainId = selectedProject.domains[0].id;
                issue.assignedTo = String.Empty;
                if (issue.practice == "null" || String.IsNullOrEmpty(issue.practice)) issue.practice = "A"; // this can be set to "Q" if it is a Quality Issue
                
                results.Add(issue);

            }
            return results.ToArray();
        }

        //public AccessibilityIssue[] PostImportProcess1(AccessibilityIssue[] issues,
        //    ObservableCollectionEx<AccessibilityIssue> accessibilityIssues,
        //    WorldSpaceProject selectedProject)
        //{
        //    System.Windows.Forms.MessageBox.Show("HC");
        //    List<AccessibilityIssue> results = new List<AccessibilityIssue>();

        //    foreach (AccessibilityIssue issue in issues)
        //    {
        //        if (accessibilityIssues.FirstOrDefault(i => i.id == issue.id) != null)
        //        {
        //            continue;
        //        }
        //        switch (issue.weight)
        //        {
        //            case 0: issue.priority = WSIssuePriority.WS_MINOR_ISSUE; break;
        //            case 1: issue.priority = WSIssuePriority.WS_MODERATE_ISSUE; break;
        //            case 2: issue.priority = WSIssuePriority.WS_SERIOUS_ISSUE; break;
        //            case 3: issue.priority = WSIssuePriority.WS_CRITICAL_ISSUE; break;
        //        }

        //        if (issue.description == null && worldSpaceViolationsShortMessages.Contains(issue.messageCode))
        //        {
        //            issue.description = worldSpaceViolationsShortMessages[issue.messageCode].ToString();
        //        }

        //        if (rules.messages != null && rules.messages.Count > 0)
        //        {
        //            ILookup<string, WSMessage> custMsgs = rules.messages.ToLookup(p => p.issueId, p => p);
        //            if (custMsgs != null && custMsgs.Contains(issue.messageCode))
        //            {
        //                IGrouping<string, WSMessage> custMsgGrp = (IGrouping<string, WSMessage>)custMsgs[issue.messageCode];
        //                WSMessage custMsg = custMsgGrp.ElementAt<WSMessage>(0);
        //                issue.description = custMsg.customMessage;
        //            }
        //        }

        //        ILookup<string, ViolationLookup> lookup = violationLookUpData.ToLookup(p => p.ID, p => p);

        //        if (lookup.Contains(issue.messageCode))
        //        {
        //            IGrouping<string, ViolationLookup> vlookup = (IGrouping<string, ViolationLookup>)lookup[issue.messageCode];
        //            ViolationLookup vl = vlookup.ElementAt<ViolationLookup>(0);
        //            issue.standard = vl.Standard;
        //            issue.level = vl.Level;
        //            issue.category = vl.Category;
        //        }
        //        else
        //        {
        //            issue.standard = String.Empty;
        //            issue.level = String.Empty;
        //            issue.category = String.Empty;
        //        }

        //        if (issue.url.Contains("{0}"))
        //        {
        //            string currentUrl = issue.url;
        //            long domainID;
        //            long.TryParse(issue.domain.ID, out domainID);

        //            // Get Domain
        //            WSProjectDomain domain = selectedProject.domains.FirstOrDefault(i => i.id == domainID);
        //            if (issue.projectMode == "prod")
        //            {
        //                issue.url = issue.stemmedUrl = issue.stemmedOwnerurl = issue.stemmedTaburl = String.Format(currentUrl, domain.production);
        //            }
        //            else
        //            {
        //                issue.url = issue.stemmedUrl = issue.stemmedOwnerurl = issue.stemmedTaburl = String.Format(currentUrl, domain.qa);
        //            }

        //        }
        //        else
        //        {
        //            issue.stemmedUrl = issue.stemmedOwnerurl = issue.stemmedTaburl = issue.url;
        //        }

        //        //issue.label = issuesLabel;
        //        issue.xpathArray = new XPathArrayElement[] { };
        //        issue.frameXpathArray = new XPathArrayElement[] { };
        //        issue.serverProjectID = issue.projectID = selectedProject.id;
        //        issue.reportType = "issue";
        //        issue.domainId = selectedProject.domains[0].id;
        //        issue.assignedTo = String.Empty;
        //        if (issue.practice == "null" || String.IsNullOrEmpty(issue.practice)) issue.practice = "A"; // this can be set to "Q" if it is a Quality Issue

        //        results.Add(issue);

        //    }
        //    return results.ToArray();
        //}

        public ArrayList PreExportProcess(ArrayList issues, WorldSpaceProject selectedProject)
        {
            foreach (AccessibilityIssue issue in issues) 
            { 
                issue.id = issue.ruleId; 
            }
            return issues;
        }

        public bool GetHelpTextViaRestCall()
        {
            return false;
        }

        public AccessibilityIssue CreateNewReadingOrderIssue(AccessibilityIssue orderIssue, string html, string tag, string xpath, long issueDate)
        {
            orderIssue.html = html;
            
            orderIssue.priority = WSIssuePriority.WS_MINOR_ISSUE;
            orderIssue.readingOrder = true;
            orderIssue.manual = false;
            orderIssue.tag = tag;
            orderIssue.status = "open";
            orderIssue.date = issueDate;
            orderIssue.xpath = xpath;

            return orderIssue;
        }
    }
}
