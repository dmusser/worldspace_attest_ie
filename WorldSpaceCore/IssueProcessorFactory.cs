﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Deque.WorldSpace.AddIns
{
    class IssueProcessorFactory
    {
        public static IIssueProcessor getInstance(string version)
        {
            Version serverVersion = new Version(version);
            Version version52 = new Version("2.1.0");

            if (serverVersion < version52)
            {
                return new IssueProcessorHc();
            }
            else
            {
                return new IssueProcessor52();
            }
            
        }
    }
}
