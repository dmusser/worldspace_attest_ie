﻿using System;
using System.Diagnostics;
using System.Text;
using log4net;
using System.Reflection;
using System.IO;
using log4net.Config;
using log4net.Repository.Hierarchy;
using log4net.Layout;
using log4net.Appender;
using log4net.Core;
using System.Runtime.InteropServices;
using System.Xml;
using System.Linq;

namespace Deque.WorldSpace.AddIns
{
    public sealed class LogWriter
    {
        
        #region Variables

        static readonly LogWriter _instance = new LogWriter();
        private ILog log = LogManager.GetLogger(typeof(LogWriter));

        #endregion

        #region ctor

        public static LogWriter Instance
        {
            get { return _instance; }
        }

        private LogWriter()
        {
            Guid localLowId = new Guid("A520A1A4-1780-4FF6-BD18-167343C5AF16");
            string folder = GetKnownFolderPath(localLowId);
            string logFile = Path.Combine(folder, "IEAccessibilityChecker\\IEAccessibilityChecker.log");

            if (!Directory.Exists(folder + "\\IEAccessibilityChecker"))
            {
                Directory.CreateDirectory(folder + "\\IEAccessibilityChecker");
            }
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

            PatternLayout patternLayout = new PatternLayout();
            patternLayout.ConversionPattern = "%date [%thread] %-5level %logger - %message%newline";
            patternLayout.ActivateOptions();

            RollingFileAppender roller = new RollingFileAppender();
            roller.AppendToFile = false;
            roller.File = logFile;
            roller.Layout = patternLayout;
            roller.MaxSizeRollBackups = 5;
            roller.MaximumFileSize = "1GB";
            roller.RollingStyle = RollingFileAppender.RollingMode.Size;
            roller.StaticLogFileName = true;
            roller.ActivateOptions();
            hierarchy.Root.AddAppender(roller);

            MemoryAppender memory = new MemoryAppender();
            memory.ActivateOptions();
            hierarchy.Root.AddAppender(memory);

            hierarchy.Root.Level = GetLogLevel(folder);

            hierarchy.Configured = true;

        }

        private Level GetLogLevel(string folder)
        {
            Level logLevel = Level.Error;
            try
            {
                if (!String.IsNullOrEmpty(folder))
                {
                    // Get Setting.xml File
                    string filename = folder + "\\IEAccessibilityChecker\\settings.xml";
                    StreamReader sr = new StreamReader(filename);
                    if ((File.Exists(filename)))
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(sr);
                        sr.Close();

                        XmlNode node = xmlDoc.SelectSingleNode("WorldSpaceSettings/IE/LogLevel");
                        if (node != null)
                        {
                            var loggerRepository = LoggerManager.GetAllRepositories().First();

                            if (loggerRepository == null)
                            {
                                throw new Exception("No logging repositories defined");
                            }

                            logLevel = loggerRepository.LevelMap[node.InnerText];

                            if (logLevel == null)
                            {
                                throw new Exception("Invalid logging level specified");
                            }
                        }

                    }
                }
            }
            catch
            {
                logLevel = Level.Error;
            }

#if DEBUG
            logLevel = Level.All;
#endif

            return logLevel;
        }

        [DllImport("shell32.dll")]
        static extern int SHGetKnownFolderPath([MarshalAs(UnmanagedType.LPStruct)] Guid rfid, uint dwFlags, IntPtr hToken, out IntPtr pszPath);

        private static string GetKnownFolderPath(Guid knownFolderId)
        {
            string folder = String.Empty;
            IntPtr pszPath = IntPtr.Zero;
            try
            {
                int hr = SHGetKnownFolderPath(knownFolderId, 0, IntPtr.Zero, out pszPath);
                if (hr >= 0)
                    folder = Marshal.PtrToStringAuto(pszPath);
                if (String.IsNullOrEmpty(folder))
                    throw Marshal.GetExceptionForHR(hr);
            }
            catch
            {
                folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            }
            finally
            {
                if (pszPath != IntPtr.Zero)
                    Marshal.FreeCoTaskMem(pszPath);
            }
            return folder;
        }
        
        #endregion

        #region ILogger Members

        public void WriteEntry(string message, LoggingLevels loggingLevel)
        {
                switch (loggingLevel)
                {
                    case LoggingLevels.FATAL:
                        log.Fatal(message);
                        break;
                    case LoggingLevels.ERROR:
                        log.Error(message);
                        break;
                    case LoggingLevels.WARN:
                        log.Warn(message);
                        break;
                    case LoggingLevels.INFO:
                        log.Info(message);
                        break;
                    case LoggingLevels.DEBUG:
                        log.Debug(message);
                        break;
                    default:
                        log.Error(message);
                        break;
                }
        }

        public void WriteEntry(string message, Exception exception, LoggingLevels loggingLevel)
        {
            switch (loggingLevel)
            {
                case LoggingLevels.FATAL:
                    log.Fatal(message, exception);
                    break;
                case LoggingLevels.ERROR:
                    log.Error(message, exception);
                    break;
                case LoggingLevels.WARN:
                    log.Warn(message, exception);
                    break;
                case LoggingLevels.INFO:
                    log.Info(message, exception);
                    break;
                case LoggingLevels.DEBUG:
                    log.Debug(message, exception);
                    break;
                default:
                    log.Error(message, exception);
                    break;
            }

        }
        /// <summary>
        /// Writes the specified information into the Application EventLog
        /// </summary>
        /// <param name="source"></param>
        /// <param name="methodName"></param>
        /// <param name="customMessage"></param>
        public void WriteEntry(string source, string methodName, string customMessage)
        {
            StringBuilder entry = new StringBuilder();
            if (customMessage.Length > 0)
            {
                entry.AppendLine(customMessage);
                entry.AppendLine();
            }
            entry.AppendLine("Method Name: " + methodName);

            WriteEntry(entry.ToString(), LoggingLevels.ERROR);
            
        }

        /// <summary>
        /// Writes the passed information into the Application Event Log
        /// </summary>
        /// <param name="source"></param>
        /// <param name="methodName"></param>
        /// <param name="customMessage"></param>
        /// <param name="innerException"></param>
        public void WriteEntry(string source, string methodName, string customMessage, Exception innerException)
        {
            StringBuilder entry = new StringBuilder();
            if (customMessage.Length > 0)
            {
                entry.AppendLine(customMessage);
                entry.AppendLine();
            }
            entry.AppendLine("Method Name: " + methodName);
            entry.AppendLine("Inner Exception");
            entry.AppendLine("------------------------");
            entry.AppendLine(innerException.Message);

            WriteEntry(entry.ToString(), innerException, LoggingLevels.ERROR);
        }

        #endregion
    
    }
}
