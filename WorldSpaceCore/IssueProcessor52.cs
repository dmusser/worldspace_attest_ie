﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Deque.WorldSpace.AddIns
{
    class IssueProcessor52 : IIssueProcessor
    {
        public AccessibilityIssue[] PostImportProcess(AccessibilityIssue[] issues, 
            ObservableCollectionEx<AccessibilityIssue> accessibilityIssues,
            WorldSpaceProject selectedProject,
            Hashtable worldSpaceViolationsShortMessages,
            WSRules rules,
            List<ViolationLookup> violationLookUpData, 
            string issuesLabel)
        {
            List<AccessibilityIssue> results = new List<AccessibilityIssue>();
            foreach (AccessibilityIssue issue in issues)
            {
                if (accessibilityIssues.FirstOrDefault(i => i.id == issue.id) != null)
                {
                    continue;
                }
                switch (issue.weight)
                {
                    case 0: issue.priority = WSIssuePriority.WS_MINOR_ISSUE; break;
                    case 1: issue.priority = WSIssuePriority.WS_MODERATE_ISSUE; break;
                    case 2: issue.priority = WSIssuePriority.WS_SERIOUS_ISSUE; break;
                    case 3: issue.priority = WSIssuePriority.WS_CRITICAL_ISSUE; break;
                }

                if (issue.description == null && issue.violations != null & issue.violations.Length > 0)
                {
                    issue.description = issue.violations[0].fullDescription;
                }
                else if (issue.description == null)
                {
                    issue.description = String.Empty;
                }

                if (issue.violations != null && issue.violations.Length > 0 && issue.violations[0].standardOptions != null &&
                    issue.violations[0].standardOptions.Length > 0 && issue.violations[0].standardOptions[0].standardNode != null)
                {
                    issue.standard = issue.violations[0].standardOptions[0].standardNode.standard;
                    issue.category = issue.violations[0].standardOptions[0].standardNode.longDescription;
                    issue.level = issue.violations[0].standardOptions[0].standardNode.level;
                }
                else
                {
                    issue.standard = String.Empty;
                    issue.level = String.Empty;
                    issue.category = String.Empty;
                }


                if (issue.url.Contains("{0}"))
                {
                    string currentUrl = issue.url;
                    long domainID;
                    long.TryParse(issue.domain.ID, out domainID);

                    // Get Domain
                    WSProjectDomain domain = selectedProject.domains.FirstOrDefault(i => i.id == domainID);
                    if (issue.projectMode == "prod")
                    {
                        issue.url = issue.stemmedUrl = issue.stemmedOwnerurl = issue.stemmedTaburl = String.Format(currentUrl, domain.production);
                    }
                    else
                    {
                        issue.url = issue.stemmedUrl = issue.stemmedOwnerurl = issue.stemmedTaburl = String.Format(currentUrl, domain.qa);
                    }

                }
                else
                {
                    issue.stemmedUrl = issue.stemmedOwnerurl = issue.stemmedTaburl = issue.url;
                }

                issue.xpathArray = new XPathArrayElement[] { };
                issue.frameXpathArray = new XPathArrayElement[] { };
                issue.reportType = "issue";
                if (issue.assignedTo == null) { issue.assignedTo = String.Empty; }
                issue.domainId = selectedProject.domains[0].id;
                if (issue.practice == "null" || String.IsNullOrEmpty(issue.practice)) issue.practice = "A"; // this can be set to "Q" if it is a Quality Issue

                results.Add(issue);

            }

            return results.ToArray();
        }

        public ArrayList PreExportProcess(ArrayList issues, WorldSpaceProject selectedProject)
        {
            ArrayList exportIssueList = new ArrayList();

            foreach (AccessibilityIssue issue in issues)
            {
                var modifiedUrl = String.Empty;
                long domainID;
                long.TryParse(issue.domain.ID, out domainID);
                WSProjectDomain domain = selectedProject.domains.FirstOrDefault(i => i.id == domainID);

                if (issue.projectMode == "prod")
                {
                    var replaceText = new Regex(Regex.Escape(domain.production));
                    modifiedUrl = replaceText.Replace(issue.url, "{0}", 1);
                }
                else
                {
                    var replaceText = new Regex(Regex.Escape(domain.qa));
                    modifiedUrl = replaceText.Replace(issue.url, "{0}", 1);
                }
                var exportIssue = new
                {
                    projectID = issue.projectID,
                    domainId = issue.domainId,
                    projectMode = issue.projectMode,
                    colorContrast = issue.colorContrast,
                    hasServerId = issue.hasServerId,
                    stickyResult = issue.stickyResult,
                    tag = issue.tag,
                    label = issue.label,
                    practice = issue.practice,
                    manual = issue.manual,
                    severityCode = issue.severityCode,
                    date = issue.date,
                    uniqueId = issue.uniqueId,
                    violations = new ArrayList(),
                    xpath = issue.xpath,
                    xpathArray = issue.xpathArray,
                    frameXpath = issue.frameXpath == null ? String.Empty : issue.frameXpath,
                    frameXpathArray = issue.frameXpathArray,
                    templates = new ArrayList(),
                    matchingTemplates = new ArrayList(),
                    msgArgs = issue.msgArgs,
                    url = modifiedUrl,
                    ownerurl = issue.ownerurl,
                    taburl = issue.taburl,
                    stemmedUrl = modifiedUrl,
                    stemmedOwnerurl = modifiedUrl,
                    stemmedTaburl = modifiedUrl,
                    comment = (issue.comment == null) ? String.Empty : issue.comment,
                    assignedTo = issue.assignedTo,
                    status = issue.status,
                    readingOrder = issue.readingOrder,
                    sandboxOwner = issue.sandboxOwner,
                    script = issue.script,
                    scriptStep = issue.scriptStep,
                    relatedNodes = issue.relatedNodes,
                    weight = issue.weight,
                    html = issue.html
                };
                if (issue.violations != null & issue.violations.Length > 0)
                {
                    exportIssue.violations.Add(issue.violations[0].code);
                }
                exportIssueList.Add(exportIssue);
            }
            return exportIssueList;
        }

        public bool GetHelpTextViaRestCall()
        {
            return true;
        }


        public AccessibilityIssue CreateNewReadingOrderIssue(AccessibilityIssue orderIssue, string html, string tag, string xpath, long issueDate)
        {

            orderIssue.category = "Paragraph D - Document readable without Style sheets";
            orderIssue.date = issueDate;
            orderIssue.description = "The reading order of the document does not match the positioning of the elements on the page.";
            orderIssue.domain = new Domain();
            orderIssue.domain.ID = orderIssue.domainId.ToString();
            orderIssue.html = html;
            //page
            orderIssue.priority = WSIssuePriority.WS_MODERATE_ISSUE;
            //project
            orderIssue.ruleId = orderIssue.messageCode = "010302-NOCSS";
            orderIssue.readingOrder = true;
            orderIssue.standard = "Section 508";
            orderIssue.tag = tag;
            orderIssue.violations = new AccessibilityViolation[] {
                new AccessibilityViolation()
                {
                    code = "010302-CSS",
                    defaultPriority = "1",
                    defaultSeverity = "level.violation",
                    fullDescription = "The reading order of the document does not match the positioning of the elements on the page.",
                    grouping = "STRUCTURE",
                    practice = "A"
                }
            };
            orderIssue.xpath = xpath;
           
            return orderIssue;
        }
    }
}
