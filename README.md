# Worldspace Attest IE

Worldspace attest IE is an automation tool which analyzes the accessbility issues on the web page. 

## Development envirionment

### Prerequisites

windows 10 64-bit or 32 Bit OS
IE 11
Microsoft Visual studio Professional 2017

### Installing Prerequisites

Install Microsoft Visual studio Professional 2017.
    - Download the setup exe from the below link.
      https://visualstudio.microsoft.com/downloads/
    - Click on the downloaded exe file to start the visual studio setup.
    
    - Install Visual studio 2017 installer project template to run the setup project.
      https://marketplace.visualstudio.com/items?itemName=VisualStudioClient.MicrosoftVisualStudio2017InstallerProjects


### Installing Worldspace Attest IE.

    - Download the Attest IE source code from the bitbucket.
        https://bitbucket.org/dmusser/worldspace_attest_ie/src/master/
    - Open the source code by using the Visual studio 2017.
    - Right click on the setup project and click "build" the project.
    - Once build is completed with no errors, right click on the setup project and      click install.
    - Click on the next button until installtion is completed.

### opening the Attest IE plugin.
    - Open IE browser.
    - Click on the "Worldspace" icon on the command bar to open the plugin (or)
      Click on the view -> explorer bar -> Worldspace Attest.




## Production envirionment

### Prerequisites

windows 10 64-bit or 32-bit OS
IE 11

### Deployment

    - Open the build project folder which is having "Setup.exe" and                         "WorldSpaceAttest_IE_Setup" files.
    - click on the setup.exe file.
    - Click on the next button until installtion is completed.

### opening the Attest IE plugin.
    - Open IE browser.
    - Click on the "Worldspace" icon on the command bar to open the plugin (or)
      Click on the view -> explorer bar -> Worldspace Attest.

## License

This project is licensed under the DEQUE - Follow below steps to see the License details.

    - Open Ie browser.
    - Click on tools -> Manage Addons
    - You can see the DEQUE Systems beside the Worldspace Attest addon.


