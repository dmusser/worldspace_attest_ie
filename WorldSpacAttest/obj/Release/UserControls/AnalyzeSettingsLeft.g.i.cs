﻿#pragma checksum "..\..\..\UserControls\AnalyzeSettingsLeft.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "E27768220B468C1DB49F4C8744C0007B22C0B437"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls {
    
    
    /// <summary>
    /// AnalyzeSettingsLeft
    /// </summary>
    public partial class AnalyzeSettingsLeft : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\UserControls\AnalyzeSettingsLeft.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls.AnalyzeSettingsLeft AnalyzeSettingsLeftControl;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\UserControls\AnalyzeSettingsLeft.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblServerURL;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\UserControls\AnalyzeSettingsLeft.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblStatus;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\UserControls\AnalyzeSettingsLeft.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Hyperlink hlSignout;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WorldSpaceAttest;component/usercontrols/analyzesettingsleft.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\AnalyzeSettingsLeft.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.AnalyzeSettingsLeftControl = ((Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls.AnalyzeSettingsLeft)(target));
            return;
            case 2:
            this.lblServerURL = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.lblStatus = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.hlSignout = ((System.Windows.Documents.Hyperlink)(target));
            
            #line 70 "..\..\..\UserControls\AnalyzeSettingsLeft.xaml"
            this.hlSignout.Click += new System.Windows.RoutedEventHandler(this.hlSignout_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

