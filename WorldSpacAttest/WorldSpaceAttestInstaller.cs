﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Reflection;
using System.IO;
using System.Linq;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer
{
    [RunInstaller(true)]
    public partial class WorldSpaceAttestInstaller : System.Configuration.Install.Installer
    {

        #region Constructor

        /// <summary>
        /// Initialization
        /// </summary>
        public WorldSpaceAttestInstaller()
        {
            InitializeComponent();
        }

        #endregion

        #region Inherited Implementations

        /// <summary>
        /// Installation procedure
        /// </summary>
        /// <param name="stateSaver"></param>
        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);                      

            RegistrationServices regsrv = new RegistrationServices();
            if (!regsrv.RegisterAssembly(this.GetType().Assembly,
                AssemblyRegistrationFlags.SetCodeBase))
            {
                throw new InstallException("Failed to Register WorldSpace Attest Components");
            }
        }

        /// <summary>
        /// Uninstallation procedure
        /// </summary>
        /// <param name="savedState"></param>
        public override void Uninstall(IDictionary savedState)
        {
            base.Uninstall(savedState);

            RegistrationServices regsrv = new RegistrationServices();
            if (!regsrv.UnregisterAssembly(this.GetType().Assembly))
            {
                throw new InstallException("Failed to Unregister WorldSpace Attest Components");
            }          
        }

        #endregion
    }
}
