﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls
{
    /// <summary>
    /// Interaction logic for AnalyzeSettingsLeft.xaml - Left panel of WorldSpace Attest
    /// </summary>
    public partial class AnalyzeSettingsLeft : UserControl,  INotifyPropertyChanged
    {

        #region Constructor 

        /// <summary>
        /// Initialization
        /// </summary>
        public AnalyzeSettingsLeft()
        {
            InitializeComponent();
            serverWorker = WSServerWorker.Instance;
        }

        #endregion

        #region Properties

        WSServerWorker serverWorker;

        /// <summary>
        /// 
        /// </summary>
        private static AnalyzeSettingsLeft _instance;

        /// <summary>
        /// 
        /// </summary>
        public static AnalyzeSettingsLeft Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new AnalyzeSettingsLeft();
                return _instance;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string ServerUrl
        {
            get;
            set;
        }

         


        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="voilations"></param>
        /// <param name="x"></param>
        public void RenderAnalysisResults(string voilations, string x)
        {
            //MessageBox.Show("Voilations are" + voilations);
        }


        public void setServerURL(string serverURL)
        {
            ServerUrl = serverURL;
            RaisePropertyChanged("ServerUrl");

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void hlSignout_Click(object sender, RoutedEventArgs e)
        {
            WebBrowser wb1 = new WebBrowser();
            wb1.Navigate(SSOLoginParameters.SSOLogoutURL);
            AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
            attestIE.resetSettings();
        }

        #endregion
        
    }
}
