﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Reflection;
using System.Collections;
using System.Windows.Threading;
using System.Configuration;
using System.Threading;
using System.Xml;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls
{
    /// <summary>
    /// Interaction logic for AnalyzeSettingsRight.xaml - Right Panel of WorldSpace Attest
    /// </summary>
    public partial class AnalyzeSettingsRight : UserControl, INotifyPropertyChanged
    {
        /// <summary>
        /// Initialize the control
        /// </summary>
        public AnalyzeSettingsRight()
        {
            InitializeComponent();
        }

        #region Properties

        WSServerWorker serverWorker = WSServerWorker.Instance;
        

        private static AnalyzeSettingsRight _instance;

        public static AnalyzeSettingsRight Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new AnalyzeSettingsRight();
                return _instance;
            }
        }

        public string ServerUrl
        {
            get;
            set;
        }



        #endregion

        #region Methods


       
        private void btnRefreshProjects_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lblSelectedProjectName.Text = "none";
                cbProjects.ClearValue(ItemsControl.ItemsSourceProperty);
                serverWorker.LoadProjects();
                cbProjects.ItemsSource = serverWorker.Projects;
                cbProjects.DisplayMemberPath = "displayName";
                downloadProjectInfo();
                lblSelectedProjectName.Text = ((WorldSpaceProject)serverWorker.SelectedProject).displayName.ToString();
                if (cbProjects.Items.Count > 0)
                {
                    cbProjects.SelectedItem = serverWorker.SelectedProject;
                }
                Dispatcher.BeginInvoke(
                            DispatcherPriority.ContextIdle,
                            new Action(delegate ()
                            {
                                lblSelectedProjectName.Focus();
                                borSelectedProjectName.BorderThickness = new Thickness(2);
                            }));

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }
        public void loadProjects()
        {
            try 
            {
                cbProjects.ItemsSource = serverWorker.Projects;
                cbProjects.DisplayMemberPath = "displayName";
                if (cbProjects.Items.Count > 0)
                {
                    cbProjects.SelectedIndex = 0;
                }
                MessageTextBlockDownload.Text = "";
                MessageTextBlockUpload.Text = "";
                gdSettingsDownload.Visibility = Visibility.Hidden;
                gdSettingsUpload.Visibility = Visibility.Hidden;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(WSConstants.WSErrorMessages.WS_Connection_Issue);
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
                attestIE.resetSettings();
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// select the project click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectProject_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                if (serverWorker.isSSOSign())
                {
                    lblSelectedProjectName.Text = ((WorldSpaceProject)cbProjects.SelectedValue).displayName.ToString();
                    serverWorker.SelectedProject = ((WorldSpaceProject)cbProjects.SelectedValue);
                    MessageTextBlockDownload.Text = "";
                    MessageTextBlockUpload.Text = "";
                    serverWorker.IsProjectChanged = true;
                    setView("download");

                    if (cbProjects.SelectedIndex != -1)
                    {
                        //Loading Labels and users
                        downloadProjectInfo();
                        
                        Dispatcher.BeginInvoke(
                            DispatcherPriority.ContextIdle,
                            new Action(delegate ()
                            {
                                lblSelectedProjectName.Focus();
                                borSelectedProjectName.BorderThickness = new Thickness(2);
                            }));

                    }
                    else
                    {
                        MessageTextBlockDownload.Text = "You must select a project to download issues from.";
                        MessageTextBlockDownload.Focusable = true;
                        Dispatcher.BeginInvoke(
                            DispatcherPriority.ContextIdle,
                            new Action(delegate ()
                            {
                                MessageTextBlockDownload.Focus();
                            }));
                    }
                }
                else
                {
                    MessageBox.Show(WSConstants.WSErrorMessages.WS_Connection_Issue);
                    AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
                    attestIE.resetSettings();
                }

                

            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a problem having connection.Please reconnect again.");
                AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
                attestIE.resetSettings();
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                
            }
        }

        protected void downloadProjectInfo()
        {
            try { 

            WSDownloadResourceResult result = serverWorker.DownloadLabels(serverWorker.SelectedProject.id.ToString());
            ComboBoxItem objItem;

            cbIssueAssignedTo.Items.Clear();
            cbIssueLabels.Items.Clear();

            objItem = new ComboBoxItem();
            objItem.Content = "Any";
            objItem.IsSelected = true;
            cbIssueLabels.Items.Add(objItem);
            objItem = new ComboBoxItem();
            objItem.Content = "Any";
            objItem.IsSelected = true;
            cbIssueAssignedTo.Items.Add(objItem);

            if (result.success)
            {

                foreach (string s in result.labels)
                {
                    objItem = new ComboBoxItem();
                    objItem.Content = s;
                    cbIssueLabels.Items.Add(objItem);
                }
                foreach (string s1 in result.usernames)
                {
                    objItem = new ComboBoxItem();
                    objItem.Content = s1;
                    cbIssueAssignedTo.Items.Add(objItem);
                }
            }

            //Loading project info
            WorldSpaceProject result1 = serverWorker.getProject(serverWorker.SelectedProject.id.ToString());
            if (result1 != null)
            {
                serverWorker.projectRuleId = result1.ruleSetDefinitionId;
            }


            //Loading Rules
            WSDownloadRulesResult result2 = serverWorker.DownloadCustomRules(serverWorker.SelectedProject.organizationId);

                if (result2.success)
                {
                    serverWorker.downloadRulesResult = result2;
                }

                //Saving project Rules into Config file.
                XmlNode objParentNode;
                XmlDocument settings = ConfigurationSettings.loadSettingsFile();
                objParentNode = ConfigurationSettings.FindNode(settings.ChildNodes, "project_" + serverWorker.SelectedProject.id.ToString());
                if (objParentNode == null)
                {
                    objParentNode = ConfigurationSettings.addNode("project_" + serverWorker.SelectedProject.id.ToString(), "");
                    if (objParentNode != null)
                    {
                        foreach (WSDownloadRule downloadRule in serverWorker.downloadRulesResult.content)
                        {
                            if (downloadRule.id == serverWorker.projectRuleId)
                                ConfigurationSettings.addComplyRuleChildNode(objParentNode, downloadRule.name, downloadRule.axeConfiguration, downloadRule.id, true);
                            else
                                ConfigurationSettings.addComplyRuleChildNode(objParentNode, downloadRule.name, downloadRule.axeConfiguration, downloadRule.id, false);
                        }
                    }
                }
                
                ConfigurationSettings.saveConfigFile();
            }
            catch (Exception ex) {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// change the view of the control based on the action.
        /// </summary>
        /// <param name="type"></param>
        public void setView(string type)
        {
            try
            {
                gdSettingsDownload.Visibility = Visibility.Hidden;
                gdSettingsUpload.Visibility = Visibility.Hidden;
                MessageTextBlockDownload.Text  = "";
                MessageTextBlockUpload.Text = "";
                switch (type.ToLower())
                {
                    case "download":
                        gdSettingsUpload.Visibility = Visibility.Hidden;
                        gdSettingsDownload.Visibility = Visibility.Visible;
                        break;
                    case "upload":
                        gdSettingsDownload.Visibility = Visibility.Hidden;
                        gdSettingsUpload.Visibility = Visibility.Visible;
                        txtIssuesLabel.Text = DateTime.Now.ToString("M/d/yyyy");
                        break;

                }
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }


        private void btnRealmSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (borderRealmPopupMenu.Visibility == Visibility.Visible)
                {
                    borderRealmPopupMenu.Visibility = Visibility.Hidden;

                }
                else
                {
                    borderRealmPopupMenu.Visibility = Visibility.Visible;
                    spPopupMenu.BringIntoView();
                    txtRealm.Focus();
                }
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        public void loadRealmSettings()
        {
            try
            {
                ConfigurationSettings.loadSettingsFile();
                txtRealm.Text = ConfigurationSettings.getNodeText("SSORealm");
                txtClientId.Text = ConfigurationSettings.getNodeText("SSOClientId");
                if (txtRealm.Text == "") txtRealm.Text = WSConstants.WS_SSO_Realm;
                if (txtClientId.Text == "") txtClientId.Text = WSConstants.WS_SSO_ClientId;
                ConfigurationSettings.saveConfigFile();
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        private void btnRealmSaveSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ConfigurationSettings.loadSettingsFile();
                ConfigurationSettings.addNode("SSORealm", txtRealm.Text);
                ConfigurationSettings.addNode("SSOClientId", txtClientId.Text);
                ConfigurationSettings.saveConfigFile();
                borderRealmPopupMenu.Visibility = Visibility.Hidden;
                btnRealmSettings.Focus();
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Download feature

        /// <summary>
        /// download the issues from the comply project.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (serverWorker.isSSOSign())
                {
                    MessageTextBlockDownload.Text = "";
                    if (cbProjects.SelectedIndex != -1)
                    {
                        string label = ((ComboBoxItem)cbIssueLabels.SelectedItem).Content.ToString();
                        string username = ((ComboBoxItem)cbIssueAssignedTo.SelectedItem).Content.ToString();
                        serverWorker.selectedLabel = string.Empty;
                        serverWorker.selectedUserName = string.Empty;
                        if (label != "Any")
                            serverWorker.selectedLabel = label;
                        if (username != "Any")
                            serverWorker.selectedUserName = username;

                        BackgroundWorker worker = new BackgroundWorker();
                        worker.DoWork += new DoWorkEventHandler(serverWorker.DownloadIssues);
                        worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
                        DownloadEventArgs dlArgs = new DownloadEventArgs();
                        dlArgs.selectedProject = (WorldSpaceProject)serverWorker.SelectedProject;
                        dlArgs.selectedType = "";
                        dlArgs.selectedMode = "";
                        dlArgs.selectedSeverity = "";
                        dlArgs.selectedPriority = "";
                        dlArgs.selectedStatus = "";
                        if (cbIssueLabels.SelectedValue == null)
                            dlArgs.selectedLabel = "any";
                        else
                            dlArgs.selectedLabel = Convert.ToString(cbIssueLabels.SelectedValue);
                        //dlArgs.selectedAssignedTo = ((ComboBoxItem)(cbIssueAssignedTo.SelectedValue)).Tag.ToString().Trim();
                        dlArgs.selectedURLPhrase = "";
                        worker.RunWorkerAsync(dlArgs);
                    }
                    else
                    {

                        MessageTextBlockDownload.Text = "You must select a project to download issues from.";
                        MessageTextBlockDownload.Focusable = true;
                        Dispatcher.BeginInvoke(
                            DispatcherPriority.ContextIdle,
                            new Action(delegate ()
                            {
                                MessageTextBlockDownload.Focus();
                            }));
                    }
                }
                else
                {
                    AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
                    attestIE.resetSettings();
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                MessageTextBlockDownload.Text = "Unable to find matching results.";
                MessageTextBlockDownload.Focusable = true;
                Dispatcher.BeginInvoke(
                    DispatcherPriority.ContextIdle,
                    new Action(delegate ()
                    {
                        MessageTextBlockDownload.Focus();
                    }));

            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                WSServerWorker serverWorker = WSServerWorker.Instance;
                AccessibilityResults results = (AccessibilityResults)e.Result;
                // If there were error messages, display them. If not, close
                if (!results.Success)
                {
                    MessageTextBlockDownload.Text = results.Message;
                    MessageTextBlockDownload.Focusable = true;
                    Dispatcher.BeginInvoke(
                        DispatcherPriority.ContextIdle,
                        new Action(delegate ()
                        {
                            MessageTextBlockDownload.Focus();
                     }));

                    
                    return;
                }
                else
                {
                    //foreach (AccessibilityIssue issue in results.Issues)
                    //{
                    //    serverWorker.AccessibilityIssues.Add(issue);
                    //}
                }
                serverWorker.AnalysisStatus = WSConstants.WS_NO_STATUS;
                serverWorker.AnalysisRunning = false;

                if (serverWorker.AccessibilityIssues.Count > 0)
                {
                    AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
                    attestIE.isAnalyzed = true;
                    attestIE.loadcheckPoints("all");
                    attestIE.cbMain.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }
        #endregion

        #region Upload feature

        /// <summary>
        /// Upload the issues to the comply project.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageTextBlockUpload.Text = ".";
                MessageTextBlockUpload.Visibility = Visibility.Hidden;
                                
                if (serverWorker.isSSOSign())
                {
                    serverWorker.IssuesLabel = txtIssuesLabel.Text;
                    WSUploadResult results = serverWorker.UploadIssues();
                    if (results.success)
                    {
                        MessageTextBlockUpload.Visibility = Visibility.Visible;
                        MessageTextBlockUpload.Text = "The Issues were uploaded successfully.";
                        MessageTextBlockUpload.Focusable = true;
                        txtIssuesLabel.Text = "";
                        Dispatcher.BeginInvoke(
                            DispatcherPriority.ContextIdle,
                            new Action(delegate ()
                            {
                                MessageTextBlockUpload.Focus();
                            }));

                        
                    }
                    else
                    {
                        MessageTextBlockUpload.Visibility = Visibility.Visible;
                        MessageTextBlockUpload.Text = "There was a problem uploading the issues. Please try again. \nIf this problem persists, please contact Deque Support";
                        MessageTextBlockUpload.Focusable = true;
                        Dispatcher.BeginInvoke(
                            DispatcherPriority.ContextIdle,
                            new Action(delegate ()
                            {
                                MessageTextBlockUpload.Focus();
                            }));
                    }
                    MessageTextBlockUpload.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageTextBlockUpload.Visibility = Visibility.Visible;
                MessageTextBlockUpload.Text = ex.Message;
                MessageTextBlockUpload.Focusable = true;
                Dispatcher.BeginInvoke(
                    DispatcherPriority.ContextIdle,
                    new Action(delegate ()
                    {
                        MessageTextBlockUpload.Focus();
                    }));
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void uploadWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
            
            try
            {
                attestIE.showProgress();
                gdLoggedInSettingsRight.Visibility = Visibility.Hidden;
                serverWorker.IssuesLabel = txtIssuesLabel.Text;
                WSUploadResult results = serverWorker.UploadIssues();
                gdLoggedInSettingsRight.Visibility = Visibility.Visible;
                attestIE.showProgress();
                if (results.success)
                {
                    MessageTextBlockUpload.Text = "The Issues were uploaded successfully.";
                    MessageTextBlockUpload.Focusable = true;
                    Dispatcher.BeginInvoke(
                        DispatcherPriority.ContextIdle,
                        new Action(delegate ()
                        {
                            MessageTextBlockUpload.Focus();
                        }));
                }
                else
                {

                    MessageTextBlockUpload.Text = "There was a problem uploading the issues. Please try again. \nIf this problem persists, please contact Deque Support";
                    MessageTextBlockUpload.Focusable = true;
                    Dispatcher.BeginInvoke(
                        DispatcherPriority.ContextIdle,
                        new Action(delegate ()
                        {
                            MessageTextBlockUpload.Focus();
                        }));
                }


            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void uploadWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
            //if (serverWorker.AccessibilityIssues.Count > 0)
            //{
            //    AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
            //    attestIE.hideProgress();
            //    attestIE.cbMain.SelectedIndex = 0;
            //}

    }



        #endregion

        #region Unused code

        //private static void SSOTest()
        //{
        //    string result = String.Empty;
        //    Hashtable loginParameters = new Hashtable();

        //    loginParameters["ClientId"] = "attest";
        //    loginParameters["Realm"] = "worldspace-comply";

        //    // HttpWebRequest loginRequest = WebRequest.Create(new Uri("https://sso.dequelabs.com/auth/realms/worldspace-comply/protocol/openid-connect/auth?response_type=token&client_id=attest")) as HttpWebRequest;
        //    //  HttpWebRequest loginRequest = WebRequest.Create(new Uri("https://sso.dequelabs.com/auth/worldspace-comply/oauth2/authorize?client_id=attest&response_type=id_token&redirect_uri=https://comply-dev.dequelabs.com/worldspace/&response_mode=form_post&scope=openid&state=12345&nonce=7362CAEA-9CA5-4B43-9BA3-34D7C303EBA7")) as HttpWebRequest;

        //    HttpWebRequest loginRequest = WebRequest.Create(new Uri("https://sso.dequelabs.com/auth/realms/worldspace-comply/protocol/openid-connect/auth?response_type=token&scope=&nonce=&client_id=attest&redirect_uri=https%3A%2F%2Fdev.dequelabs.com/*")) as HttpWebRequest;
        //    CookieContainer sessionCookies = new CookieContainer();
        //    // loginRequest.ContentType = "application/x-www-form-urlencoded";
        //    //  loginRequest.CookieContainer = sessionCookies;
        //    loginRequest.KeepAlive = true;
        //    //loginRequest.Timeout = //WSConstants.WS_REQUEST_TIMEOUT;
        //    loginRequest.Method = "GET";
        //    // GetParametrizedRequest(ref loginRequest, loginParameters);

        //    // Pick up the response:    
        //    try
        //    {
        //        using (HttpWebResponse resp = loginRequest.GetResponse() as HttpWebResponse)
        //        {
        //            StreamReader reader = new StreamReader(resp.GetResponseStream());
        //            result = reader.ReadToEnd();
        //            Console.WriteLine(result);
        //            Console.ReadLine();
        //        }

        //        /*  HttpWebRequest getOrgsReq = WebRequest.Create(new Uri("https://comply-dev.dequelabs.com/worldspace/organizations")) as HttpWebRequest;
        //          getOrgsReq.ContentType = "application/x-www-form-urlencoded";
        //          getOrgsReq.CookieContainer = sessionCookies;
        //          getOrgsReq.KeepAlive = true;
        //          getOrgsReq.Method = "GET";
        //          //GetParametrizedRequest(ref getOrgsReq, loginParameters);

        //          using (HttpWebResponse orgsResp = getOrgsReq.GetResponse() as HttpWebResponse)
        //          {
        //              StreamReader reader = new StreamReader(orgsResp.GetResponseStream());
        //              result = reader.ReadToEnd();
        //          } */
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("login failed : " + ex.Message);
        //        Console.ReadLine();
        //    }
        //}

        #endregion

        private void lblSelectedProjectName_LostFocus(object sender, RoutedEventArgs e)
        {
            borSelectedProjectName.BorderThickness = new Thickness(0);
        }

        private void lblSelectedProjectName_GotFocus(object sender, RoutedEventArgs e)
        {
            borSelectedProjectName.BorderThickness = new Thickness(2);
        }
    }
}
