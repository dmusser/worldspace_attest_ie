﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Collections;
using System.Reflection;
using System.Windows.Threading;
using System.Configuration;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls
{

    /// <summary>
    /// Interaction logic for AttestSettings.xaml
    /// </summary>
    public partial class AttestSettings : UserControl
    {
        public string strServerURL = "";
        WSServerWorker serverWorker = WSServerWorker.Instance;

        private static AttestSettings _instance;

        public static AttestSettings Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new AttestSettings();
                return _instance;
            }
        }

        public AttestSettings()
        {
            InitializeComponent();
        }

        public void loadConfigServerURL()
        {
            try
            {
                ConfigurationSettings.loadSettingsFile();
                txtServerURL.Text = ConfigurationSettings.getNodeText("ServerURL");
                txtRealm.Text = ConfigurationSettings.getNodeText("SSORealm");
                txtClientId.Text = ConfigurationSettings.getNodeText("SSOClientId");
                if (txtRealm.Text == "") txtRealm.Text = WSConstants.WS_SSO_Realm;
                if (txtClientId.Text == "") txtClientId.Text = WSConstants.WS_SSO_ClientId;
                ConfigurationSettings.saveConfigFile();
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);

            }
        }

        protected void saveConfigServerURL()
        {
            try
            {
                ConfigurationSettings.loadSettingsFile();
                ConfigurationSettings.addNode("ServerURL", serverWorker._serverUrl);
                ConfigurationSettings.saveConfigFile();
            }
            catch(Exception ex) { 
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);

            }
        }

        /// <summary>
        /// check the given username, pwd and authenicate and get the projects from comply.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnSignin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                string signInErrorStatus = "";
                string wsURL = txtServerURL.Text.Trim();
                if (serverWorker.ValidateWorldSpaceURL(ref wsURL, out signInErrorStatus))
                {
                    
                    if (signInErrorStatus == "")
                    {
                        MessageTextBlockSignin.Text = signInErrorStatus;
                        settingsBorderShow.BorderBrush = new SolidColorBrush(Colors.White);
                        settingsBorderShow.CornerRadius = new CornerRadius(18);
                        strServerURL = wsURL;
                        var builder = new UriBuilder(wsURL);
                        builder.Path = String.Empty;
                        saveConfigServerURL();        
                        DependencyObject visualForm = this;
                        ConfigurationManager.AppSettings["SSOBaseURL"] = builder.Uri.ToString();
                        ConfigurationManager.AppSettings["SSORealm"] = txtRealm.Text.ToString();
                        ConfigurationManager.AppSettings["SSOClientId"] = txtClientId.Text.ToString();
                        if (serverWorker._SSOToken.Trim() == "")
                        {
                            SSOLogin objSignInForm = new SSOLogin();
                            objSignInForm.visualForm = visualForm;
                            objSignInForm.ServerURL = serverWorker._serverUrl;
                            objSignInForm.loadURL(SSOLoginParameters.SSOLoginURL);
                            objSignInForm.ShowDialog();
                        }
                        else
                        {
                            try
                            {
                                loadProjectNames(wsURL, this);
                            }
                            catch (Exception ex)
                            {
                                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                                MessageBox.Show("Couldn't load the projects", WSConstants.WS_ADDIN_NAME);
                            };
                        }
                    }
                    else
                    {
                        MessageTextBlockSignin.Text = signInErrorStatus;
                        MessageTextBlockSignin.Focusable = true;
                        Dispatcher.BeginInvoke(
                            DispatcherPriority.ContextIdle,
                            new Action(delegate ()
                            {
                                MessageTextBlockSignin.Focus();
                            }));
                    }
                }
                else
                {
                    MessageTextBlockSignin.Text = signInErrorStatus;
                    MessageTextBlockSignin.Focusable = true;
                    Dispatcher.BeginInvoke(
                        DispatcherPriority.ContextIdle,
                        new Action(delegate ()
                        {
                            MessageTextBlockSignin.Focus();
                        }));
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                MessageBox.Show("Couldn't sign in for some reason. Please re-verify the details provided OR contact Deque support", WSConstants.WS_ADDIN_NAME);
            };
        }

        
        public static string ForceHttps(string requestUrl)
        {
            var uri = new UriBuilder(requestUrl);
            try
            {
                var hadDefaultPort = uri.Uri.IsDefaultPort;
                uri.Port = hadDefaultPort ? -1 : uri.Port;
                uri.Scheme = Uri.UriSchemeHttps;
                
            }
                       
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                
            }
            return uri.ToString();
        }

        public void loadProjectNames(string serverURL,DependencyObject visualForm)
        {            
            
            string result = String.Empty;
            Hashtable loginParameters = new Hashtable();

            // Pick up the response:    
            try
            {

                serverURL = ForceHttps(serverURL);
                serverWorker._serverUrl = serverURL;
                serverWorker.LoadProjects();

                AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(visualForm) as AttestIE;
                attestIE.setSettingsLeft(serverURL);
                attestIE.setSettingsRight();
                attestIE.Focus();

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void btnRealmSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (borderRealmPopupMenu.Visibility == Visibility.Visible)
                {
                    borderRealmPopupMenu.Visibility = Visibility.Hidden;
                    
                }
                else
                {
                    borderRealmPopupMenu.Visibility = Visibility.Visible;
                    spPopupMenu.BringIntoView();
                    txtRealm.Focus();
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        private void btnRealmSaveSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ConfigurationSettings.loadSettingsFile();
                ConfigurationSettings.addNode("SSORealm", txtRealm.Text);
                ConfigurationSettings.addNode("SSOClientId", txtClientId.Text);
                ConfigurationSettings.saveConfigFile();
                borderRealmPopupMenu.Visibility = Visibility.Hidden;
                btnRealmSettings.Focus();
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

    }

}
