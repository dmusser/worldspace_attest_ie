﻿using System;
using System.Collections;
using System.Windows.Controls;
using System.Windows.Input;
using mshtml;
using SHDocVw;
using System.Reflection;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls
{
    /// <summary>
    /// Interaction logic for HTMLUserControl.xaml
    /// </summary>
    public partial class HTMLUserControl : UserControl
    {

        public TreeViewItem inspectElement { get; set; }

        public HTMLUserControl()
        {
            InitializeComponent();
            WSServerWorker serverWorker = WSServerWorker.Instance;
            serverWorker.PropertyChanged += new PropertyChangedEventHandler(serverWorker_PropertyChanged);
        }

        void serverWorker_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "HTMLDoc")
            {
                LoadHTMLPageSource(true);
            }
        }

        #region Events

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        #endregion


        internal void LoadHTMLPageSource(bool async)
        {
            //this.htmlTreeView.Dispatcher.BeginInvoke(new Action(LoadHTMLSource), System.Windows.Threading.DispatcherPriority.Normal, null);
            LoadHTMLSource();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadHTMLSource();
        }

        private void LoadHTMLSource()
        {
            try
            {
                WSServerWorker serverWorker = WSServerWorker.Instance;
                htmlTreeView.Items.Clear();

                if (serverWorker.HTMLDoc != null)
                {
                    IHTMLElement rootElement = serverWorker.HTMLDoc.documentElement;
                    LoadHtmlTreeNode(null, rootElement, serverWorker.HTMLDoc.frames);
                    ExpandTreeNodes(htmlTreeView.Items);
                    
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage("Entered analyze section", MethodInfo.GetCurrentMethod().Name);
                TreeViewItem error = new TreeViewItem();
                error.Header = "Not able to load HTML source for this webpage, please check error log for more details.";
                htmlTreeView.Items.Add(error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="currentNode"></param>
        private void LoadHtmlTreeNode(TreeViewItem parentNode, IHTMLElement currentNode, FramesCollection frames=null, int frameCount = 0)
        {

            try
            {
                IHTMLElementCollection currentNodeChildren = (IHTMLElementCollection)currentNode.children;
                if (currentNodeChildren.length == 0)
                {
                    string tagName = currentNode.tagName;
                    string cNodeOuterText = "<null>";
                    if (currentNode.outerHTML != null)
                    {
                        cNodeOuterText = currentNode.outerHTML.Replace("\r\n", "").Trim();
                        cNodeOuterText = cNodeOuterText.Substring(0, cNodeOuterText.IndexOf(">") + 1);
                    }
                    TreeViewItem tn = new TreeViewItem();
                    tn.Header = cNodeOuterText;
                    parentNode.Items.Add(tn);
                    if (currentNode.innerHTML != null)
                    {
                        string cNodeInnerText = currentNode.innerHTML.Replace("\r\n", "").Trim();
                        tn.Items.Add(cNodeInnerText);
                    }
                    if (tagName == "IFRAME")
                    {
                        var src = currentNode.getAttribute("src");
                        if (frames != null && frameCount < frames.length)
                        {
                            object refIndex = frameCount;
                            IHTMLWindow2 frame = (IHTMLWindow2)frames.item(ref refIndex);                               
                            IHTMLDocument2 document = CrossFrame.GetDocumentFromWindow(frame);
                            LoadHtmlTreeNode(tn, document.body);
                            frameCount++;
                        }
                        
                    }

                }
                else
                {
                    TreeViewItem ancestorNode = new TreeViewItem();
                    string cNodeOuterText = currentNode.outerHTML.Replace("\r\n", "").Trim();
                    ancestorNode.Header = cNodeOuterText.Substring(0, cNodeOuterText.IndexOf(">") + 1);

                    if (parentNode != null)
                    {
                        parentNode.Items.Add(ancestorNode);
                    }
                    else
                    {
                        this.htmlTreeView.Items.Add(ancestorNode);
                    }
                    parentNode = ancestorNode;

                    foreach (IHTMLElement childNode in currentNodeChildren)
                    {
                        LoadHtmlTreeNode(parentNode, childNode, frames, frameCount);
                    }

                }

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        private void ExpandTreeNodes(ItemCollection nodes)
        {
            for (int ind = 0; ind < nodes.Count; ind++)
            {
                TreeViewItem node = nodes[ind] as TreeViewItem;
                if (node == null || (node != null && node.Items == null))
                {
                    return;
                }
                else
                {
                    node.IsExpanded = true;
                    ExpandTreeNodes(node.Items);
                }
            }
        }

        /// <summary>
        /// Highlights the source code in html page source
        /// </summary>
        public void InspectHTMLSource(string elementXPath, string elementFrameXPath)
        {
            try
            {
                if (!elementXPath.Equals(String.Empty))
                {
                    string xpath = elementXPath;
                    string[] xpathElements = xpath.Split(new char[] { '/' });
                    TreeViewItem tempNode = (TreeViewItem)this.htmlTreeView.Items[0];
                    TreeViewItem targetNode = null;
                    TreeViewItem frameTargetNode = null;

                    if (!String.IsNullOrEmpty(elementFrameXPath))
                    {
                        string frameXpath = elementFrameXPath;
                        string[] frameXpathElements = frameXpath.Split(new char[] { '/' });
                        TreeViewItem frameTempNode = (TreeViewItem)this.htmlTreeView.Items[0];

                        foreach (string frameXpathElementName in frameXpathElements)
                        {
                            if (!frameXpathElementName.Trim().Equals(String.Empty) && !frameXpathElementName.Trim().Equals("html", StringComparison.CurrentCultureIgnoreCase))
                            {
                                frameTargetNode = GetTreeNode(frameTempNode, frameXpathElementName);
                                frameTempNode = frameTargetNode;
                            }
                        }
                        if (frameTargetNode != null)
                        {
                            tempNode = frameTargetNode;
                        }
                    }

                    foreach (string xpathElementName in xpathElements)
                    {
                        if (!xpathElementName.Trim().Equals(String.Empty) && !xpathElementName.Trim().Equals("html", StringComparison.CurrentCultureIgnoreCase))
                        {
                            targetNode = GetTreeNode(tempNode, xpathElementName);
                            tempNode = targetNode;
                        }
                    }

                    //If we couldn't find the in-frame node, but could find the iframe itself, use that.
                    if (targetNode == null && frameTargetNode != null) { targetNode = frameTargetNode; } 

                    if (targetNode != null)
                    {
                        htmlTreeView.Focus();
                        ExpandTreeNodes(this.htmlTreeView.Items);
                        this.Dispatcher.BeginInvoke((Action)(() =>
                            {
                                targetNode.IsSelected = true;
                                targetNode.Focus();
                                targetNode.BringIntoView();
                            }));
                    }
                }
                else
                {
                    ((TreeViewItem)this.htmlTreeView.Items[0]).IsSelected = true;
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// selects and returns the treenode for given xpath
        /// </summary>
        /// <param name="node"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        private TreeViewItem GetTreeNode(TreeViewItem node, string xpathElementName)
        {
            TreeViewItem nodeToReturn = null;
            try
            {
                string tagName = xpathElementName;
                int tagNumber = 1;
                int tagNumberInd = tagName.IndexOf("[");
                string tagID = String.Empty;

                try
                {
                    if (tagNumberInd != -1)
                    {
                        tagNumber = Convert.ToInt32(tagName.Substring(tagNumberInd).Replace("[", "").Replace("]", ""));
                        tagName = tagName.Remove(tagNumberInd);
                    }
                }
                catch { }

                try
                {
                    if (tagNumberInd != -1)
                    {
                        tagID = tagName.Substring(tagNumberInd).Replace("[", "").Replace("]", "");
                        tagID = tagID.Replace("@id=", "").Replace("'", "");
                        tagName = tagName.Remove(tagNumberInd);
                    }
                }
                catch { }


                if (!tagID.Equals(String.Empty))
                {
                    //nodeToReturn =
                    inspectElement = null;
                    GetTreeNodeById(node, tagName, tagID, false);
                    if (inspectElement != null)
                    {
                        nodeToReturn = inspectElement;
                    }
                }
                else
                {
                    int ind = 0;
                    ItemCollection elements = node.Items;
                    foreach (TreeViewItem element in elements)
                    {
                        if (element.Header.ToString().StartsWith("<" + tagName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            ind++;
                            if (ind == tagNumber)
                            {
                                nodeToReturn = element;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
            return nodeToReturn;
        }

        /// <summary>
        /// Returns the treenode by id
        /// </summary>
        /// <param name="node"></param>
        /// <param name="tagName"></param>
        /// <param name="tagID"></param>
        /// <returns></returns>
        private void GetTreeNodeById(TreeViewItem node, string tagName, string tagID, bool elemFound)
        {
            if (elemFound)
            {
                return;
            }
            else
            {
                try
                {
                    ItemCollection elements = node.Items;
                    for (int elemInd = 0; elemInd < elements.Count; elemInd++) //each (TreeViewItem element in elements)
                    {
                        if (elements[elemInd] is TreeViewItem)
                        {
                            TreeViewItem element = elements[elemInd] as TreeViewItem;
                            if (element.Header.ToString().StartsWith("<" + tagName, StringComparison.CurrentCultureIgnoreCase))
                            {
                                string pattern = @"\sID[\s]*=[\s]*[""\w-""]*";
                                string elemID = String.Empty;
                                MatchCollection idMatches = Regex.Matches(element.Header.ToString(), pattern, RegexOptions.IgnoreCase);
                                if (idMatches != null && idMatches.Count > 0)
                                {
                                    Match idMatch = idMatches[0];
                                    elemID = idMatch.Value.Trim();
                                    int ind = elemID.IndexOf("=");
                                    elemID = elemID.Replace(elemID.Substring(0, ind + 1), "");
                                    elemID = elemID.Replace("\"", "");
                                }
                                elemFound = false;
                                if (tagID.Equals(elemID))
                                {
                                    elemFound = true;
                                    inspectElement = element;
                                }
                            }
                            GetTreeNodeById(element, tagName, tagID, elemFound);
                        }
                    }
                }
                catch { }
            }
        }

        private void htmlTreeView_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            if (htmlTreeView.SelectedItem != null)
            {
                TreeViewItem tvi = htmlTreeView.SelectedItem as TreeViewItem;
                tvi.Focus();
            }
        }
    }

    class CustomTreeViewItem : TreeViewItem
    {
        public TextBlock tagName { get; set; }
        public Hashtable attributes { get; set; }
        public TextBlock content { get; set; }
    }

}
