﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Xml;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls
{
    /// <summary>
    /// Interaction logic for CustomRulesLeft.xaml
    /// </summary>
    public partial class CustomRulesLeft : UserControl
    {
        #region "Properties"
        
        private static CustomRulesLeft _instance;

        AccessibilityRule activeRule = null;
        

        WSServerWorker serverWorker;
        public static CustomRulesLeft Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new CustomRulesLeft();
                return _instance;
            }
        }

        ObservableCollection<AccessibilityRule> DefinedRulesList, complyDownloadRulesList, customCreatedRulesList, totalRulesList;
        AccessibilityRule customRule;
        #endregion

        #region "Methods"
        public CustomRulesLeft()
        {
            InitializeComponent();
            serverWorker = WSServerWorker.Instance;
        }

        public void loadRules()
        {
            try
            {
                lvRules.ClearValue(ItemsControl.ItemsSourceProperty);
                int count = 0, intActiveRuleIndex = 0;
                WSActiveRule activeRule = null;
               
                if(DefinedRulesList == null) DefinedRulesList = new ObservableCollection<AccessibilityRule>();
                if (customCreatedRulesList == null) customCreatedRulesList = new ObservableCollection<AccessibilityRule>();
                if (complyDownloadRulesList == null) complyDownloadRulesList = new ObservableCollection<AccessibilityRule>();
                totalRulesList = new ObservableCollection<AccessibilityRule>();
                XmlNodeList nodeList = ConfigurationSettings.getRuleNodes();
                AccessibilityRule tempRule;
                
                if (serverWorker.downloadRulesResult == null)
                {
                    DefinedRulesList = new ObservableCollection<AccessibilityRule>();

                    AccessibilityRule customRule1 = new AccessibilityRule();
                    customRule1.isCustom = false;
                    customRule1.isActive = false;
                    customRule1.ruleDescription = WSConstants.WS_WCAGAA_DESC;
                    customRule1.javaScript = "";
                    if (nodeList == null || nodeList.Count == 0)
                    {
                        activeRule = new WSActiveRule();
                        activeRule.ruleName = WSConstants.WS_WCAGAA_DESC;
                        activeRule.ruleJSON = "";
                        activeRule.ruleType = "Tag";
                        serverWorker.activeRule = activeRule;
                        
                    }
                    else
                    {
                        customCreatedRulesList = new ObservableCollection<AccessibilityRule>();
                        string nodename = "";
                        foreach (XmlNode nodeObj in nodeList)
                        {
                            tempRule = new AccessibilityRule();
                            tempRule.isCustom = true;
                            tempRule.isActive = Convert.ToBoolean(nodeObj.Attributes["Active"].Value);
                            nodename = nodeObj.Name;
                            if (nodename.Contains("___"))
                            {
                                nodename = nodename.Replace("___", " ");
                            }
                            tempRule.ruleDescription = nodename;
                            tempRule.javaScript = nodeObj.InnerXml;
                            if (nodeObj.Attributes["Active"] != null)
                            {
                                if (Convert.ToBoolean(nodeObj.Attributes["Active"].Value))
                                {
                                    activeRule = new WSActiveRule();
                                    activeRule.ruleName = tempRule.ruleDescription;
                                    activeRule.ruleJSON = tempRule.javaScript;
                                    activeRule.ruleType = "Rule";
                                    if (serverWorker.activeRule == null || Convert.ToString(serverWorker.activeRule) == "")
                                    {
                                        serverWorker.activeRule = activeRule;
                                    }
                                }
                            }
                            customCreatedRulesList.Add(tempRule);
                        }
                    }
                    DefinedRulesList.Add(customRule1);

                    AccessibilityRule customRule2 = new AccessibilityRule();
                    customRule2.isCustom = false;
                    customRule2.isActive = false;
                    customRule2.ruleDescription = WSConstants.WS_SECTION508_DESC;
                    customRule2.javaScript = "";

                    DefinedRulesList.Add(customRule2);

                    

                    foreach (AccessibilityRule accRule in DefinedRulesList)
                    {
                        count++;
                        accRule.isActive = false;
                        if (serverWorker.activeRule != null)
                        {
                            if (accRule.ruleDescription == serverWorker.activeRule.ruleName)
                            {
                                accRule.isActive = true;
                                intActiveRuleIndex = count;
                            }
                        }
                        totalRulesList.Add(accRule);

                    }

                    foreach (AccessibilityRule accRule in customCreatedRulesList)
                    {
                        count++;
                        accRule.isActive = false;
                        if (serverWorker.activeRule != null)
                        {
                            if (accRule.ruleDescription.ToLower() == serverWorker.activeRule.ruleName.ToLower())
                            {
                                accRule.isActive = true;
                                intActiveRuleIndex = count;
                            }
                        }
                        totalRulesList.Add(accRule);

                    }

                }
                else
                {
                    if(!serverWorker.IsProjectChanged == false)
                    {
                        string rulename = "";
                        //customCreatedRulesList = new ObservableCollection<AccessibilityRule>();
                        //if ((customCreatedRulesList == null || customCreatedRulesList.Count == 0) && nodeList != null)
                        //{
                        //    string nodename = "";
                        //    foreach (XmlNode nodeObj in nodeList)
                        //    {
                        //        tempRule = new AccessibilityRule();
                        //        tempRule.isCustom = true;
                        //        tempRule.isActive = Convert.ToBoolean(nodeObj.Attributes["Active"].Value);
                        //        nodename = nodeObj.Name;
                        //        if (nodename.Contains("___"))
                        //        {
                        //            nodename = nodename.Replace("___", " ");
                        //        }
                        //        tempRule.ruleDescription = nodename;
                        //        tempRule.javaScript = nodeObj.InnerXml;

                        //        if (Convert.ToBoolean(nodeObj.Attributes["Active"].Value))
                        //        {
                        //            activeRule = new WSActiveRule();
                        //            activeRule.ruleName = tempRule.ruleDescription;
                        //            activeRule.ruleJSON = tempRule.javaScript;
                        //            activeRule.ruleType = "Rule";
                        //            serverWorker.activeRule = activeRule;
                        //            serverWorker.projectRuleId = "";
                        //        }
                        //        customCreatedRulesList.Add(tempRule);
                        //    }
                        //}


                        complyDownloadRulesList = new ObservableCollection<AccessibilityRule>();
                        foreach (WSDownloadRule downloadRule in serverWorker.downloadRulesResult.content)
                        {
                            try { 
                                XmlDocument settings = ConfigurationSettings.loadSettingsFile();
                                XmlNode objParentNode = ConfigurationSettings.FindNode(settings.ChildNodes, "project_" + serverWorker.SelectedProject.id.ToString());
                                if (objParentNode != null)
                                {
                                    rulename = downloadRule.name;
                                    if (rulename.Contains(" "))
                                    {
                                        rulename = rulename.Replace(" ", "___");
                                    }
                                    XmlNode activeNode = objParentNode.SelectSingleNode(rulename + "[@Active='true']");
                                    if(activeNode != null)
                                        serverWorker.projectRuleId = Convert.ToString(activeNode.Attributes["Id"].Value);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                            }
                            serverWorker.IsProjectChanged = true;
                            customRule = new AccessibilityRule();
                            customRule.isCustom = false;
                            if (downloadRule.id == serverWorker.projectRuleId)
                            {
                                customRule.isActive = true;
                                activeRule = new WSActiveRule();
                                activeRule.ruleName = downloadRule.name;
                                activeRule.ruleJSON = downloadRule.axeConfiguration;
                                if (downloadRule.axeConfiguration == null || Convert.ToString(downloadRule.axeConfiguration) == "")
                                {
                                    activeRule.ruleType = RuleTypes.Tag.ToString().ToLower();
                                    if (downloadRule.name.ToLower() == WSConstants.WS_SECTION508_DESC.ToLower())
                                        activeRule.ruleJSON = WSConstants.WS_SECTION508_JSON;
                                    else if (downloadRule.name.ToLower() == WSConstants.WS_WCAGA_DESC.ToLower())
                                        activeRule.ruleJSON = WSConstants.WS_WCAGA_JSON.ToLower();
                                }
                                else
                                    activeRule.ruleType = RuleTypes.Rule.ToString().ToLower();

                                serverWorker.activeRule = activeRule;
                                serverWorker.projectRuleId = downloadRule.id;
                                
                            }
                            else
                            {
                                customRule.isActive = false;
                            }
                        
                            customRule.ruleDescription = downloadRule.name;
                            customRule.javaScript = downloadRule.axeConfiguration;

                            if (complyDownloadRulesList != null && complyDownloadRulesList.Count != 0)
                            {
                                AccessibilityRule aExistedRule = complyDownloadRulesList.FirstOrDefault(x => x.ruleDescription == downloadRule.name);
                                if (aExistedRule == null)
                                    complyDownloadRulesList.Add(customRule);
                            }
                            else
                            {
                                complyDownloadRulesList = new ObservableCollection<AccessibilityRule>();
                                complyDownloadRulesList.Add(customRule);
                            }

                        }
                        
                    }

                    foreach (AccessibilityRule accRule in complyDownloadRulesList)
                    {
                        count++;
                        accRule.isActive = false;
                        if (serverWorker.activeRule != null)
                        {
                            if (accRule.ruleDescription.ToLower() == serverWorker.activeRule.ruleName.ToLower())
                            {
                                accRule.isActive = true;
                                intActiveRuleIndex = count;
                            }
                        }
                        totalRulesList.Add(accRule);

                    }

                    //foreach (AccessibilityRule accRule in customCreatedRulesList)
                    //{
                    //    count++;
                    //    accRule.isActive = false;
                    //    if (serverWorker.activeRule != null)
                    //    {
                    //        if (accRule.ruleDescription.ToLower() == serverWorker.activeRule.ruleName.ToLower())
                    //        {
                    //            accRule.isActive = true;
                    //            intActiveRuleIndex = count;
                    //        }
                    //    }
                    //    totalRulesList.Add(accRule);

                    //}

                }
                
                lvRules.ItemsSource = totalRulesList;
                if (totalRulesList.Count > 1 && intActiveRuleIndex != 0)
                {
                    lvRules.SelectedItem = lvRules.Items[intActiveRuleIndex -1];
                }
                else if(lvRules.Items.Count > 0)
                {
                    lvRules.SelectedItem = lvRules.Items[0];
                }

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage("Entered analyze section", MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }

        }

        public void deleteCustomRule(string ruleName)
        {
            try
            {
                bool isDeleted = false;
                foreach(AccessibilityRule oRule in customCreatedRulesList)
                {
                    if(oRule.ruleDescription.ToLower() == ruleName.ToLower())
                    {
                        if (oRule.isActive == true)
                        {
                            serverWorker.activeRule = null;
                        }
                        customCreatedRulesList.Remove(oRule);
                        isDeleted = true;
                        
                        break;
                    }
                }
                if(isDeleted)
                {
                    XmlDocument settings = ConfigurationSettings.loadSettingsFile();
                    XmlNode node = settings.SelectSingleNode("//"+ruleName);
                    if (node != null) ConfigurationSettings.FindNode(settings.ChildNodes, "CustomRules").RemoveChild(node);
                    ConfigurationSettings.saveConfigFile();
                    loadRules();
                }
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        public void addCustomRule(string ruleDesc, string jsonString)
        {
            try
            {
                AccessibilityRule aExistedRule = null;
                //foreach(AccessibilityRule aRule in customRulesList)
                //{
                //    if(aRule.ruleDescription.ToLower() == ruleDesc.ToLower())
                //    {
                //        aExistedRule = aRule;
                //        break;
                //    }
                //}
                if(customCreatedRulesList == null)
                    customCreatedRulesList = new ObservableCollection<AccessibilityRule>();
                aExistedRule = customCreatedRulesList.FirstOrDefault(x => x.ruleDescription == ruleDesc);
                

                if (aExistedRule == null)
                {
                    AccessibilityRule newRule = new AccessibilityRule();
                    newRule.isCustom = true;
                    newRule.isActive = true;
                    newRule.ruleDescription = ruleDesc;
                    newRule.javaScript = jsonString;
                    customCreatedRulesList.Add(newRule);
                }
                else
                {
                    aExistedRule.javaScript = jsonString;
                    aExistedRule.isCustom = true;
                    aExistedRule.isActive = true;
                }

                setActiveRule(ruleDesc);
                
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage("Entered analyze section", MethodInfo.GetCurrentMethod().Name);
                MessageBox.Show(ex.Message);
            }
        }

        
        private void lvRules_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (lvRules.SelectedItem != null)
                {
                    AccessibilityRule selectedRule = lvRules.SelectedItem as AccessibilityRule;
                    AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
                    attestIE.setRulesRight(selectedRule.ruleDescription, selectedRule.javaScript, selectedRule.isCustom, selectedRule.isActive);
                }

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage("Entered analyze section", MethodInfo.GetCurrentMethod().Name);

            }
        }

        //public void loadUpdatedRules(string activeRuleName)
        //{
        //    try
        //    {
        //        int count = 0, intActiveRuleIndex = 0;
        //        foreach (AccessibilityRule aRule in customRulesList)
        //        {
        //            aRule.isActive = false;
        //            count++;
        //            if (aRule.ruleDescription.ToLower() == activeRuleName.ToLower())
        //            {
        //                intActiveRuleIndex = count;
        //                aRule.isActive = true;
                        
        //            }
        //        }

        //        lvRules.ItemsSource = customRulesList;
        //        lvRules.SelectedItem = lvRules.Items[intActiveRuleIndex - 1];
        //        //for(int i= 0; i<customRulesList.Count; i++)
        //        //{
        //        //    if(customRulesList[i].ruleDescription.ToLower() == activeRuleName.ToLower())
        //        //    {
        //        //        lvRules.SelectedIndex = i;
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        LogEvent.LogErrorMessage("Entered analyze section", MethodInfo.GetCurrentMethod().Name);
        //    }
        //}

        public void setActiveRule(string key)
        {
            try
            {
                WSActiveRule activeRule = new WSActiveRule();
                
                foreach (AccessibilityRule aRule in DefinedRulesList)
                {
                    aRule.isActive = false;
                    if (aRule.ruleDescription.ToLower() == key.ToLower())
                    {
                        aRule.isActive = true;
                        activeRule.ruleJSON = aRule.javaScript;
                        activeRule.ruleName = aRule.ruleDescription.ToString();
                        if (aRule.javaScript == null || Convert.ToString(aRule.javaScript) == "")
                        {
                            activeRule.ruleType = RuleTypes.Tag.ToString().ToLower();
                            if (aRule.ruleDescription.ToLower() == WSConstants.WS_SECTION508_DESC.ToLower())
                                activeRule.ruleJSON = WSConstants.WS_SECTION508_JSON;
                            else if (aRule.ruleDescription.ToLower() == WSConstants.WS_WCAGA_DESC.ToLower())
                                activeRule.ruleJSON =WSConstants.WS_WCAGA_JSON;
                        }
                        else
                            activeRule.ruleType = RuleTypes.Rule.ToString().ToLower();

                    }
                    
                }

                foreach (AccessibilityRule aRule in complyDownloadRulesList)
                {
                    aRule.isActive = false;
                    if (aRule.ruleDescription.ToLower() == key.ToLower())
                    {
                        aRule.isActive = true;
                        activeRule.ruleJSON = aRule.javaScript;
                        activeRule.ruleName = aRule.ruleDescription.ToString();
                        if (aRule.javaScript == null || Convert.ToString(aRule.javaScript) == "")
                            activeRule.ruleType = RuleTypes.Tag.ToString().ToLower();
                        else
                            activeRule.ruleType = RuleTypes.Rule.ToString().ToLower();
                        if (aRule.ruleDescription.ToLower() == WSConstants.WS_SECTION508_DESC.ToLower())
                            activeRule.ruleJSON = WSConstants.WS_SECTION508_JSON.ToLower();
                        else if (aRule.ruleDescription.ToLower() == WSConstants.WS_WCAGA_DESC.ToLower())
                            activeRule.ruleJSON = WSConstants.WS_WCAGA_JSON;
                    }
                }

                foreach (AccessibilityRule aRule in customCreatedRulesList)
                {
                    aRule.isActive = false;
                    if (aRule.ruleDescription.ToLower() == key.ToLower())
                    {
                        aRule.isActive = true;
                        activeRule.ruleJSON = aRule.javaScript;
                        activeRule.ruleName = aRule.ruleDescription.ToString();
                        if(aRule.javaScript == null || Convert.ToString(aRule.javaScript) == "")
                            activeRule.ruleType = RuleTypes.Tag.ToString().ToLower();
                        else
                            activeRule.ruleType = RuleTypes.Rule.ToString().ToLower();
                        if (aRule.ruleDescription.ToLower() == WSConstants.WS_SECTION508_DESC.ToLower())
                            activeRule.ruleJSON = WSConstants.WS_SECTION508_JSON.ToLower();
                        else if (aRule.ruleDescription.ToLower() == WSConstants.WS_WCAGA_DESC.ToLower())
                            activeRule.ruleJSON = WSConstants.WS_WCAGA_JSON;
                    }
                }
                
                serverWorker.activeRule = activeRule;
                saveActiveruleToConfig(key);
                loadRules();
                
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }
        #endregion
        public void saveActiveruleToConfig(string key)
        {
            try
            {
                XmlDocument settings = ConfigurationSettings.loadSettingsFile();
                key = key.Replace(" ", "___");
                XmlNode curProjNode = ConfigurationSettings.FindNode(settings.ChildNodes,"project_"+ Convert.ToString(serverWorker.SelectedProject.id));
                if (curProjNode != null)
                {
                    foreach(XmlNode oNode in curProjNode.ChildNodes)
                    {
                        if (oNode.Name.ToLower() == key.ToLower())
                            oNode.Attributes["Active"].Value = "true";
                        else
                            oNode.Attributes["Active"].Value = "false";
                    }
                }
                ConfigurationSettings.saveConfigFile();
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }
        private void btnAddRule_Click(object sender, RoutedEventArgs e)
        {
            AttestIE attestIE = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
            attestIE.setRulesRight("", "", true, false);
        }
    }
}
