﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Reflection;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls
{
    /// <summary>
    /// Interaction logic for AnalyzeAttestIE.xaml
    /// </summary>
    public partial class AnalyzeAttestIE : UserControl, INotifyPropertyChanged
    {
        public AnalyzeAttestIE()
        {
            InitializeComponent();

        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string IssueHeading
        {
            get; set;
        }

        private string _labelID;
        public string LabelID
        {
            get
            {
                return _labelID;
            }
            set
            {
                _labelID = value;
            }
        }

        public int CurrNodeIndex
        {
            get; set;
        }

        public int MaxNodeIndex
        {
            get; set;
        }

        

        private void btnLast_Click(object sender, RoutedEventArgs e)
        {
            bindAccessbilityNode(MaxNodeIndex);
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            bindAccessbilityNode(CurrNodeIndex - 1);
        }

        private void btnFirst_Click(object sender, RoutedEventArgs e)
        {
            bindAccessbilityNode(0);
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            bindAccessbilityNode(CurrNodeIndex + 1);
        }

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void setPaging()
        {
            if(MaxNodeIndex == 1)
            {
                btnFirst.IsEnabled = false;
                btnPrev.IsEnabled = false;
                btnNext.IsEnabled = false;
                btnLast.IsEnabled = false;
            }
            else if(CurrNodeIndex == MaxNodeIndex)
            {
                btnFirst.IsEnabled = true;
                btnPrev.IsEnabled = true;
                btnNext.IsEnabled = false;
                btnLast.IsEnabled = false;
            }
            else if(CurrNodeIndex < MaxNodeIndex)
            {
                btnFirst.IsEnabled = true;
                btnPrev.IsEnabled = true;
                btnNext.IsEnabled = true;
                btnLast.IsEnabled = true;
            }
        }

        public void bindAccessbilityNode(int nodeIndex)
        {
            try
            {                
                WSServerWorker serverWorker = WSServerWorker.Instance;
                AccessibilityIssue AccIssue = serverWorker.AccessibilityIssues.FirstOrDefault(p => p.id == _labelID);
                MaxNodeIndex = AccIssue.nodes.Length;
                CurrNodeIndex = nodeIndex;
                lblIssueHeading.Text = AccIssue.help;
                IssueHeading = AccIssue.help;
                OnPropertyChanged("IssueHeading");
                lblIssueCountmax.Text = AccIssue.nodes.Length.ToString();
                lblIssueDesc.Text = AccIssue.description;
                lblElementlocation.Text = AccIssue.nodes[nodeIndex].html;
                lblElementSource.Text = AccIssue.nodes[nodeIndex].target[0];
                lblFailureSummary.Text = AccIssue.nodes[nodeIndex].failureSummary;
                

                setPaging();
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }
    }
}
