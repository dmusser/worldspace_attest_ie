﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SHDocVw;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls
{
    /// <summary>
    /// Interaction logic for TabUserControl.xaml
    /// </summary>
    public partial class TabUserControl : UserControl
    {

        public WorldSpaceAttest coreinstance
        {
            get;
            set;
        }
        public WebBrowserClass TabUserControlWebBrowser
        {
            get;
            set;
        }

        public TabUserControl()
        {
            
                
            try
            {
                tabControl1.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, "Error Launching TabUserControl");
            }
            InitializeComponent();
        }

        
    }
}
