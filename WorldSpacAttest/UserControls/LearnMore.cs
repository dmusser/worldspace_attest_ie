﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer
{
    public partial class LearnMore : Form
    {
        public LearnMore()
        {
            InitializeComponent();
        }

        public string LearnURL = string.Empty;

        public void loadURL(string helpUrl)
        {
            
            webBrowser1.Navigate(helpUrl);
        }
        private void LearnMore_Load(object sender, EventArgs e)
        {
            
        }
    }
}
