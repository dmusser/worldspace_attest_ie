﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Web;
using System.Windows;
namespace Deque.WorldSpace.AddIns
{
    public partial class SSOLogin : Form
    {
        public string ServerURL = string.Empty;
        public DependencyObject visualForm;
        public SSOLogin()
        {
            InitializeComponent();
        }

        public void showLogin(string serverURl)
        {
            this.Show();
        }

        private void webBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs args)
        {
            WSServerWorker serverWorker = WSServerWorker.Instance;
            WebBrowser wb = (WebBrowser)sender;
            if (wb.Url.ToString().Contains("access_token"))
            {
                string url = wb.Url.ToString();
                url = url.Replace("#", "?");
                Uri uri = new Uri(url);
                var qparams = HttpUtility.ParseQueryString(uri.Query);
                string access_token = qparams["access_token"];
                
                serverWorker._SSOToken = access_token;
                serverWorker.UserLoggedIn = true;
                webBrowser1.Navigate(new Uri("about:blank"));

                this.Close();
                this.Dispose();

                MSInternetExplorer.UserControls.AttestSettings objSettings = new MSInternetExplorer.UserControls.AttestSettings();
                objSettings.loadProjectNames(ServerURL,visualForm);
                
            }
        }
        
        public void loadURL(string URL)
        {
            webBrowser1.Navigate(URL);
            webBrowser1.Navigated += webBrowser1_Navigated;
        }

        private void SSOLogin_Load(object sender, EventArgs e)
        {
            
        }


    }
}
