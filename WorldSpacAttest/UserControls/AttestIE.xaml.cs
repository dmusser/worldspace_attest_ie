﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Data;
using mshtml;
using System.Text;
using System.Runtime.InteropServices;
using SHDocVw;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Net;
using System.IO;
using System.Collections;
using System.Windows.Input;
using System.Windows.Threading;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls
{
    /// <summary>
    /// Interaction logic for AttestIE.xaml
    /// </summary>
    public partial class AttestIE : UserControl
    {
        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public AttestIE()
        {
            InitializeComponent();
        }

        #endregion      

        #region Variables
        WSServerWorker serverWorker = WSServerWorker.Instance;
        DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        ObservableCollection<AccessibleLabelsCount> AccessibleLabels;
        public bool isHighlightPressed = false;
        public bool isAnalyzed = false;
        public string strServerURL = "";
        private IHTMLElement lastFocusedElement;
        private string lastFocusedElementHtml;
        private List<string> highlightedElements = new List<string>();
        public Int32 totalSelectedItemCount; 
        private string _labelID;
        public string LabelID
        {
            get
            {
                return _labelID;
            }
            set
            {
                _labelID = value;
            }
        }

        public int CurrNodeIndex
        {
            get; set;
        }
        public int CurrNodeReviewIndex
        {
            get; set;
        }


        public int MaxNodeIndex
        {
            get; set;
        }
        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public WorldSpaceAttest coreinstance
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public WebBrowserClass TabUserControlWebBrowser
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public AccessibilityIssue currentIssue
        {
            get;
            set;
        }

        #endregion

        #region Methods

        public void resetAttest()
        {
            try
            {
                serverWorker.AnalysisRunning = false;
                serverWorker.UserLoggedIn = false;
                serverWorker._SSOToken = "";
                gdSettingsLeft.Children.Clear();
                gdSettingsLoggedInRight.Children.Clear();
                serverWorker.downloadRulesResult = null;
                isAnalyzed = false;
                showControls("reset");
                
                gridIssueDetails.Visibility = Visibility.Visible;
                spIssueHeading.Visibility = Visibility.Visible;
                gdIssueDetailsHeader.Visibility = Visibility.Visible;

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        public void resetSettings()
        {
            try
            {
                serverWorker.UserLoggedIn = false;
                serverWorker.SelectedProject = null;
                serverWorker._SSOToken = "";
                gdSettingsLeft.Children.Clear();
                gdSettingsLoggedInRight.Children.Clear();
                serverWorker.downloadRulesResult = null;
                reset();
                showControls("settings");
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        public void showControls(string selectedType)
        {
            switch (selectedType)
            {
                case "reset":
                    gdIssueDetails.Visibility = Visibility.Hidden;
                    spIssueHeadingLast.Visibility = Visibility.Hidden;
                    spIssueDetailsFooter.Visibility = Visibility.Hidden;
                    lblIssueCountmin.Text = "0";
                    lblIssueCountmax.Text = "0";
                    btnFirst.IsEnabled = false;
                    btnPrev.IsEnabled = false;
                    btnNext.IsEnabled = false;
                    btnLast.IsEnabled = false;
                    lblIssueHeading.Text = "";
                    
                    spLoad.Visibility = Visibility.Visible;
                    if (cbMain.SelectedIndex == 0)
                    {
                        cbMain.SelectedIndex = 1;
                        cbMain.SelectedIndex = 0;

                    }
                    else
                        cbMain.SelectedIndex = 0;

                    
                    break;
                    
                case "settings":
                    gdSettingsLoggedInRight.Children.Clear();
                    gdIssuesLeft.Visibility = Visibility.Hidden;
                    gdSettingsLeft.Visibility = Visibility.Hidden;
                    gridIssueDetails.Visibility = Visibility.Hidden;
                    gdIssueDetails.Visibility = Visibility.Hidden;
                    gdAnalyzeLoad.Visibility = Visibility.Hidden;
                    if (serverWorker.UserLoggedIn)
                    {
                        setSettingsLeft(serverWorker._serverUrl);
                        setSettingsRight();
                        gdSettingsLeft.Visibility = Visibility.Visible;
                        gdSettingsLoggedInRight.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        gdSettings.Visibility = Visibility.Visible;
                        AttestSettings settings = AttestSettings.Instance;
                        settings.loadConfigServerURL();
                        gdSettings.Children.Clear();
                        gdSettings.Children.Add(settings);


                    }
                    break;
                case "rules":
                    setView("rules");
                    break;
                case "analyze":
                    gdSettings.Visibility = Visibility.Hidden;
                    gdSettingsLeft.Visibility = Visibility.Hidden;
                    gdSettingsLoggedInRight.Visibility = Visibility.Hidden;

                    if (isAnalyzed && lvCheckPoints.Items.Count > 0)
                    {
                        gdIssueDetailsHeader.Visibility = Visibility.Visible;
                        spIssueDetailsFooter.Visibility = Visibility.Visible;
                        gdIssuesLeft.Visibility = Visibility.Visible;
                        gridAnalyzeLeft.Visibility = Visibility.Visible;
                        gridIssueDetails.Visibility = Visibility.Visible;
                        gdIssueDetails.Visibility = Visibility.Visible;
                        wpAnalyzeButton.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        gdIssueDetailsHeader.Visibility = Visibility.Hidden;
                        spIssueDetailsFooter.Visibility = Visibility.Hidden;
                        gdIssuesLeft.Visibility = Visibility.Visible;
                        gridAnalyzeLeft.Visibility = Visibility.Hidden;
                        wpAnalyzeButton.Visibility = Visibility.Visible;
                        gridIssueDetails.Visibility = Visibility.Visible;
                        gdAnalyzeLoad.Visibility = Visibility.Visible;
                    }
                    break;
            }
        }

        public void loadViolationCount()
        {
            try
            {
                int violationCount = 0;
                foreach (AccessibilityIssue vltn in serverWorker.AccessibilityIssues)
                {
                    violationCount = violationCount + vltn.nodes.Length;
                }
                serverWorker.violationCount = violationCount;
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// Loading all checkpoints depends on the axe results.
        /// </summary>
        /// <param name="vltnsjson">Violations</param>
        /// <param name="rwtnjson">Review</param>
        public void RenderAnalysisResults(string vltnsjson, string rwtnjson)
        {
            try
            {
                int violationCount = 0, needsReviewCount = 0;
                WSServerWorker serverWorker = WSServerWorker.Instance;
                JavaScriptSerializer oJS = new JavaScriptSerializer();
                IHTMLWindow2 windowObj = (serverWorker.HTMLDoc).parentWindow;
                AccessibilityIssue[] results = oJS.Deserialize<AccessibilityIssue[]>(vltnsjson);
                serverWorker.AccessibilityIssues.Clear();
                foreach (AccessibilityIssue vltn in results)
                {
                    if (vltn.nodes.Length != 0)
                    {
                        if (Array.IndexOf(vltn.tags, "best-practice") < 0)
                        { 
                            vltn.issueType = IssueTypes.Violations.ToString();
                            violationCount = violationCount + vltn.nodes.Length;
                            foreach (AccessibilityNode node in vltn.nodes)
                            {
                                node.issueType = IssueTypes.Violations.ToString();
                            }
                            serverWorker.AccessibilityIssues.Add(vltn);
                        }
                    }
                }
               
                serverWorker.violationCount = violationCount;
                results = oJS.Deserialize<AccessibilityIssue[]>(rwtnjson);

                ObservableCollection<AccessibilityIssue> objAddCollection;
                foreach (AccessibilityIssue vltn in results)
                {
                    if (vltn.nodes.Length != 0)
                    {
                        if (Array.IndexOf(vltn.tags, "best-practice") < 0)
                        {
                            vltn.issueType = IssueTypes.Review.ToString();
                            needsReviewCount = needsReviewCount + vltn.nodes.Length;
                            foreach (AccessibilityNode node in vltn.nodes)
                            {
                                node.issueType = IssueTypes.Review.ToString();
                            }
                            serverWorker.AccessibilityIssues.Add(vltn);
                        }
                    }

                }
                serverWorker.needsReviewCount = needsReviewCount;

                spInProgress.Visibility = Visibility.Hidden;

                ((ComboBoxItem)cbIssueType.Items[3]).IsEnabled = false;
                if (needsReviewCount == 0)
                    ((ComboBoxItem)cbIssueType.Items[2]).IsEnabled = false;
                else
                    ((ComboBoxItem)cbIssueType.Items[2]).IsEnabled = true;

                //gridIssueDetails.Visibility = Visibility.Visible;
                loadcheckPoints(((ComboBoxItem)cbIssueType.SelectedItem).Content.ToString());
                loadLeftAnalyze();
                gridViolations.Focusable = true;
                Dispatcher.BeginInvoke(
                            DispatcherPriority.ContextIdle,
                            new Action(delegate ()
                            {
                                gridViolations.Focus();
                            }));
                borViolations.Visibility = Visibility.Visible;

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }


    
        /// <summary>
        /// loading the UI when analyze is clicked.
        /// </summary>
        public void loadAnalyze()
        {
            try
            {
                showProgress();
                borderShow.BorderBrush = new SolidColorBrush(Colors.White);
                borderShow.CornerRadius = new CornerRadius(18);
                isAnalyzed = false;
                gridIssueDetails.Visibility = Visibility.Hidden;
                gdIssueDetails.Visibility = Visibility.Hidden;

                lblCurrentStatus.Focusable = true;
                Dispatcher.BeginInvoke(
                            DispatcherPriority.ContextIdle,
                            new Action(delegate ()
                            {
                                lblCurrentStatus.Focus();
                            }));
                loadAnalyzeUI();
                isAnalyzed = true;
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        public void showProgress()
        {
            gdAnalyzeLoad.Visibility = Visibility.Visible;
            spLoad.Visibility = Visibility.Hidden;
            spInProgress.Visibility = Visibility.Visible;
        }
        public void hideProgress()
        {
            gdAnalyzeLoad.Visibility = Visibility.Hidden;
            spLoad.Visibility = Visibility.Hidden;
            spInProgress.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Loading the left side UI when analyze button is clicked
        /// </summary>
        protected void loadLeftAnalyze()
        {
            try
            {
                gridAnalyzeLeft.Visibility = Visibility.Visible;
                wpAnalyzeButton.Visibility = Visibility.Hidden;
                spIssueHeadingLast.Visibility = Visibility.Visible;
                gdIssueDetails.Visibility = Visibility.Visible;
                spIssueDetailsFooter.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// Loading  the checkpoints on the left side.
        /// </summary>
        /// <param name="strIssueType"></param>
        public void loadcheckPoints(string strIssueType)
        {
            try
            {
                AccessibleLabels = new ObservableCollection<AccessibleLabelsCount>();
                violationCount.Text = serverWorker.violationCount.ToString();
                needsReview.Text = serverWorker.needsReviewCount.ToString();
                borViolations.Visibility = Visibility.Visible;
                ObservableCollection<AccessibilityIssue> issues;
                AccessibleLabelsCount AccLabel;
                if (strIssueType == "all")
                {
                    issues = new ObservableCollection<AccessibilityIssue>(serverWorker.AccessibilityIssues.OrderBy(p => p.help));
                    foreach (AccessibilityIssue objAccessbilityIssue in issues)
                    {
                        AccLabel = AccessibleLabels.FirstOrDefault(i => i.Name == objAccessbilityIssue.help);
                        if (AccLabel == null)
                            AccessibleLabels.Add(new AccessibleLabelsCount() { LabelID = objAccessbilityIssue.id, Name = objAccessbilityIssue.help, count = objAccessbilityIssue.nodes.Length });
                        else
                            AccLabel.count = AccLabel.count + objAccessbilityIssue.nodes.Length;
                    }
                }
                else
                {
                    Int32 lblCount = 0;
                    bool blnExist = false;
                    foreach (AccessibilityIssue objAccessbilityIssue in serverWorker.AccessibilityIssues)
                    {
                        blnExist = false;
                        lblCount = 0;
                        foreach (AccessibilityNode objNode in objAccessbilityIssue.nodes)
                        {
                            if(objNode.issueType == strIssueType)
                            {
                                blnExist = true;
                                lblCount++;
                            }
                            
                        }
                        if (blnExist)
                        {
                            AccLabel = AccessibleLabels.FirstOrDefault(i => i.Name == objAccessbilityIssue.help);
                            if (AccLabel == null)
                                AccessibleLabels.Add(new AccessibleLabelsCount() { LabelID = objAccessbilityIssue.id, Name = objAccessbilityIssue.help, count = lblCount });
                            else
                                AccLabel.count = AccLabel.count + lblCount;
                        }
                    }
                        //issues = new ObservableCollection<AccessibilityIssue>(serverWorker.AccessibilityIssues.Where(p => p.issueType == strIssueType));

                        //foreach (AccessibilityIssue objAccessbilityIssue in issues)
                        //{

                        //    AccLabel = AccessibleLabels.FirstOrDefault(i => i.Name == objAccessbilityIssue.help);
                        //    if (AccLabel == null)
                        //        AccessibleLabels.Add(new AccessibleLabelsCount() { LabelID = objAccessbilityIssue.id, Name = objAccessbilityIssue.help, count = objAccessbilityIssue.nodes.Where(i => i.issueType == strIssueType).ToList().Count });
                        //    else
                        //        AccLabel.count = AccLabel.count + objAccessbilityIssue.nodes.Where(i => i.issueType == strIssueType).ToList().Count;
                        //}

                        //if(strIssueType == IssueTypes.violations.ToString())
                        //{
                        //    issues = new ObservableCollection<AccessibilityIssue>(serverWorker.AccessibilityIssues.Where(p => p.issueType == IssueTypes.review.ToString()));

                        //    foreach (AccessibilityIssue objAccessbilityIssue in issues)
                        //    {
                        //        AccLabel = AccessibleLabels.FirstOrDefault(i => i.Name == objAccessbilityIssue.help);
                        //        if (AccLabel != null)
                        //            AccLabel.count = AccLabel.count + objAccessbilityIssue.nodes.Where(i => i.issueType == IssueTypes.violations.ToString()).ToList().Count;
                        //    }
                        //}
                    }

                lvCheckPoints.ItemsSource = AccessibleLabels;
                if (AccessibleLabels.Count > 0)
                {
                    lvCheckPoints.SelectedIndex = 0;
                    spIssueHeadingLast.Visibility = Visibility.Visible;
                }
                else
                {
                    loadLeftAnalyze();
                    setView("noresults");
                    spIssueHeadingLast.Visibility = Visibility.Hidden;
                }

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                
                throw ex;
            }
        }

        /// <summary>
        /// Load method call when Analyze button is clicked.
        /// </summary>
        protected void loadAnalyzeUI()
        {
            
            try
            {
                WSServerWorker serverWorker = WSServerWorker.Instance;
                serverWorker.AnalysisStatus = WSConstants.WS_PREPARING_REQUEST_STATUS;
                serverWorker.AnalysisRunning = true;

                // Remove Current Issues
                var issuesToRemove =
                    from issue in serverWorker.AccessibilityIssues
                    where issue.url == serverWorker.BrowserURL && issue.readingOrder == false && issue.manual == false
                    select issue;

                foreach (var i in issuesToRemove.ToList())
                {
                    serverWorker.AccessibilityIssues.Remove(i);
                }

                BackgroundWorker orderWorker = new BackgroundWorker();
                orderWorker.DoWork += new DoWorkEventHandler(analyzeWorker_DoWork1);
                orderWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(analyzeWorker_RunWorkerCompleted1);
                orderWorker.RunWorkerAsync(serverWorker.HTMLDoc);

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// Handling the paging controls based on the issue count.
        /// </summary>
        protected void setPaging()
        {
            if (MaxNodeIndex == 1)
            {
                btnFirst.IsEnabled = false;
                btnPrev.IsEnabled = false;
                btnNext.IsEnabled = false;
                btnLast.IsEnabled = false;
            }
            else if (CurrNodeIndex == 1)
            {
                btnFirst.IsEnabled = false;
                btnPrev.IsEnabled = false;
                btnNext.IsEnabled = true;
                btnLast.IsEnabled = true;
            }
            else if (CurrNodeIndex == MaxNodeIndex)
            {
                btnFirst.IsEnabled = true;
                btnPrev.IsEnabled = true;
                btnNext.IsEnabled = false;
                btnLast.IsEnabled = false;
            }
            else if (CurrNodeIndex != MaxNodeIndex)
            {
                btnFirst.IsEnabled = true;
                btnPrev.IsEnabled = true;
                btnNext.IsEnabled = true;
                btnLast.IsEnabled = true;
            }
            else
            {
                btnFirst.IsEnabled = false;
                btnPrev.IsEnabled = false;
                btnNext.IsEnabled = true;
                btnLast.IsEnabled = true;

            }
        }

        /// <summary>
        /// Binding accessibility issue meta data.
        /// </summary>
        /// <param name="nodeIndex"></param>
        public void bindAccessbilityNode(int nodeIndex,string strLookType)
        {
            try
            {
                gdAnalyzeLoad.Visibility = Visibility.Hidden;
                lblIssueHeading.Focusable = true;
                WSServerWorker serverWorker = WSServerWorker.Instance;
                ObservableCollection<AccessibilityIssue> issues;
                AccessibilityIssue AccIssue;
                AccessibilityNode AccNode;
                ObservableCollection<AccessibilityNode> issueNodes, violationNodes;
                int localNodeIndex = 0;
                rdoReviewIssue.IsChecked = false;
                rdoReviewNotIssue.IsChecked = false;
                txtReviewAnswer.Text = "";
                lblReviewIssueMessage.Text = "";
                txtReviewAnswer.Visibility = Visibility.Hidden;
                lblReviewAnswer.Visibility = Visibility.Hidden;
                lblIssueHeading.Focusable = true;

                if (strLookType == "all")
                {
                    issues = new ObservableCollection<AccessibilityIssue>(serverWorker.AccessibilityIssues.Where(i => i.id == _labelID));
                    lblFailureSummary.Visibility = Visibility.Hidden;
                    spReviewMessage.Visibility = Visibility.Hidden;

                    if (issues.Count > 1)
                    {
                        if (nodeIndex > issues[0].nodes.Length)
                        {
                            if (issues[1].nodes[nodeIndex - issues[0].nodes.Length - 1].issueType == IssueTypes.Review.ToString())
                            {
                                spReviewMessage.Visibility = Visibility.Visible;
                                AccIssue = issues[1];
                                localNodeIndex = nodeIndex - issues[0].nodes.Length;
                                AccNode = AccIssue.nodes[localNodeIndex - 1];
                                if (AccNode.any.Length > 0)
                                    lblReviewIssueMessage.Text = AccNode.any[0].message;
                            }
                            else
                            {
                                lblFailureSummary.Visibility = Visibility.Visible;
                                AccIssue = issues[1];
                                localNodeIndex = nodeIndex - issues[0].nodes.Length;
                                AccNode = AccIssue.nodes[localNodeIndex - 1];
                            }

                        }
                        else
                        {
                            lblFailureSummary.Visibility = Visibility.Visible;
                            AccIssue = issues[0];
                            localNodeIndex = nodeIndex;
                            AccNode = AccIssue.nodes[localNodeIndex - 1];
                        }

                    }
                    else
                    {
                        AccIssue = issues[0];
                        localNodeIndex = nodeIndex;
                        AccNode = AccIssue.nodes[localNodeIndex - 1];
                        if (AccNode.any.Length > 0)
                            lblReviewIssueMessage.Text = AccNode.any[0].message;
                        if (AccIssue.nodes[localNodeIndex - 1].issueType == IssueTypes.Review.ToString())
                            spReviewMessage.Visibility = Visibility.Visible;
                        else
                            lblFailureSummary.Visibility = Visibility.Visible;
                    }
                }
                else if (strLookType == IssueTypes.Violations.ToString())
                {
                    issues = new ObservableCollection<AccessibilityIssue>(serverWorker.AccessibilityIssues.Where(i => i.id == _labelID));
                    lblFailureSummary.Visibility = Visibility.Visible;
                    spReviewMessage.Visibility = Visibility.Hidden;
                    
                    if (issues.Count > 1)
                    {
                        if (nodeIndex > issues[0].nodes.Length)
                        {
                            AccIssue = issues[1];
                            localNodeIndex = nodeIndex - issues[0].nodes.Length;
                        }
                        else
                        {
                            AccIssue = issues[0];
                            localNodeIndex = nodeIndex;
                        }
                    }
                    else
                    {
                        AccIssue = issues[0];
                        localNodeIndex = nodeIndex;
                    }
                    violationNodes = new ObservableCollection<AccessibilityNode>(AccIssue.nodes.Where(i => i.issueType == IssueTypes.Violations.ToString()));
                    AccNode = violationNodes[localNodeIndex - 1];
                }
                else if (strLookType == IssueTypes.Review.ToString())
                {
                    lblFailureSummary.Visibility = Visibility.Hidden;
                    spReviewMessage.Visibility = Visibility.Hidden;

                    if (strLookType == IssueTypes.Review.ToString())
                        spReviewMessage.Visibility = Visibility.Visible;
                    else
                        lblFailureSummary.Visibility = Visibility.Visible;
                    localNodeIndex = nodeIndex;
                    AccIssue = serverWorker.AccessibilityIssues.FirstOrDefault(i => i.issueType == IssueTypes.Review.ToString());
                    issueNodes = new ObservableCollection<AccessibilityNode>(AccIssue.nodes.Where(i => i.issueType == IssueTypes.Review.ToString()));
                    AccNode = issueNodes[localNodeIndex - 1];
                    if (AccNode.any.Length > 0)
                        lblReviewIssueMessage.Text = AccNode.any[0].message;

                }
                else
                {
                    lblFailureSummary.Visibility = Visibility.Visible;
                    spReviewMessage.Visibility = Visibility.Hidden;
                    localNodeIndex = nodeIndex;
                    AccIssue = serverWorker.AccessibilityIssues.FirstOrDefault(i => i.issueType == IssueTypes.Review.ToString());
                    issueNodes = new ObservableCollection<AccessibilityNode>(AccIssue.nodes.Where(i => i.issueType == IssueTypes.Rejected.ToString()));
                    AccNode = issueNodes[localNodeIndex - 1];
                    if (AccNode.any.Length > 0)
                        lblReviewIssueMessage.Text = AccNode.any[0].message;
                    if (strLookType == IssueTypes.Rejected.ToString())
                        lblFailureSummary.Visibility = Visibility.Visible;
                }

                
                currentIssue = AccIssue;
                MaxNodeIndex = totalSelectedItemCount;
                CurrNodeIndex = nodeIndex;
                CurrNodeReviewIndex = localNodeIndex;
                lblIssueHeading.Text = AccIssue.help;
                lblIssueHeading.SetValue(System.Windows.Automation.AutomationProperties.NameProperty, lblIssueHeading.Text + " heading");
                lblIssueTags.Text = "";
                lblIssueCountmax.Text = Convert.ToString(totalSelectedItemCount);
                lblIssueCountmin.Text = Convert.ToString(nodeIndex);
                lblIssueDesc.Text = AccIssue.description;
                if (AccNode.target != null)
                    lblElementlocation.Text = AccNode.target[0];
                lblElementSource.Text = AccNode.html;
                lblFailureSummary.Text = (AccNode.failureSummary != null ? AccNode.failureSummary : "Fix the following: " + Environment.NewLine + "No description specified");
                if (!String.IsNullOrEmpty(AccIssue.impact))
                {
                    if (AccIssue.impact.ToLower() == IssueImpacts.Serious.ToString().ToLower())
                    {
                        lblImpact.Text = IssueImpacts.Serious.ToString().ToLower();
                        lblImpact.Foreground = new SolidColorBrush(Colors.Brown);
                    }
                    else if (AccIssue.impact.ToLower() == IssueImpacts.Critical.ToString().ToLower())
                    {
                        lblImpact.Text = IssueImpacts.Critical.ToString().ToLower();
                        lblImpact.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#BE503F"));
                    }
                    else if (AccIssue.impact.ToLower() == IssueImpacts.Minor.ToString().ToLower())
                    {
                        lblImpact.Text = IssueImpacts.Minor.ToString().ToLower();
                        lblImpact.Foreground = new SolidColorBrush(Colors.Black);
                    }
                    else if (AccIssue.impact.ToLower() == IssueImpacts.Moderate.ToString().ToLower())
                    {
                        lblImpact.Text = IssueImpacts.Moderate.ToString().ToLower();
                        lblImpact.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#9c6c00"));
                    }
                }
                for (Int32 i = 0; i < AccIssue.tags.Length; i++)
                {
                    if(AccIssue.tags[i].Contains("cat."))
                        lblIssueTags.Text = lblIssueTags.Text + "   " + AccIssue.tags[i].Replace("cat.","category: ");
                    else
                        lblIssueTags.Text = lblIssueTags.Text + "   " + AccIssue.tags[i];
                }
                if (!String.IsNullOrEmpty(AccIssue.helpUrl))
                {
                    Uri helpUri = new Uri(AccIssue.helpUrl, UriKind.Absolute);
                    hlLearnMore.NavigateUri = helpUri;
                    
                }
                setPaging();
                if (isHighlightPressed)
                {
                    UnHighlightAllIssues();
                    if(AccNode.xpath != null)
                        HighlightTargetNode(AccNode.xpath[0], true, currentIssue.frameXpath);
                }

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                
                throw ex;
            }
        }
        
        /// <summary>
        /// Unhighlight the element
        /// </summary>
        /// <param name="nodeToHighlight"></param>
        private void UnHighlightTargetNode(string elementXPath, string elementFrameXPath)
        {
            try
            {
                WSServerWorker serverWorker = WSServerWorker.Instance;
                AddHighlightCSS();
                string xpath = !String.IsNullOrEmpty(elementFrameXPath) ? elementFrameXPath : elementXPath;
                string[] xpathElements = xpath.Split(new char[] { '/' });
                IHTMLElement docNode = null;
                IHTMLElement tempElement = serverWorker.HTMLDoc.documentElement;
                foreach (string xpathElementName in xpathElements)
                {
                    if (!xpathElementName.Trim().Equals(String.Empty) && !xpathElementName.Trim().Equals("html", StringComparison.CurrentCultureIgnoreCase))
                    {
                        docNode = GetDocumentNode(tempElement, xpathElementName);
                        tempElement = docNode;
                    }
                }
                if (docNode != null)
                {
                    lastFocusedElement = docNode;
                    lastFocusedElementHtml = docNode.outerHTML;

                    int index = 0;
                    if (docNode.className != null)
                    {
                        index = docNode.className.IndexOf("iepluginhighlight");
                    }
                    if (index > 0)
                    {
                        string newClassName = docNode.className.Substring(0, index - 1) + docNode.className.Substring(index + 17);
                        docNode.className = newClassName;
                    }
                    highlightedElements.Remove(xpath);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (lastFocusedElement != null && !lastFocusedElementHtml.Equals(String.Empty))
                    {
                        lastFocusedElement.outerHTML = lastFocusedElementHtml;
                        lastFocusedElementHtml = String.Empty;
                    }
                }
                catch
                {
                    lastFocusedElementHtml = String.Empty;
                }
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                //MessageBox.Show("Not able to UnHighlight the node, please check event viewer for exact errors.", WSConstants.WS_ADDIN_NAME);
            }
        }

        /// <summary>
        /// unighlight all issues
        /// </summary>
        public void UnHighlightAllIssues()
        {
            try
            {
                WSServerWorker serverWorker = WSServerWorker.Instance;
                foreach (AccessibilityIssue issue in serverWorker.AccessibilityIssues)
                {
                    for (int i = 0; i < issue.nodes.Length; i++)
                    {
                        if(issue.nodes[i].xpath != null)
                            UnHighlightTargetNode(issue.nodes[i].xpath[0], issue.frameXpath);
                    }
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }
        
        /// <summary>
        /// Adding CSS for highlighted element
        /// </summary>
        private void AddHighlightCSS()
        {
            try
            {
                WSServerWorker serverWorker = WSServerWorker.Instance;

                foreach (IHTMLStyleSheet entry in serverWorker.HTMLDoc.styleSheets)
                {
                    if (entry.cssText != null && entry.cssText.Contains("iepluginhighlight")) return;
                }

                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                string imageFile = System.IO.Path.GetDirectoryName(path) + "\\Images\\stripe.png";

                IHTMLStyleSheet css = (IHTMLStyleSheet)serverWorker.HTMLDoc.createStyleSheet("", 0);
                css.cssText = ".iepluginhighlight { border: 5px solid #7eb233 !important; background: url(\" " + imageFile + " \") repeat !important; }";
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="xpathElementName"></param>
        /// <returns></returns>
        private IHTMLElement GetDocumentNode(IHTMLElement elem, string xpathElementName)
        {
            IHTMLElement nodeToReturn = null;
            try
            {
                string tagName = xpathElementName;
                int tagNumber = 1;
                int tagNumberInd = tagName.IndexOf("[");
                string tagID = String.Empty;

                try
                {
                    if (tagNumberInd != -1)
                    {
                        tagNumber = Convert.ToInt32(tagName.Substring(tagNumberInd).Replace("[", "").Replace("]", ""));
                        tagName = tagName.Remove(tagNumberInd);
                    }
                }
                catch { };

                try
                {
                    tagID = tagName.Substring(tagNumberInd).Replace("[", "").Replace("]", "");
                    tagID = tagID.Replace("@id=", "").Replace("'", "").Replace("\"", "");
                    tagName = tagName.Remove(tagNumberInd);
                }
                catch { };

                IHTMLElementCollection elements = (IHTMLElementCollection)elem.children;
                if (!tagID.Equals(String.Empty))
                {
                    nodeToReturn = serverWorker.HTMLDoc.getElementById(tagID);
                }
                else
                {
                    int ind = 0;
                    foreach (IHTMLElement element in elements)
                    {
                        if (element.tagName.Equals(tagName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            ind++;
                            if (ind == tagNumber)
                            {
                                nodeToReturn = element;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
            return nodeToReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeToHighlight"></param>
        public void HighlightTargetNode(string elementXPath, bool unhighlightOldElement, string elementFrameXPath)
        {
            try
            {
                
                AddHighlightCSS();
                string xpath = !String.IsNullOrEmpty(elementFrameXPath) ? elementFrameXPath : elementXPath;
                string[] xpathElements = xpath.Split(new char[] { '/' });
                IHTMLElement docNode = null;
                IHTMLElement tempElement = serverWorker.HTMLDoc.documentElement;

                foreach (string xpathElementName in xpathElements)
                {
                    if (!xpathElementName.Trim().Equals(String.Empty) && !xpathElementName.Trim().Equals("html", StringComparison.CurrentCultureIgnoreCase))
                    {
                        docNode = GetDocumentNode(tempElement, xpathElementName);
                        tempElement = docNode;
                    }
                }

                //if (unhighlightOldElement) UnHighlightAllIssues();

                if (docNode != null)
                {
                    lastFocusedElement = docNode;
                    lastFocusedElementHtml = docNode.outerHTML;

                    string classNames = docNode.className;
                    if (classNames == null || !classNames.Contains("iepluginhighlight"))
                    {
                        docNode.className = docNode.className + " iepluginhighlight";
                    }
                    highlightedElements.Add(xpath);
                    docNode.scrollIntoView(true);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (lastFocusedElement != null && !lastFocusedElementHtml.Equals(String.Empty))
                    {
                        lastFocusedElement.outerHTML = lastFocusedElementHtml;
                        lastFocusedElementHtml = String.Empty;
                    }
                }
                catch
                {
                    lastFocusedElementHtml = String.Empty;
                }
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// Highlight button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HightLight_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!isHighlightPressed)
                {
                    isHighlightPressed = true;
                    hlHighlight.Text = "Stop highlight";
                    HighlightTargetNode(currentIssue.nodes[CurrNodeReviewIndex - 1].xpath[0], true, currentIssue.frameXpath);
                }
                else
                {
                    isHighlightPressed = false;
                    hlHighlight.Text = "highlight";
                    UnHighlightTargetNode(currentIssue.nodes[CurrNodeReviewIndex - 1].xpath[0], currentIssue.frameXpath);
                }

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                MessageBox.Show("Not able to highlight the object", WSConstants.WS_ADDIN_NAME);
                //throw ex;
            }

        }

        /// <summary>
        /// Inspect button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Inspect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                WSServerWorker serverWorker = WSServerWorker.Instance; 
                
                TabControl tabControl = EXVisualTreeHelper.FindVisualParent<TabControl>(this);
                if (tabControl != null)
                {
                    tabControl.SelectedIndex = 0;
                    foreach (HTMLUserControl htmlControl in EXVisualTreeHelper.FindChildren<HTMLUserControl>(tabControl))
                    {
                        htmlControl.InspectHTMLSource(currentIssue.nodes[CurrNodeReviewIndex - 1].xpath[0], currentIssue.frameXpath);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                MessageBox.Show("Not able to Inspect the object", WSConstants.WS_ADDIN_NAME);
                //throw ex;
            }
        }

        /// <summary>
        /// Analyze combobox selection changed event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Analyze_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (!cbMain.IsLoaded)
                    return;
                borderPopupMenu.Visibility = Visibility.Hidden;
                string selectedType = ((ComboBoxItem)cbMain.SelectedItem).Content.ToString().ToLower();
                showControls(selectedType);

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// change the view of the control based on the action.
        /// </summary>
        /// <param name="type"></param>
        public void setView(string type)
        {
            try
            {
                
                gdSettings.Visibility = Visibility.Hidden;
                gridIssueDetails.Visibility = Visibility.Hidden;
                gdIssueDetails.Visibility = Visibility.Hidden;
                gdIssuesLeft.Visibility = Visibility.Hidden;
                gdSettingsLeft.Visibility = Visibility.Hidden;
                gdSettingsLoggedInRight.Visibility = Visibility.Hidden;
                //gdSettingsDownload.Visibility = Visibility.Hidden;
                //gdSettingsUpload.Visibility = Visibility.Hidden;
                switch (type.ToLower())
                {
                    case "settings":
                        if (serverWorker.UserLoggedIn)
                        {
                            gdSettingsLeft.Visibility = Visibility.Visible;
                            gdSettingsLoggedInRight.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            gdSettings.Visibility = Visibility.Visible;
                        }
                        break;
                    case "settingsdownload":
                        cbMain.SelectedIndex = 2;
                        AnalyzeSettingsRight settingsRight = AnalyzeSettingsRight.Instance;
                        settingsRight.setView("download");
                        break;
                    case "settingsupload":
                        cbMain.SelectedIndex = 2;
                        AnalyzeSettingsRight settingsRight1 = AnalyzeSettingsRight.Instance;
                        settingsRight1.setView("upload");
                        break;
                    case "projects":
                        gdSettingsLeft.Visibility = Visibility.Visible;
                        gdSettingsLoggedInRight.Visibility = Visibility.Visible;
                        break;
                    //case "download":
                    //    gdSettingsDownload.Visibility = Visibility.Visible;
                    //    break;
                    case "analyze":
                        wpAnalyzeButton.Visibility = Visibility.Visible;
                        gdAnalyzeLoad.Visibility = Visibility.Visible;
                        break;
                    case "rules":
                        gdSettingsLeft.Children.Clear();
                        gdSettingsLoggedInRight.Children.Clear();
                        gdAnalyzeLoad.Visibility = Visibility.Hidden;
                        setRulesLeft();

                        gdSettingsLeft.Visibility = Visibility.Visible;
                        gdSettingsLoggedInRight.Visibility = Visibility.Visible;
                        break;
                    case "rulesright":
                        gdAnalyzeLoad.Visibility = Visibility.Hidden;
                        gdSettingsLeft.Visibility = Visibility.Visible;
                        gdSettingsLoggedInRight.Visibility = Visibility.Visible;
                        break;
                    case "analyzeresults":
                        gdIssuesLeft.Visibility = Visibility.Visible;
                        gridIssueDetails.Visibility = Visibility.Visible;
                        gdIssueDetails.Visibility = Visibility.Visible;
                        gdIssueDetailsHeader.Visibility = Visibility.Visible;
                        break;
                    case "noresults":
                        gdIssuesLeft.Visibility = Visibility.Visible;
                        gridAnalyzeLeft.Visibility = Visibility.Visible;
                        gridIssueDetails.Visibility = Visibility.Hidden;
                        gdIssueDetails.Visibility = Visibility.Hidden;
                        gdAnalyzeLoad.Visibility = Visibility.Visible;
                        spLoad.Visibility = Visibility.Visible;
                        txtLoadHeader.Text = "Congratulations";
                        txtLoadHeader.SetValue(System.Windows.Automation.AutomationProperties.NameProperty, "Congratulations heading");
                        txtLoadMessage.Text = "No accessibility violations found in the current state of the page. Now you should rerun Attest on every state of the page (including expanding accordians, modals," + 
                                               "sub - menus, error messaging and more). You should also perform manual testing using assistive technologies like NVDA, VoiceOver and JAWS";
                        break;
                    case "blank":
                        break;
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
               
                //throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void reset()
        {
            try
            {
                System.Threading.Thread.Sleep(5000);
                gdIssuesLeft.Visibility = Visibility.Hidden;
                wpAnalyzeButton.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void setRulesLeft()
        {
            try
            {
                    gdSettingsLeft.Children.Clear();
                    CustomRulesLeft rulesLeft = CustomRulesLeft.Instance;
                    gdSettingsLeft.Children.Add(rulesLeft);
                    rulesLeft.loadRules();
                   
                
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverURL"></param>
        public void setSettingsLeft(string serverURL)
        {
            try
            {   
                   AnalyzeSettingsLeft settingsLeft = AnalyzeSettingsLeft.Instance;
                    settingsLeft.setServerURL(serverURL);
                gdSettingsLeft.Children.Clear();
                    gdSettingsLeft.Children.Add(settingsLeft);
               
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void setSettingsRight()
        {
            try
            {
                
                AnalyzeSettingsRight settingsRight = AnalyzeSettingsRight.Instance;
                settingsRight.loadRealmSettings();
                if (serverWorker.SelectedProject == null)
                {
                    settingsRight.loadProjects();
                    settingsRight.setView("");
                }
                else
                {
                    settingsRight.setView("download");
                }
                gdSettingsLoggedInRight.Children.Clear();
                gdSettingsLoggedInRight.Children.Add(settingsRight);
                
                setView("projects");

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruleName"></param>
        /// <param name="JsonText"></param>
        public void setRulesRight(string ruleName, string JsonText,bool isCustom,bool isActive)
        {
            try
            {
                setView("rulesright");
                CustomRulesRight rulesRight = CustomRulesRight.Instance;
                rulesRight.loadData(ruleName, JsonText, isCustom);
                rulesRight.showCustomRuleSection(isCustom,isActive);
                gdSettingsLoggedInRight.Children.Clear();
                gdSettingsLoggedInRight.Children.Add(rulesRight);
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }

        }

        #endregion

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Analyze_Click(object sender, RoutedEventArgs e)
        {
            loadAnalyze();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttestIE_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void hlLearnMore_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LearnMore lm = new LearnMore();
                lm.loadURL(hlLearnMore.NavigateUri.ToString());
                lm.ShowDialog();
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);

            };


        }

        private void Grid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (borderPopupMenu.Visibility == Visibility.Visible)
                {
                    openPopup.SetValue(System.Windows.Automation.AutomationProperties.NameProperty, "Analyze Tools menu Collapsed");
                    borderPopupMenu.Visibility = Visibility.Hidden;
                }
                else
                {
                    openPopup.SetValue(System.Windows.Automation.AutomationProperties.NameProperty, "Analyze Tools menu Expanded");
                    borderPopupMenu.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                
            }
        }

        private void Grid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (borderPopupMenu.Visibility == Visibility.Visible)
                {
                    openPopup.SetValue(System.Windows.Automation.AutomationProperties.NameProperty, "Analyze Tools menu Collapsed");
                    borderPopupMenu.Visibility = Visibility.Hidden;
                }
                else
                {
                    openPopup.SetValue(System.Windows.Automation.AutomationProperties.NameProperty, "Analyze Tools menu Expanded");
                    borderPopupMenu.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void analyzeWorker_DoWork1(object sender, DoWorkEventArgs e)
        {
            try
            {
                string axeScript = Properties.Resources.axeJs;
                string readingScript = Properties.Resources.readingorder;
                string jsonScript = Properties.Resources.json2;
                string jquerytoolsScript = Properties.Resources.jquery_tools_min;

                HTMLDocument htmlDoc = e.Argument as HTMLDocument;

                IHTMLScriptElement axeScriptElement = (IHTMLScriptElement)htmlDoc.createElement("script");
                axeScriptElement.text = axeScript;
                htmlDoc.appendChild((IHTMLDOMNode)axeScriptElement);

                IHTMLScriptElement readingScriptElement = (IHTMLScriptElement)htmlDoc.createElement("script");
                readingScriptElement.text = readingScript;
                htmlDoc.appendChild((IHTMLDOMNode)readingScriptElement);

                IHTMLScriptElement jsonScriptElement = (IHTMLScriptElement)htmlDoc.createElement("script");
                jsonScriptElement.text = jsonScript;
                htmlDoc.appendChild((IHTMLDOMNode)jsonScriptElement);

                IHTMLScriptElement jquerytoolsScriptElement = (IHTMLScriptElement)htmlDoc.createElement("script");
                jquerytoolsScriptElement.text = jquerytoolsScript;
                htmlDoc.appendChild((IHTMLDOMNode)jquerytoolsScriptElement);

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        void analyzeWorker_RunWorkerCompleted1(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                try
                {
                    IHTMLWindow2 windowObj = (serverWorker.HTMLDoc).parentWindow; 
                    long issueDate = (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
                    TabUserControl attestIE1 = EXVisualTreeHelper.FindVisualParent<TabUserControl>(this) as TabUserControl;
                    if(serverWorker.activeRule == null)
                    {
                        windowObj.GetType().InvokeMember("axeRun", BindingFlags.InvokeMethod, null, windowObj, new object[] { windowObj.document, "", "tag", attestIE1.coreinstance });
                    }
                    else
                    {
                        string ruleType = "", ruleJSON = "";
                        ruleType = serverWorker.activeRule.ruleType;
                        if (ruleType == RuleTypes.Rule.ToString().ToLower() && !serverWorker.activeRule.ruleJSON.Contains("rules"))
                        {
                            var base64EncodedBytes = System.Convert.FromBase64String(serverWorker.activeRule.ruleJSON);
                            ruleJSON = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                        }
                        else
                        {
                            ruleJSON = serverWorker.activeRule.ruleJSON;
                        }
                        windowObj.GetType().InvokeMember("axeRun", BindingFlags.InvokeMethod, null, windowObj, new object[] { windowObj.document, ruleJSON, ruleType.ToLower(), attestIE1.coreinstance });
                    }
                    

                }
                catch (Exception ex)
                {
                    LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                    throw ex;
                }
                finally
                {
                    serverWorker.AnalysisRunning = false;
                    serverWorker.AnalysisStatus = String.Empty;
                }
            }
        }

        /// <summary>
        /// checkpoint list box item selected changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvCheckPoints_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (lvCheckPoints.SelectedItem != null)
                {
                    AccessibleLabelsCount alc = lvCheckPoints.SelectedItem as AccessibleLabelsCount;
                    LabelID = alc.LabelID;
                    totalSelectedItemCount = alc.count;
                    bindAccessbilityNode(1,((ComboBoxItem)cbIssueType.SelectedItem).Content.ToString());
                    setView("analyzeresults");
                }

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                //throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLast_Click(object sender, RoutedEventArgs e)
        {
            bindAccessbilityNode(MaxNodeIndex, ((ComboBoxItem)cbIssueType.SelectedItem).Content.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            bindAccessbilityNode(CurrNodeIndex - 1, ((ComboBoxItem)cbIssueType.SelectedItem).Content.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFirst_Click(object sender, RoutedEventArgs e)
        {
            bindAccessbilityNode(1, ((ComboBoxItem)cbIssueType.SelectedItem).Content.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            bindAccessbilityNode(CurrNodeIndex + 1, ((ComboBoxItem)cbIssueType.SelectedItem).Content.ToString());
        }

        /// <summary>
        /// Issue Type combobox selection changed event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbIssueType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (!cbIssueType.IsLoaded)
                    return;
                loadcheckPoints(((ComboBoxItem)cbIssueType.SelectedItem).Content.ToString());
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openPopup_Click(object sender, RoutedEventArgs e)
        {
            if (borderPopupMenu.Visibility == Visibility.Visible)
            {
                openPopup.SetValue(System.Windows.Automation.AutomationProperties.NameProperty, "Analyze Tools menu Collapsed");
                borderPopupMenu.Visibility = Visibility.Hidden;
            }
            else
            {
                openPopup.SetValue(System.Windows.Automation.AutomationProperties.NameProperty, "Analyze Tools menu Expanded");
                borderPopupMenu.Visibility = Visibility.Visible;
            }

            if (serverWorker.UserLoggedIn)
            {
                PopSignin.IsEnabled = false;

                if (serverWorker.SelectedProject != null)
                {
                    popDownload.IsEnabled = true;
                    if (isAnalyzed)
                        popUpload.IsEnabled = true;
                    else
                        popUpload.IsEnabled = false;
                    Dispatcher.BeginInvoke(
                       DispatcherPriority.ContextIdle,
                       new Action(delegate ()
                       {
                           popDownload.Focus();
                       }));
                }
                else
                {
                    popDownload.IsEnabled = false;
                    popUpload.IsEnabled = false;
                }


            }
            else
            {
                PopSignin.IsEnabled = true;
                popUpload.IsEnabled = false;
                popDownload.IsEnabled = false;
                

            }

            Dispatcher.BeginInvoke(
                        DispatcherPriority.ContextIdle,
                        new Action(delegate ()
                        {
                            cbMain.Focus();
                        }));

            Dispatcher.BeginInvoke(
                        DispatcherPriority.ContextIdle,
                        new Action(delegate ()
                        {
                            openPopup.Focus();
                        }));

            
        }

        /// <summary>
        /// settings view - sign in click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SignIn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                gridIssueDetails.Visibility = Visibility.Hidden;
                gdIssueDetails.Visibility = Visibility.Hidden;
                gdSettings.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void popDownload_Click(object sender, RoutedEventArgs e)
        {
            setView("settingsdownload");
            if (borderPopupMenu.Visibility == Visibility.Visible)
                borderPopupMenu.Visibility = Visibility.Hidden;
            else
                borderPopupMenu.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void popUpload_Click(object sender, RoutedEventArgs e)
        {
            setView("settingsupload");
            if (borderPopupMenu.Visibility == Visibility.Visible)
                borderPopupMenu.Visibility = Visibility.Hidden;
            else
                borderPopupMenu.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PopSignin_Click(object sender, RoutedEventArgs e)
        {
            cbMain.SelectedIndex = 2;
            if (borderPopupMenu.Visibility == Visibility.Visible)
                borderPopupMenu.Visibility = Visibility.Hidden;
            else
                borderPopupMenu.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvCheckPoints_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openPopup_MouseDown(object sender, MouseButtonEventArgs e)
        {
            

        }

        private void rdoReviewIssue_Click(object sender, RoutedEventArgs e)
        {
            lblReviewAnswer.Visibility = Visibility.Visible;
            txtReviewAnswer.Visibility = Visibility.Visible;
        }

        private void rdoReviewNotIssue_Click(object sender, RoutedEventArgs e)
        {
            txtReviewAnswer.Visibility = Visibility.Hidden;
            lblReviewAnswer.Visibility = Visibility.Hidden;
        }

        private void btnReviewSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if(rdoReviewIssue.IsChecked == false && rdoReviewNotIssue.IsChecked == false)
                //{

                //}
                bool blnReviewChanged = false;
                ObservableCollection<AccessibilityNode> issueNodes;
                if (rdoReviewIssue.IsChecked == true)
                {
                    foreach(AccessibilityIssue issue in serverWorker.AccessibilityIssues)
                    {
                        if(issue == currentIssue)
                        {
                            issueNodes = new ObservableCollection<AccessibilityNode>(issue.nodes.Where(o => o.issueType == IssueTypes.Review.ToString()));
                            issueNodes[CurrNodeReviewIndex - 1].issueType = IssueTypes.Violations.ToString();
                            issueNodes[CurrNodeReviewIndex - 1].failureSummary = "Fix the following: " + Environment.NewLine + (txtReviewAnswer.Text.Trim() != "" ? txtReviewAnswer.Text : "No issue description specified");
                            blnReviewChanged = true;
                            break;
                        }
                        
                    }
                    if(blnReviewChanged)
                    {
                        serverWorker.needsReviewCount = serverWorker.needsReviewCount - 1;
                        serverWorker.violationCount = serverWorker.violationCount + 1;
                    }
                    
                }
                else if(rdoReviewNotIssue.IsChecked == true)
                {
                    foreach (AccessibilityIssue issue in serverWorker.AccessibilityIssues)
                    {
                        if (issue == currentIssue)
                        {
                            issueNodes = new ObservableCollection<AccessibilityNode>(issue.nodes.Where(o => o.issueType == IssueTypes.Review.ToString()));
                            issueNodes[CurrNodeReviewIndex - 1].issueType = IssueTypes.Rejected.ToString();
                            issueNodes[CurrNodeReviewIndex - 1].failureSummary = "Issue has been reviewed " + Environment.NewLine + "Manually evaluated as 'Not an issue'.";
                            blnReviewChanged = true;
                            ((ComboBoxItem)cbIssueType.Items[3]).IsEnabled = true;
                            break;
                        }

                    }
                    if (blnReviewChanged)
                    {
                        serverWorker.needsReviewCount = serverWorker.needsReviewCount - 1;
                        
                    }
                }
                loadcheckPoints(((ComboBoxItem)cbIssueType.SelectedItem).Content.ToString());
                if (lvCheckPoints.Items.Count > 0)
                {
                    lvCheckPoints.SelectedItem = lvCheckPoints.Items[0];
                    ((ComboBoxItem)cbIssueType.Items[2]).IsEnabled = true;
                }
                else
                {
                    ((ComboBoxItem)cbIssueType.Items[2]).IsEnabled = false;
                    cbIssueType.SelectedIndex = 0;
                }
                    

            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }



        #endregion

        private void gridViolations_LostFocus(object sender, RoutedEventArgs e)
        {
            borViolations.BorderThickness = new Thickness(0);
            gridViolations.Focusable = false;
        }

    }
}


