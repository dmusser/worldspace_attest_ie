﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using mshtml;
using System.Reflection;
using System.Windows.Threading;
using System.Web.Script.Serialization;
using System.Xml;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls
{
    /// <summary>
    /// Interaction logic for CustomRulesRight.xaml
    /// </summary>
    public partial class CustomRulesRight : UserControl
    {
        public CustomRulesRight()
        {
            InitializeComponent();
        }
        protected bool isRuleExist = false;

        #region "Properties"
        private static CustomRulesRight _instance;

        public static CustomRulesRight Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new CustomRulesRight();
                return _instance;
            }
        }

        string ruleType = string.Empty, ruleFilterJSON = string.Empty;

        WSServerWorker serverWorker = WSServerWorker.Instance;

        DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        #endregion

        #region "Methods"
        public void loadData(string ruleName, string JsonText, bool isCustom)
        {
            borderDeleteRule.Visibility = Visibility.Hidden;
            isRuleExist = false;
            deleteRule.IsEnabled = false;
            txtCustomRuleName.Text = "";
            txtRuleJSON.Text = "";
            lblRulesetName.Text = "Ruleset";
            MessageTextBlockRuleName.Text = "";
            MessageTextBlockRuleJSON.Text = "";
            if (!ruleName.ToLower().Contains("add a ruleset"))
            {
                txtCustomRuleName.Text = ruleName;
                deleteRule.IsEnabled = true;
                isRuleExist = true;
                if (ruleName != "")
                {
                    borderDeleteRule.Visibility = Visibility.Visible;
                    borderDeleteRule.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#D5D5D5"));

                }
                if (!isCustom && JsonText != null)
                {
                    var base64EncodedBytes = System.Convert.FromBase64String(JsonText);
                    txtRuleJSON.Text = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                }
                else
                {
                    txtRuleJSON.Text = JsonText;
                }
                lblRulesetName.Text = lblRulesetName.Text + " " + ruleName;
                lblRulesetName.SetValue(System.Windows.Automation.AutomationProperties.NameProperty, lblRulesetName.Text + " heading");
                txtRuleName.Text = ruleName;
                txtCustRuleName.Text = ruleName;
            }

        }

        public void showCustomRuleSection(bool isCustom, bool isActive)
        {
            if (!isCustom)
            {
                spCustomRule.Visibility = Visibility.Visible;
                spDefineRule.Visibility = Visibility.Hidden;
            }
            else
            {
                spDefineRule.Visibility = Visibility.Visible;
                spCustomRule.Visibility = Visibility.Hidden;
            }

            if (isActive)
            {
                btnUsetheRule.IsEnabled = false;
                borUsetheRule.IsEnabled = false;
                spActiveRule.Visibility = Visibility.Visible;

            }
            else
            {
                btnUsetheRule.IsEnabled = true;
                borUsetheRule.IsEnabled = true;
                spActiveRule.Visibility = Visibility.Hidden;
            }

            if (isCustom && isActive)
            {
                spCustomActiveRule.Visibility = Visibility.Visible;
                spCustomActiveRule.Focusable = true;
            }
            else
            {
                spCustomActiveRule.Visibility = Visibility.Hidden;
                spCustomActiveRule.Focusable = false;
            }

        }
        #endregion

        private void saveanduse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageTextBlockRuleName.Text = "";
                MessageTextBlockRuleJSON.Text = "";
                if (txtCustomRuleName.Text.Trim() == "")
                {
                    MessageTextBlockRuleName.Text = "Please enter the rule name.";
                    MessageTextBlockRuleName.Focusable = true;
                    Dispatcher.BeginInvoke(
                        DispatcherPriority.ContextIdle,
                        new Action(delegate ()
                        {
                            MessageTextBlockRuleName.Focus();
                        }));
                    return;
                }

                if (txtRuleJSON.Text.Trim() == "")
                {
                    MessageTextBlockRuleJSON.Text = "Please enter the JSON.";
                    MessageTextBlockRuleJSON.Focusable = true;
                    Dispatcher.BeginInvoke(
                        DispatcherPriority.ContextIdle,
                        new Action(delegate ()
                        {
                            MessageTextBlockRuleJSON.Focus();
                        }));
                    return;
                }
                var serializer = new JavaScriptSerializer();
                try
                {
                    int result = serializer.Deserialize<Dictionary<string, object>>(txtRuleJSON.Text).Count;
                    if (result > 0)
                    {
                        XmlDocument totalSettings = ConfigurationSettings.loadSettingsFile();
                        XmlNode parentNode = ConfigurationSettings.FindNode(totalSettings.ChildNodes, "CustomRules");
                        if (parentNode == null)
                            parentNode = ConfigurationSettings.addNode("CustomRules", "");

                        ConfigurationSettings.addRuleChildNode(parentNode, txtCustomRuleName.Text, txtRuleJSON.Text);
                        ConfigurationSettings.saveConfigFile();
                        CustomRulesLeft.Instance.addCustomRule(txtCustomRuleName.Text, txtRuleJSON.Text);
                    }
                    spCustomActiveRule.Visibility = Visibility.Visible;
                    Task.Delay(2000).ContinueWith(_ =>
                    {
                        Dispatcher.BeginInvoke(
                        DispatcherPriority.ContextIdle,
                        new Action(delegate ()
                        {
                            spCustomActiveRule.Focus();
                        }));
                    }
                 );
                }
                catch (Exception ex)
                {
                    MessageTextBlockRuleJSON.Text = "Please enter valid JSON.";
                    MessageTextBlockRuleJSON.Focusable = true;
                    Dispatcher.BeginInvoke(
                        DispatcherPriority.ContextIdle,
                        new Action(delegate ()
                        {
                            MessageTextBlockRuleJSON.Focus();
                        }));
                    return;
                }

               
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        private void deleteRule_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                borderDeleteRuleShow.BorderBrush = new SolidColorBrush(Colors.Black);
                borderDeleteRuleShow.CornerRadius = new CornerRadius(18);
                CustomRulesLeft.Instance.deleteCustomRule(txtCustomRuleName.Text);
                
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        private void btnUsetheRule_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CustomRulesLeft rulesLeft = CustomRulesLeft.Instance;
                rulesLeft.setActiveRule(txtCustomRuleName.Text);

                Task.Delay(2000).ContinueWith(_ =>
                {
                    spCustomActiveRule.Visibility = Visibility.Visible;
                    Dispatcher.BeginInvoke(
                    DispatcherPriority.ContextIdle,
                    new Action(delegate ()
                    {
                        spCustomActiveRule.Focus();
                    }));
                }
                );


            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }

        }

        void analyzeWorker_DoWork1(object sender, DoWorkEventArgs e)
        {
            try
            {
                string axeScript = Properties.Resources.axeJs;
                string readingScript = Properties.Resources.readingorder;
                string jsonScript = Properties.Resources.json2;
                string jquerytoolsScript = Properties.Resources.jquery_tools_min;

                HTMLDocument htmlDoc = e.Argument as HTMLDocument;

                IHTMLScriptElement axeScriptElement = (IHTMLScriptElement)htmlDoc.createElement("script");
                axeScriptElement.text = axeScript;
                htmlDoc.appendChild((IHTMLDOMNode)axeScriptElement);

                IHTMLScriptElement readingScriptElement = (IHTMLScriptElement)htmlDoc.createElement("script");
                readingScriptElement.text = readingScript;
                htmlDoc.appendChild((IHTMLDOMNode)readingScriptElement);

                IHTMLScriptElement jsonScriptElement = (IHTMLScriptElement)htmlDoc.createElement("script");
                jsonScriptElement.text = jsonScript;
                htmlDoc.appendChild((IHTMLDOMNode)jsonScriptElement);

                IHTMLScriptElement jquerytoolsScriptElement = (IHTMLScriptElement)htmlDoc.createElement("script");
                jquerytoolsScriptElement.text = jquerytoolsScript;
                htmlDoc.appendChild((IHTMLDOMNode)jquerytoolsScriptElement);

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }
        
        private void hlAnalyze_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                serverWorker.AnalysisStatus = WSConstants.WS_PREPARING_REQUEST_STATUS;
                serverWorker.AnalysisRunning = true;

                WSActiveRule activeRule = serverWorker.activeRule;
                ruleType = activeRule.ruleType.ToLower();
                if(ruleType == RuleTypes.Rule.ToString().ToLower())
                {
                    var base64EncodedBytes = System.Convert.FromBase64String(activeRule.ruleJSON);
                    ruleFilterJSON = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                }
                else
                {
                    ruleFilterJSON = activeRule.ruleJSON;
                }
                
                
                BackgroundWorker orderWorker = new BackgroundWorker();
                orderWorker.DoWork += new DoWorkEventHandler(analyzeWorker_DoWork1);
                orderWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(analyzeWorker_RunWorkerCompleted1);
                orderWorker.RunWorkerAsync(serverWorker.HTMLDoc);

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        private void hlCustomActivRule_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                serverWorker.AnalysisStatus = WSConstants.WS_PREPARING_REQUEST_STATUS;
                serverWorker.AnalysisRunning = true;

                WSActiveRule activeRule = serverWorker.activeRule;
                ruleType = activeRule.ruleType.ToLower();
                ruleFilterJSON = activeRule.ruleJSON;

                BackgroundWorker orderWorker = new BackgroundWorker();
                orderWorker.DoWork += new DoWorkEventHandler(analyzeWorker_DoWork1);
                orderWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(analyzeWorker_RunWorkerCompleted1);
                orderWorker.RunWorkerAsync(serverWorker.HTMLDoc);

            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        private void spCustomActiveRule_GotFocus(object sender, RoutedEventArgs e)
        {
            borSPCustomRule.BorderThickness = new Thickness(2);
        }

        private void spCustomActiveRule_LostFocus(object sender, RoutedEventArgs e)
        {
            borSPCustomRule.BorderThickness = new Thickness(0);
        }

        void analyzeWorker_RunWorkerCompleted1(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                try
                {
                    IHTMLWindow2 windowObj = (serverWorker.HTMLDoc).parentWindow;
                    long issueDate = (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
                    TabUserControl attestIE1 = EXVisualTreeHelper.FindVisualParent<TabUserControl>(this) as TabUserControl;
                    windowObj.GetType().InvokeMember("axeRun", BindingFlags.InvokeMethod, null, windowObj, new object[] { windowObj.document, ruleFilterJSON, ruleType, attestIE1.coreinstance });
                    AttestIE attestIE2 = EXVisualTreeHelper.FindVisualParent<AttestIE>(this) as AttestIE;
                    attestIE2.isAnalyzed = true;
                    attestIE2.cbMain.SelectedIndex = 0;
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                    LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                    throw ex;
                }
                finally
                {
                    serverWorker.AnalysisRunning = false;
                    serverWorker.AnalysisStatus = String.Empty;
                }
            }
        }
    }
}
