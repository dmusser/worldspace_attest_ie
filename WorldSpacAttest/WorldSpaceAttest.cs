﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using BandObjectLib;
using Microsoft.Win32;
using SHDocVw;
using mshtml;
using System.ComponentModel;
using System.Collections.Generic;
using Deque.WorldSpace.AddIns.MSInternetExplorer.UserControls;
using System.Web.Script.Serialization;
using System.Reflection;
using System.IO;
using System.Linq;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer
{
    [Guid("1740043F-8495-4000-AA9E-F8C7C9B6B96C")]
    [BandObject(Name=WSConstants.WS_ADDIN_NAME, Style=BandObjectStyle.Horizontal, HelpText =WSConstants.WS_ADDIN_NAME)] 
    public partial class WorldSpaceAttest : BandObject
    {
        #region Variables

        /// <summary>
        /// The BandsCLSID for internet explorer toolbar extensions {E0DD6CAB-2D10-11D2-8F1A-0000F87ABD16}
        /// </summary>
        private const string ToolbarExtensionForBandsCLSID = "{E0DD6CAB-2D10-11D2-8F1A-0000F87ABD16}";
        public const string BHO_REGISTRY_KEY_NAME = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects";
        public string prevBrowserUrl = "";
        #endregion

        #region Constructor

        /// <summary>
        /// Initialization
        /// </summary>
        public WorldSpaceAttest()
        {          
            try
            {
                InitializeComponent();
                tabUserControl1.GotFocus += new System.Windows.RoutedEventHandler(tabUserControl1_GotFocus);
                tabUserControl1.MouseEnter += new System.Windows.Input.MouseEventHandler(tabUserControl1_MouseEnter);
                this.VisibleChanged += new EventHandler(bandObjectVisibilityChanged);
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Implementation for event onMouseEnter = onGotGocus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tabUserControl1_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.OnGotFocus(e);
        }

        /// <summary>
        /// Implementation for event onGotGocus = parent's onGotGocus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tabUserControl1_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            this.OnGotFocus(e);
        }

        /// <summary>
        /// Initialization on component load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorldSpaceAttest_Load(object sender, EventArgs e)
        {
            try
            {
                Explorer.DocumentComplete += new DWebBrowserEvents2_DocumentCompleteEventHandler(Explorer_DocumentComplete);
                WSServerWorker serverWorker = WSServerWorker.Instance;
                if (Explorer.ReadyState == tagREADYSTATE.READYSTATE_COMPLETE)
                {
                    serverWorker.HTMLDoc = (HTMLDocument)Explorer.Document;
                    if (Explorer.LocationURL != WSConstants.WS_Blank_URL)
                        serverWorker.BrowserURL = Explorer.LocationURL;
                    
                }
                
                tabUserControl1.TabUserControlWebBrowser = Explorer;
                tabUserControl1.coreinstance = this;

                //if (serverWorker.Status != "Not Logged In")
                //{
                //    tabUserControl1.tabControl1.SelectedIndex = 1;
                //}
                //else
                //{
                //    tabUserControl1.tabControl1.SelectedIndex = 3;
                //}
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        /// <summary>
        /// Series of actions to be executed on document complete
        /// Sets the HTML document and browserURL on background model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="url"></param>
        private void Explorer_DocumentComplete(object sender, ref object url)
        {
            WSServerWorker serverWorker = WSServerWorker.Instance;
            try
            {
                SHDocVw.WebBrowser wb = (SHDocVw.WebBrowser)sender;
                if (Explorer.LocationURL != WSConstants.WS_Blank_URL)
                    serverWorker.BrowserURL = Explorer.LocationURL;
                if (wb.ReadyState == tagREADYSTATE.READYSTATE_COMPLETE &&
                    Explorer.LocationURL.Equals(url) &&
                    Explorer.LocationURL.StartsWith("http"))
                {
                    serverWorker.HTMLDoc = (HTMLDocument)Explorer.Document;
                    if (Explorer.LocationURL != WSConstants.WS_Blank_URL)
                    {
                        serverWorker.BrowserURL = Explorer.LocationURL;
                        AttestIE attestIE = (AttestIE)EXVisualTreeHelper.GetVisualChild<AttestIE>(tabUserControl1);
                        attestIE.resetAttest();
                    }
                    
                    
                }

            }
            catch (Exception ex)
            {
                if (Explorer.LocationURL != WSConstants.WS_Blank_URL)
                {
                    serverWorker.BrowserURL = Explorer.LocationURL;
                    AttestIE attestIE = (AttestIE)EXVisualTreeHelper.GetVisualChild<AttestIE>(tabUserControl1);
                    attestIE.resetAttest();
                }
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public WebBrowserClass ExplorerClass
        {
            get { return Explorer; }
        }

        #endregion

        #region Inherited Interface Methods
        
        /// Show or hide component UI 
        /// <param name="fShow"></param>
        public override void ShowDW(bool fShow)
        {           
            if (fShow)
            {
                Show();
            }
            else
                Hide();
        }

        [DllImport("user32.dll")]
        private static extern int TranslateMessage(ref MSG lpMsg);

        [DllImport("user32", EntryPoint = "DispatchMessage")]
        private static extern bool DispatchMessage(ref MSG msg);

        /// <summary>
        /// Handles input 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public override int TranslateAcceleratorIO(ref MSG msg)
        {
            try
            {
                if (msg.message == 0x100)//WM_KEYDOWN
                {
                    if (msg.wParam == (uint)Keys.F6)//keys used by explorer to navigate from control to control
                    {
                        if (SelectNextControl(
                                ActiveControl,
                                ModifierKeys == Keys.Shift ? false : true,
                                true,
                                true,
                                false)
                            )
                            return 0;//S_OK
                    }
                    else
                    {
                        TranslateMessage(ref msg);
                        DispatchMessage(ref msg);
                        return 0;
                    }

                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);                
            }
            return 1;
        }

        #endregion

        #region COM Register/Unregister Methods

        /// <summary>
        /// Called when derived class is registered as a COM server.
        /// </summary>
        [ComRegisterFunctionAttribute]
        public static void Register(Type t)
        {
            LogEvent.LogInformationMessage("Registering BandObject... Started... ", MethodInfo.GetCurrentMethod().Name);
            try
            {
                string guid = t.GUID.ToString("B");

                RegistryKey rkcls = Registry.ClassesRoot.CreateSubKey(@"Classes\\CLSID\" + guid);                
                RegistryKey rkc = rkcls.CreateSubKey("Implemented Categories");

                RegistryKey rkClass = Registry.ClassesRoot.CreateSubKey(@"CLSID\" + guid);
                RegistryKey rkCat = rkClass.CreateSubKey("Implemented Categories");

                BandObjectAttribute[] boa = (BandObjectAttribute[])t.GetCustomAttributes(
                    typeof(BandObjectAttribute),
                    false);

                string name = t.Name;
                string help = t.Name;
                BandObjectStyle style = 0;
                if (boa.Length == 1)
                {
                    if (boa[0].Name != null)
                        name = boa[0].Name;

                    if (boa[0].HelpText != null)
                        help = boa[0].HelpText;

                    style = boa[0].Style;
                }

                rkcls.SetValue(null, name);
                rkcls.SetValue("MenuText", name);
                rkcls.SetValue("HelpText", help);

                rkClass.SetValue(null, name);
                rkClass.SetValue("MenuText", name);
                rkClass.SetValue("HelpText", help);

                if (0 != (style & BandObjectStyle.Horizontal))
                {
                    //InvalidateExplorerBarsList();
                    rkCat.CreateSubKey("{00021494-0000-0000-C000-000000000046}");
                    rkc.CreateSubKey("{00021494-0000-0000-C000-000000000046}");
                }   
              
                 
                RegistryKey registryKey =
                Registry.LocalMachine.OpenSubKey(BHO_REGISTRY_KEY_NAME, true);

                if (registryKey == null)
                    registryKey = Registry.LocalMachine.CreateSubKey(
                                            BHO_REGISTRY_KEY_NAME);
                 
                RegistryKey ourKey = registryKey.OpenSubKey(guid);

                if (ourKey == null)
                {
                    ourKey = registryKey.CreateSubKey(guid);
                }

                ourKey.SetValue(null, WSConstants.WS_ADDIN_NAME);
                ourKey.SetValue("NoExplorer", 1, RegistryValueKind.DWord);

                registryKey.Close();
                ourKey.Close();

                CreateCommandBarButton();
                InvalidateExplorerBarsList();

                LogEvent.LogInformationMessage("Registering BandObject... Finished... ", MethodInfo.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage("Error received while registering BandObject. Error: " + ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        /// <summary>
        /// Called when derived class is unregistered as a COM server.
        /// </summary>
        [ComUnregisterFunctionAttribute]
        public static void Unregister(Type t)
        {
            LogEvent.LogInformationMessage("Un-Registering BandObject... Started... ", MethodInfo.GetCurrentMethod().Name);
            try
            {
                string guid = t.GUID.ToString("B");
                BandObjectAttribute[] boa = (BandObjectAttribute[])t.GetCustomAttributes(
                    typeof(BandObjectAttribute),
                    false);

                BandObjectStyle style = 0;
                if (boa.Length == 1) style = boa[0].Style;

                if (0 != (style & BandObjectStyle.ExplorerToolbar))
                    Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Toolbar").DeleteValue(guid, false);

                InvalidateExplorerBarsList();
                DeleteCommandBarButton();

               Registry.ClassesRoot.CreateSubKey(@"CLSID").DeleteSubKeyTree(guid);

               RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(BHO_REGISTRY_KEY_NAME, true);                

                if (registryKey != null)
                    registryKey.DeleteSubKey(guid, false); 

                LogEvent.LogInformationMessage("Un-Registering BandObject... Finished... ", MethodInfo.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage("Error received while un-registering BandObject. Error: " + ex.Message, MethodInfo.GetCurrentMethod().Name);
            }
        }

        /// <summary>
        /// Creates a new entry in the registry for a custom button in internet explorer
        /// and set the values for ButtonText,HotIcon, Icon and ApplicationPath or ComCLSID or BandCLSID
        /// </summary>
        public static void CreateCommandBarButton()
        {
            try
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                string curDir = Path.GetDirectoryName(path);

                Type myType = typeof(WorldSpaceAttest);
                string BandCLSID = myType.GUID.ToString("B");

                // Create a new id for the button
                Guid buttonGuid = Guid.NewGuid();

                //set the path to the new registry entry
                string SubKey = @"Software\Microsoft\Internet Explorer\Extensions\" + buttonGuid.ToString("B");
                //Create the registry entry for the guid
                RegistryKey rk = Registry.LocalMachine.CreateSubKey(SubKey);

                //create the common registry string values and set them
                rk.SetValue(null, @"C:\Program Files\Deque\WorldSpaceAttestForIE\WorldSpaceAttest.dll");
                rk.SetValue("Default Visible", "Yes");
                rk.SetValue("ButtonText", WSConstants.WS_ADDIN_NAME);
                rk.SetValue("HotIcon", curDir + "\\Images\\worldspace.ico"); //path to *.ico file
                rk.SetValue("Icon", curDir + "\\Images\\worldspace.ico"); //path to *.ico file
                string strCLSID = ToolbarExtensionForBandsCLSID;
                rk.SetValue("BandCLSID", BandCLSID);
                rk.SetValue("CLSID", strCLSID);
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                //throw ex;
            }
        }

        /// <summary>
        /// Deletes the command bar button which is registered while installing the product
        /// </summary>
        private static void DeleteCommandBarButton()
        {
            try
            {
                Type myType = typeof(WorldSpaceAttest);
                string BandCLSID = myType.GUID.ToString("B");

                RegistryKey key = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Internet Explorer\Extensions\", true);
                foreach (var button in key.GetSubKeyNames())
                {
                    if (button != null)
                    {
                        RegistryKey buttonKey = key.OpenSubKey(button);
                        if (buttonKey.GetValueNames().Any(("BandCLSID").Contains))
                        {
                            string buttonCLSID = Convert.ToString(buttonKey.GetValue("BandCLSID"));
                            if (buttonCLSID == BandCLSID)
                            {
                                key.DeleteSubKeyTree(button);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                throw ex;
            }
        }

        /// <summary>
        /// These keys removal helps the IE to avoid opening explorer bar from cache
        /// </summary>
        private static void InvalidateExplorerBarsList()
        {
            try
            {
                RegistryKey categories = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Explorer\Discardable\PostSetup\Component Categories", true);
                if (categories != null)
                {
                    categories.OpenSubKey("{00021494-0000-0000-C000-000000000046}", true).DeleteSubKey("Enum", false);
                }

                // Remove entries for all users
                RegistryKey profiles = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\", false);
                if (profiles != null)
                {
                    foreach (string user in profiles.GetSubKeyNames())
                    {
                        try
                        {
                            RegistryKey userEntry = Registry.Users.OpenSubKey(user + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\Discardable\PostSetup\Component Categories", true);
                            if (userEntry != null)
                            {
                                userEntry.OpenSubKey("{00021494-0000-0000-C000-000000000046}", true).DeleteSubKey("Enum", false);
                            }

                            RegistryKey userEntry64 = Registry.Users.OpenSubKey(user + @"\Software\Microsoft\Windows\CurrentVersion\Explorer\Discardable\PostSetup\Component Categories64", true);
                            if (userEntry64 != null)
                            {
                                userEntry64.OpenSubKey("{00021494-0000-0000-C000-000000000046}", true).DeleteSubKey("Enum", false);
                            }
                        }
                        catch { };
                    }
                }
            }
            catch (Exception ex)
            {
               // throw ex;
            }
        }

        #endregion

        #region Methods


        protected void bandObjectVisibilityChanged(object sender, EventArgs args)
        {
            if (this.Visible)
            {
                AttestIE attestIE = (AttestIE)EXVisualTreeHelper.GetVisualChild<AttestIE>(tabUserControl1);
                attestIE.resetAttest();
            }
        }

        /// <summary>
        /// Renders analysis results on UI after having result set from aXe-core API
        /// </summary>
        /// <param name="vltnsjson"></param>
        /// <param name="rwtnjson"></param>
        public void RenderAnalysisResults(string vltnsjson, string rwtnjson, bool error)
        {
            try
            {
                AttestIE cDocCtrl = (AttestIE)EXVisualTreeHelper.GetVisualChild<AttestIE>(tabUserControl1);
                if (error)
                {
                    MessageBox.Show("aXe has thrown error... Please check with Deque Support");
                    cDocCtrl.spInProgress.Visibility = System.Windows.Visibility.Hidden;
                    return;
                }
                cDocCtrl.RenderAnalysisResults(vltnsjson, rwtnjson);
            }
            catch(Exception ex)
            {
                LogEvent.LogErrorMessage(ex.Message, MethodInfo.GetCurrentMethod().Name);
                MessageBox.Show("Not able to render Analysis results, " + WSConstants.WS_EVENTVIEWER_MESSAGE, WSConstants.WS_ADDIN_NAME);
            }
        }

        #endregion
    }
}
