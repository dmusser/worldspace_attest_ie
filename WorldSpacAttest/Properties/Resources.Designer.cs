﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Deque.WorldSpace.AddIns.MSInternetExplorer.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Deque.WorldSpace.AddIns.MSInternetExplorer.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to /*! aXe v2.6.1
        /// * Copyright (c) 2017 Deque Systems, Inc.
        /// *
        /// * Your use of this Source Code Form is subject to the terms of the Mozilla Public
        /// * License, v. 2.0. If a copy of the MPL was not distributed with this
        /// * file, You can obtain one at http://mozilla.org/MPL/2.0/.
        /// *
        /// * This entire copyright notice must appear in every copy of this file you
        /// * distribute or in any file that contains substantial portions of this source
        /// * code.
        /// */
        ///
        /////sujasreek - IE extension modifications - start
        ///
        ///var axeRun;
        /// / [rest of string was truncated]&quot;;.
        /// </summary>
        public static string axeJs {
            get {
                return ResourceManager.GetString("axeJs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap book {
            get {
                object obj = ResourceManager.GetObject("book", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap color {
            get {
                object obj = ResourceManager.GetObject("color", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap critical {
            get {
                object obj = ResourceManager.GetObject("critical", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap exclamation_red {
            get {
                object obj = ResourceManager.GetObject("exclamation_red", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap hand_property {
            get {
                object obj = ResourceManager.GetObject("hand_property", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to /*!
        /// * jQuery Tools v1.2.6 - The missing UI library for the Web
        /// * 
        /// * tooltip/tooltip.js
        /// * tooltip/tooltip.dynamic.js
        /// * tooltip/tooltip.slide.js
        /// * 
        /// * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
        /// * 
        /// * http://flowplayer.org/tools/
        /// * 
        /// */
        ////*! jQuery v1.6.4 http://jquery.com/ | http://jquery.org/license */
        ///(function(a,b){function cu(a){return f.isWindow(a)?a:a.nodeType===9?a.defaultView||a.parentWindow:!1}function cr(a){if(!cg[a]){var b=c.body,d=f(&quot;&lt;&quot;+a+&quot;&gt;&quot;).appendTo(b),e=d.css(&quot;display&quot;);d [rest of string was truncated]&quot;;.
        /// </summary>
        public static string jquery_tools_min {
            get {
                return ResourceManager.GetString("jquery_tools_min", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to /*
        ///    json2.js
        ///    2012-10-08
        ///
        ///    Public Domain.
        ///
        ///    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
        ///
        ///    See http://www.JSON.org/js.html
        ///
        ///
        ///    This code should be minified before deployment.
        ///    See http://javascript.crockford.com/jsmin.html
        ///
        ///    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
        ///    NOT CONTROL.
        ///
        ///
        ///    This file creates a global JSON object containing two methods: stringify
        ///    and parse.
        ///
        ///        JSON.stringify(value, replacer, space [rest of string was truncated]&quot;;.
        /// </summary>
        public static string json2 {
            get {
                return ResourceManager.GetString("json2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap minor {
            get {
                object obj = ResourceManager.GetObject("minor", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap moderate {
            get {
                object obj = ResourceManager.GetObject("moderate", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap question {
            get {
                object obj = ResourceManager.GetObject("question", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to function getAllChildrenInReadingOrder( node) {
        ///    var child = node.firstChild;
        ///    var retVal = new Array();
        ///    while( child) {
        ///        retVal.push( child); //getJSObject(child));
        ///        var childElements = getAllChildrenInReadingOrder(child);  //getJSObject(child));
        ///        retVal = retVal.concat( childElements);
        ///        child = child.nextSibling;
        ///    }
        ///    return retVal;
        ///}
        ///
        ///function getOffsetParentLeft( element) {
        ///    var parent = element.offsetParent;
        ///    if ( parent != null) {
        ///         [rest of string was truncated]&quot;;.
        /// </summary>
        public static string readingorder {
            get {
                return ResourceManager.GetString("readingorder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap serious {
            get {
                object obj = ResourceManager.GetObject("serious", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap stripe {
            get {
                object obj = ResourceManager.GetObject("stripe", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        public static System.Drawing.Bitmap system_monitor {
            get {
                object obj = ResourceManager.GetObject("system_monitor", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
