﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.Windows;
using System.Runtime.InteropServices;
using HtmlToXamlConvert;
using System.Windows.Markup;
using System.Windows.Documents;
using System.Diagnostics;

namespace Deque.WorldSpace.AddIns.MSInternetExplorer
{
    public static class Helpers
    {
        [DllImport("shell32.dll")]
        static extern int SHGetKnownFolderPath([MarshalAs(UnmanagedType.LPStruct)] Guid rfid, uint dwFlags, IntPtr hToken, out IntPtr pszPath);

        public static string GetKnownFolderPath(Guid knownFolderId)
        {
            string folder = String.Empty;
            IntPtr pszPath = IntPtr.Zero;
            try
            {
                int hr = SHGetKnownFolderPath(knownFolderId, 0, IntPtr.Zero, out pszPath);
                if (hr >= 0)
                    folder = Marshal.PtrToStringAuto(pszPath);
                if (String.IsNullOrEmpty(folder)) 
                    throw Marshal.GetExceptionForHR(hr);
            }
            catch
            {
                folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            }
            finally
            {
                if (pszPath != IntPtr.Zero)
                    Marshal.FreeCoTaskMem(pszPath);
            }
            return folder;
        }
        
        public static string GetRemediationFileName(string ruleId)
        {
            WSServerWorker serverWorker = WSServerWorker.Instance;
            string fileName = String.Empty;

            if (serverWorker.WorldSpaceHelpFileAssociations.ContainsKey(ruleId))
            {
                fileName = (string)serverWorker.WorldSpaceHelpFileAssociations[ruleId];
                return fileName;
            }

            // Try removing the dash-Letter 
            if (ruleId.Contains('-'))
            {
                ruleId = ruleId.Split('-')[0];
                if (serverWorker.WorldSpaceHelpFileAssociations.ContainsKey(ruleId))
                {
                    fileName = (string)serverWorker.WorldSpaceHelpFileAssociations[ruleId];
                    return fileName;
                }
            }

            // Try removing beginning letter
            if (ruleId.StartsWith("N"))
            {
                ruleId = ruleId.TrimStart('N');
                if (serverWorker.WorldSpaceHelpFileAssociations.ContainsKey(ruleId))
                {
                    fileName = (string)serverWorker.WorldSpaceHelpFileAssociations[ruleId];
                    return fileName;
                }
            }

            // Try removing both 
            if (ruleId.Contains('-') &&
                ruleId.StartsWith("N"))
            {
                ruleId = ruleId.Split('-')[0];
                ruleId = ruleId.TrimStart('N');
                if (serverWorker.WorldSpaceHelpFileAssociations.ContainsKey(ruleId))
                {
                    fileName = (string)serverWorker.WorldSpaceHelpFileAssociations[ruleId];
                    return fileName;
                }
            }
            return fileName;
        }
    }

    class NotUrlMatchToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            WSServerWorker serverWorker = WSServerWorker.Instance;
            if (value is string && ((string)value).Equals(serverWorker.BrowserURL))
            {
                return Visibility.Collapsed;
            }
            else
            {
                return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class UrlMatchToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            WSServerWorker serverWorker = WSServerWorker.Instance;
            if (value is string && ((string)value).Equals(serverWorker.BrowserURL))
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class ProjectNameConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string projectName = values[0].ToString();
            string browserURL = values[1].ToString();
            string matched = " (Unmatched)";

            string results = "No Project";

            WSServerWorker serverWorker = WSServerWorker.Instance;
            if (serverWorker.SelectedProject != null && serverWorker.SelectedProject.domains != null)
            {
                results = serverWorker.SelectedProject.orgProjName;
                foreach (WSProjectDomain domain in serverWorker.SelectedProject.domains)
                {
                    if ((!String.IsNullOrEmpty(domain.production) && browserURL.Contains(domain.production)) ||
                        (!String.IsNullOrEmpty(domain.qa) && browserURL.Contains(domain.qa)))
                    {
                        matched = " (Matched)";
                        break;
                    }
                }
            }

            return results + matched;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class StatusToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                return (string)value == "Logged In" ? "/WorldSpaceAttest;component/Images/status.png" : "/WorldSpaceAttest;component/Images/status-offline.png";
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    class StatusToIsEnabledConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                return (string)value == "Logged In" ? "True" : "False";
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    class ProjectToIsEnabledConverter : IValueConverter
    {
        public object Convert(object value, Type targettype, object parameter, CultureInfo culture)
        {
            if (value != null && value is WorldSpaceProject && ((WorldSpaceProject)value).id != -1)
            {
                return true;
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    class IntToIsEnabledConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int)
            {
                if (((int)value) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class BooleanToEnabledMutiConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool automaticallyScan = false;
            bool analysisRunning = false;
            bool.TryParse(values[0].ToString(), out automaticallyScan);
            bool.TryParse(values[1].ToString(), out analysisRunning);

            if (automaticallyScan) return false;
            if (analysisRunning) return false;

            return true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                return !(bool)value;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                return !(bool)value;
            }
            return value;
        }
    }
    
    public class GroupToTitleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            GroupItem groupItem = value as GroupItem;
            CollectionViewGroup collectionViewGroup = groupItem.Content as CollectionViewGroup;
            AccessibilityIssue entryFromGroup = collectionViewGroup.Items[0] as AccessibilityIssue;
            string groupTitle = string.Format("{0} Issue(s) - {1}", collectionViewGroup.ItemCount, entryFromGroup.url);
            return groupTitle;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class StringEllipse : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                string valueToEllipse = ((string)value).Replace("\r", "").Replace("\n", "");
                if (valueToEllipse.Length > 200)
                {
                    return valueToEllipse.Substring(0, 175) + " ... " + valueToEllipse.Substring(valueToEllipse.Length - 20, 20);
                }

                return valueToEllipse;
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PriorityImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string imageUri = "/WorldSpaceAttest;component/Images/";

            switch ((string)value)
            {
                case "critical":
                    imageUri = imageUri + "critical.png";
                    break;
                case "minor":
                    imageUri = imageUri + "minor.png";
                    break;
                case "moderate":
                    imageUri = imageUri + "moderate.png";
                    break;
                case "serious":
                    imageUri = imageUri + "serious.png";
                    break;
                default:
                    imageUri = imageUri + "minor.png";
                    break;
            }

            BitmapImage bitmapImage = new BitmapImage(new Uri(@"pack://application:,,," + imageUri, UriKind.Absolute));
            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SeverityTooltipConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string tooltip = String.Empty;

            switch ((string)value)
            {
                case "level.potential":
                    tooltip = WSConstants.WS_POTENTIAL_VIOLATION_TOOLTIP;
                    break;
                case "level.manual":
                    tooltip = WSConstants.WS_MANUAL_VIOLATION_TOOLTIP;
                    break;
                default:
                    tooltip = WSConstants.WS_VIOLATION_TOOLTIP;
                    break;
            }

            return tooltip;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SeverityImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string imageUri = "/WorldSpaceAttest;component/Images/";

            if ((string)value == "level.manual")
            {
                imageUri = imageUri + "question.png";
            }
            else
            {
                imageUri = imageUri + "exclamation-red.png";
            }

            BitmapImage bitmapImage = new BitmapImage(new Uri(@"pack://application:,,," + imageUri, UriKind.Absolute));
            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ViolationImageConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool color = false;
            bool reading = false;
            bool manual = false;
            bool.TryParse(values[0].ToString(), out color);
            bool.TryParse(values[1].ToString(), out reading);
            bool.TryParse(values[2].ToString(), out manual);
            string imageUri = WSImages.WS_SYSTEM_MONITOR;
            if (color) //color is true
            {
                imageUri = WSImages.WS_COLOR;
            }
            else if (reading) // reading order is true
            {
                imageUri = WSImages.WS_ORDER;
            }
            else if (manual)
            {
                imageUri = WSImages.WS_HAND_PROPERTY;
            }

            BitmapImage bitmapImage = new BitmapImage(new Uri(@"pack://application:,,," + imageUri, UriKind.Absolute));
            return bitmapImage;
        }



        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class ViolationTooltipConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool colorBool = false;
            bool orderBool = false;
            bool manualBool = false;

            Boolean.TryParse(values[0].ToString(), out colorBool);
            Boolean.TryParse(values[1].ToString(), out orderBool);
            Boolean.TryParse(values[2].ToString(), out manualBool);

            if (colorBool) //color is true
            {
                return WSConstants.WS_COLOR_TOOLTIP;
            }
            else if (orderBool) // reading order is true
            {
                return WSConstants.WS_ORDER_TOOLTIP;
            }
            else if (manualBool)
            {
                return WSConstants.WS_MANUAL_TOOLTIP;
            }
            else
            {
                return WSConstants.WS_STATIC_ANALYSIS_TOOLTIP;
            }
        }



        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class HtmlToFlowDocConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
          CultureInfo culture)
        {
            var xaml = HtmlToXamlConverter.ConvertHtmlToXaml((string)value, true);
            var flowDocument = XamlReader.Parse(xaml);
            if (flowDocument is FlowDocument)
                SubscribeToAllHyperlinks((FlowDocument)flowDocument);
            return flowDocument;
        }

        private void SubscribeToAllHyperlinks(FlowDocument flowDocument)
        {
            var hyperlinks = GetVisuals(flowDocument).OfType<Hyperlink>();
            foreach (var link in hyperlinks)
                link.RequestNavigate += LinkRequestNavigate;
        }

        private static IEnumerable<DependencyObject> GetVisuals(DependencyObject root)
        {
            foreach (var child in
               LogicalTreeHelper.GetChildren(root).OfType<DependencyObject>())
            {
                yield return child;
                foreach (var descendants in GetVisuals(child))
                    yield return descendants;
            }
        }

        private void LinkRequestNavigate(object sender,
          System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
          CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
