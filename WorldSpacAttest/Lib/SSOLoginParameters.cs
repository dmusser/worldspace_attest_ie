﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Deque.WorldSpace.AddIns
{
 
    public class SSOLoginParameters
    {
        public static string SSOLoginURL => string.Format(ConfigurationManager.AppSettings["SSOBaseURL"] + "auth/realms/{0}/protocol/openid-connect/auth?response_type=token&scope=&nonce=&client_id={1}&redirect_uri={2}", ConfigurationManager.AppSettings["SSORealm"], ConfigurationManager.AppSettings["SSOClientId"], "https%3A%2F%2Fdev.dequelabs.com/*");

        public static string SSOLogoutURL
        {
            get
            {
                string baseURL = ConfigurationManager.AppSettings["SSOBaseURL"];
                if (baseURL.EndsWith("/"))
                {
                    baseURL = baseURL.Remove(baseURL.Length - 1);
                }
                return string.Format(baseURL + "/auth/realms/{0}/protocol/openid-connect/logout", ConfigurationManager.AppSettings["SSORealm"]);
            }
        }

    }

}
